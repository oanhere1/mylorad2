
            var o = require("skill_data"), t = require("GlobalData"), i = require("Utils"), s = require("Const"), n = require("VideoMgr"), a = (require("wxSdk"), 
            require("Storage"), require("ShareUtils"));
            cc.Class({
                extends: cc.Component,
                properties: {
                    coinLabel: cc.Label,
                    btnUpgrade: cc.Node,
                    goldNode: cc.Node,
                    node1: cc.Node,
                    node2: cc.Node,
                    _sid: 1,
                    _param: 0
                },
                init: function(a) {
                    this._sid = a, this.on_refresh();
                },
                on_refresh: function() {
                    var a = t.skills[this._sid - 1] || 1, e = .4 * ((this.node.getChildByName("lv").getComponent(cc.Label).string = a) - 1);
                    if (this.node.getChildByName("detail").getComponent(cc.Label).string = [ "武器长度", "移动速度", "挥刀速度" ][this._sid - 1] + "增加 " + e.toFixed(1) + "%", 
                    this.node.getChildByName("skill_icon_bg").getChildByName("detail").getComponent(cc.Label).string = "+" + e.toFixed(1) + "%", 
                    a >= s.MAX_SKILL_LV) return this.node1.active = !1, void (this.node2.active = !1);
                    var d = o.data[a]["skill_" + this._sid] || 1;
                    1 == (this._param = d) ? (this.node1.active = !1, this.node2.active = !0, 
                    this.init_skill_ad(), this.notify_upgrade(1), t.io && !t.s_c || (this.node2.getChildByName("btn_upgrade_ad").x = 0, 
                    this.node2.getChildByName("btn_upgrade_share").active = !1, this.node2.getChildByName("label").active = !1)) : (this.node1.active = !0, 
                    this.node2.active = !1, this.goldNode.active = !0, this.goldNode.getChildByName("count").getComponent(cc.Label).string = d, 
                    t.gold >= d ? (this.btnUpgrade.getComponent(cc.Button).interactable = !0, this.btnUpgrade.getChildByName("ico_red").active = !0, 
                    this.notify_upgrade(2)) : (this.btnUpgrade.getComponent(cc.Button).interactable = !1, this.btnUpgrade.getChildByName("ico_upgrade").active = !1, 
                    this.btnUpgrade.getChildByName("ico_upgrade").stopAllActions(), this.btnUpgrade.getChildByName("ico_red").active = !1));
                },
                notify_upgrade: function(a) {
                    var e = this.btnUpgrade.getChildByName("ico_upgrade");
                    1 == a && (e = this.node2.getChildByName("btn_upgrade_ad").getChildByName("ico_upgrade")), e.active = !0, e.stopAllActions(), 
                    e.runAction(cc.repeatForever(cc.sequence(cc.moveBy(.3, cc.v2(0, 5)), cc.moveBy(.3, cc.v2(0, -5)))));
                },
                adOnClose: function(a) {
                    n.videoAd.bind_this.after_ad(a);
                },
                after_ad: function(a) {
                    var e = t.system_info.SDKVersion || "";
                    !i.sdkVersionGreater(e, "2.1.0") || null != a && a.isEnded ? (i.log_event("video_ad_6_5", {}), n.releaseVideo(), 
                    this.after_upgrade(), i.log_event("click_skill_ad_finish", {})) : this.scheduleOnce(function() {
                        try {
                            wx.showToast({
                                title: "看完广告升级"
                            });
                        } catch (a) {}
                    }.bind(this), .2);
                },
                init_skill_ad: function() {
                    i.is_ad_enabled() && 6 != n.cur_adid && (n.createVideo(this, 6), this.enable_ad(!1));
                },
                on_ad_loaded: function() {
                    i.log_event("video_ad_" + n.cur_adid + "_2", {}), this.enable_ad(!0);
                },
                enable_ad: function(a) {
                    this.node2.getChildByName("btn_upgrade_ad").getComponent(cc.Button).interactable = a;
                },
                after_upgrade: function() {
                    i.log_event("upgrade_skill", {
                        skillid: "" + this._sid
                    });
                    var a = t.skills[this._sid - 1] || 1;
                    t.on_update_skill(this._sid - 1, a + 1), this.node.parent.getChildByName("skill_item_1").getComponent("SkillItem").on_refresh(), 
                    this.node.parent.getChildByName("skill_item_2").getComponent("SkillItem").on_refresh(), this.node.parent.getChildByName("skill_item_3").getComponent("SkillItem").on_refresh(), 
                    wx.showToast({
                        title: "升级成功"
                    }), null != t.gameCtrl && t.gameCtrl.play_audio(4, 1);
                },
                on_click_upgrade_ad: function() {
                    1 <= this._sid && 3 >= this._sid && i.is_ad_enabled() && (n.videoAdLoaded ? (i.log_event("video_ad_" + n.cur_adid + "_3", {}), 
                    i.log_event("click_skill_ad", {}), n.showVideo(this)) : wx.showModal({
                        title: "拉取广告失败",
                        content: "亲，暂无合适的广告，请勿着急，稍后再试",
                        showCancel: !1,
                        cancelText: "",
                        confirmText: "好的",
                        complete: function() {}.bind(this)
                    }));
                },
                on_click_upgrade_share: function() {
                    1 <= this._sid && 3 >= this._sid && t.io && a.shareToWx(3, this._sid);
                },
                on_click_upgrade: function() {
                    1 <= this._sid && 3 >= this._sid && (t.gold >= this._param ? (t.on_update_gold(t.gold - this._param), 
                    this.coinLabel.string = t.gold, this.after_upgrade()) : wx.showToast({
                        title: "金币不足"
                    }));
                },
                start: function() {}
            })