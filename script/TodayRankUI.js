
            var d= require("wxSdk"), _= require("GlobalData"), i= require("Utils");
            cc.Class({
                extends: cc.Component,
                properties: {
                    noRankNode: cc.Node,
                    loadingNode: cc.Node,
                    rankItemPrefab: cc.Prefab,
                    rankScrollView: cc.ScrollView,
                    resultRankItemPrefab: cc.Prefab,
                    _lastReqTime: 0
                },
                start: function() {},
                on_refresh: function(o) {
                    var a = i.getNow();
                    120 > a - this._lastReqTime && !_.homeUICtrl._todayRankNeedsUpdate || (this._lastReqTime = a, _.homeUICtrl._todayRankNeedsUpdate = !1, 
                    this.noRankNode.active = !1, this.showLoading(), d.request({
                        url: _.baseUrl + "get_rank_today",
                        method: "GET",
                        success: function(a) {
                            cc.log("get_rank_today request_success"), cc.log(a);
                            var e = a.data;
                            if (this.hideLoading(), 0 == e.code) {
                                this.noRankNode.active = !1;
                                var d = e.data;
                                this.rankScrollView.content.removeAllChildren();
                                var _ = d.length;
                                o && (_ = Math.min(_, 3));
                                for (var t, s = 0; s < _; s++) if (t = d[s], 6 <= t.length) {
                                    var i = null;
                                    o ? (i = cc.instantiate(this.resultRankItemPrefab)).getComponent("ResultRankItem").init(s, t) : (i = cc.instantiate(this.rankItemPrefab)).getComponent("RankItem").init(s, t), 
                                    this.rankScrollView.content.addChild(i);
                                }
                            } else 0 == this.rankScrollView.content.children.length && (this.noRankNode.active = !0);
                        }.bind(this),
                        fail: function() {
                            this.hideLoading(), this.noRankNode.active = !0;
                        }.bind(this)
                    }));
                },
                showLoading: function() {
                    this.loadingNode.active = !0, this.loadingNode.stopAllActions(), this.loadingNode.runAction(cc.repeatForever(cc.rotateBy(.2, 40)));
                },
                hideLoading: function() {
                    this.loadingNode.stopAllActions(), this.loadingNode.active = !1;
                }
            })