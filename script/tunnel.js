function $(W) {
    function F() {
        return _e.status === _;
    }
    function G() {
        return _e.status === d;
    }
    function Y() {
        return _e.status === o;
    }
    function K(a) {
        _e.status !== a && (_e.status = a);
    }
    function J(a, d) {
        h.forEach(function(i) {
            var e = i[0], _ = i[1];
            "*" === e ? _(a, d) : e === a && _(d);
        });
    }
    function x() {
        function s(a) {
            ne ? (K(U), J("error", {
                code: t,
                message: "连接信道服务失败，网络错误或者信道服务没有正确响应",
                detail: a || null
            })) : de(a);
        }
        a || (a = !0, K(ne ? _ : o), i.request({
            url: W,
            method: "GET",
            success: function(a) {
                var e;
                200 == +a.statusCode && a.data && a.data.data.connectUrl ? (e = _e.socketUrl = a.data.data.connectUrl, 
                n.listen({
                    onOpen: z,
                    onMessage: Z,
                    onClose: L,
                    onError: R
                }), wx.connectSocket({
                    url: e
                }), ne = !1) : s(a);
            },
            fail: s,
            complete: function() {
                return a = !1;
            }
        }));
    }
    function z() {
        F() ? J("connect") : Y() && (J("reconnect"), T()), K(d), c.forEach(ee), c.length = 0, 
        ae();
    }
    function Z(e) {
        !function() {
            var d, _, o, t, i = e.data.split(":"), s = i.shift(), l = i.join(":") || null, a = {
                type: s
            };
            if (l) try {
                a.content = JSON.parse(l);
            } catch (a) {}
            switch (a.type) {
              case u:
                t = a.content, _ = t.type, o = t.content, -1 < te.indexOf(_) && (_ = "@" + _), 
                J(_, o);
                break;

              case f:
                ae();
                break;

              case g:
                d = 1e3 * a.content, isNaN(d) || (oe = d, k());
                break;

              case b:
                ie();
            }
        }();
    }
    function ee(a) {
        var e, d;
        G() ? (d = [ (e = a).type ], e.content && d.push(JSON.stringify(e.content)), wx.sendSocketMessage({
            data: d.join(":"),
            fail: R
        })) : c.push(a);
    }
    function ae() {
        clearTimeout(p), clearTimeout(v), p = setTimeout(k, oe);
    }
    function k() {
        G() && (ee({
            type: r
        }), v = setTimeout(m, oe));
    }
    function m() {
        de("服务器已失去响应");
    }
    function de(a) {
        w <= se ? (ie(), J("error", {
            code: l,
            message: "重连失败",
            detail: a
        })) : (wx.closeSocket(), y += S, K(o), E = setTimeout(A, y)), 0 == se && J("reconnecting"), 
        se += 1;
    }
    function A() {
        x();
    }
    function T() {
        y = se = 0;
    }
    function L() {
        C || G() && de("链接已断开");
    }
    function ie() {
        C = !0, G() && ee({
            type: b
        }), wx.closeSocket(), K(U), T(), ne = !1, clearTimeout(p), clearTimeout(v), clearTimeout(E), 
        J("close"), C = !1;
    }
    function R(a) {
        switch (_e.status) {
          case $.STATUS_CONNECTING:
            J("error", {
                code: s,
                message: "连接信道失败，网络错误或者信道服务不可用",
                detail: a
            });
        }
    }
    if (D && D.status !== U) throw new Error("当前有未关闭的信道，请先关闭之前的信道，再打开新信道");
    var _e = D = this;
    this.serviceUrl = W, this.socketUrl = null, this.status = null, this.open = x, 
    this.on = function(a, e) {
        "function" == typeof e && h.push([ a, e ]);
    }, this.emit = function(a, d) {
        ee({
            type: u,
            content: {
                type: a,
                content: d
            }
        });
    }, this.close = ie, this.isClosed = function() {
        return _e.status === U;
    }, this.isConnecting = F, this.isActive = G, this.isReconnecting = Y, K(U);
    var te = "connect,close,reconnecting,reconnect,error".split(","), h = [], ne = !0, a = !1, c = [], oe = 15e3, p = 0, v = 0, se = 0, w = $.MAX_RECONNECT_TRY_TIMES || O, y = 0, S = $.RECONNECT_TIME_INCREASE || P, E = 0, C = !1;
}
var i = require("request"), n = require("wxTunnel"), D = null, U = $.STATUS_CLOSED = "CLOSED", _ = $.STATUS_CONNECTING = "CONNECTING", d = $.STATUS_ACTIVE = "ACTIVE", o = $.STATUS_RECONNECTING = "RECONNECTING", t = $.ERR_CONNECT_SERVICE = 1001, l = ($.ERR_CONNECT_SOCKET = 1002, 
$.ERR_RECONNECT = 2001), s = $.ERR_SOCKET_ERROR = 3001, u = "message", r = "ping", f = "pong", g = "timeout", b = "close", O = 5, P = 1e3;
module.exports = $