
            var d = "weapp_session_" + require("constants").WX_SESSION_MAGIC_ID;
            module.exports = {
                get: function() {
                    return wx.getStorageSync(d) || null;
                },
                set: function(a) {
                    wx.setStorageSync(d, a);
                },
                clear: function() {
                    wx.removeStorageSync(d);
                }
            }