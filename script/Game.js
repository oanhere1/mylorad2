
            var l = require("GlobalData"), o = require("Const"), r = require("avatar_data"), n = require("food_data"), g = require("Utils"), a = (require("quadtree"), 
            require("name_data")), s = (require("wxSdk"), require("Storage")), i = require("ShareUtils");
            cc.Class({
                extends: cc.Component,
                properties: {
                    playerPrefab: cc.Prefab,
                    foodPrefab: cc.Prefab,
                    footprintPrefab: cc.Prefab,
                    speedyBtn: cc.Button,
                    attackBtn: cc.Node,
                    revivePrefab: cc.Prefab,
                    _reviveNode: null,
                    levelLabel: cc.Label,
                    killLabel: cc.Label,
                    cameraNode: cc.Node,
                    friendUINode: cc.Node,
                    ranksPrefab: cc.Prefab,
                    audioNode: cc.Node,
                    newbieNode: cc.Node,
                    comboNode: cc.Node,
                    bottomNode: cc.Node,
                    skill1Lbl: cc.Label,
                    skill2Lbl: cc.Label,
                    skill3Lbl: cc.Label,
                    _player: null,
                    _rankOpen: !0,
                    _rankNum: 5,
                    _ranksNode: cc.Node,
                    _foodsToLoad: [],
                    _foodsToLeave: [],
                    _avatarIdx: 1,
                    _foodIdx: 1,
                    _foodtree: null,
                    _avatartree: null,
                    _startLoadAvatar: !1,
                    _fixedTimeStep: .05,
                    _accumulatedTime: 0,
                    _reviveTimes: 0,
                    _lastRefreshExp: 0,
                    _gameStartTime: 0,
                    _killTimes: 0,
                    _score: 0,
                    _myRank: 13,
                    _foodMax: 130,
                    _avatarMax: 11,
                    _comboQueue: [],
                    _seqKillTimes: 0,
                    _lastKillTime: 0
                },
                onLoad: function() {
                    cc.game.setFrameRate(60), l.gameOver = !1, (l.gameCtrl = this).entities = {}, this.avatars = {}, 
                    this.avatarsPool = new cc.NodePool(), this.foods = {}, this.food_infos = {}, this.foodsPool = new cc.NodePool(), 
                    this.footprintsPool = new cc.NodePool(), this.speedyBtn.node.on(cc.Node.EventType.TOUCH_START, function() {
                        this.onClickSpeedyStart();
                    }.bind(this)), this.speedyBtn.node.on(cc.Node.EventType.TOUCH_END, function() {
                        this.onClickSpeedyEnd();
                    }.bind(this)), this.speedyBtn.node.on(cc.Node.EventType.TOUCH_CANCEL, function() {
                        this.onClickSpeedyEnd();
                    }.bind(this)), cc.game.renderType === cc.game.RENDER_TYPE_CANVAS && (cc.renderer.enableDirtyRegion(!0), 
                    cc.renderer.setDirtyRegionCountThreshold(14)), this.init(), this._ranksNode = cc.instantiate(this.ranksPrefab), 
                    this.node.addChild(this._ranksNode), this.audioNode.getComponent("AudioCtrl").init(!0), this.scheduleOnce(function() {
                        this.refresh_ui();
                    }.bind(this), .05), this.scheduleOnce(function() {
                        1 >= l.play_times ? this.refresh_newbie() : this.newbieNode.active = !1;
                    }.bind(this), .1);
                },
                play_audio: function(a, e) {
                    this.audioNode.getComponent("AudioCtrl").play_audio(a, e);
                },
                clear: function() {
                    this.avatarsPool.clear(), this.entities = {}, this.avatars = {}, this.foodsPool.clear(), 
                    this.footprintsPool.clear(), this.foods = {}, this.food_infos = {};
                },
                init: function() {
                    l.gameOver = !1, this.clear(), this._foodtree = new Quadtree({
                        x: -o.MAP_WIDTH / 2,
                        y: -o.MAP_HEIGHT / 2,
                        width: o.MAP_WIDTH,
                        height: o.MAP_HEIGHT
                    }, 5, 10), this._avatartree = new Quadtree({
                        x: -o.MAP_WIDTH / 2,
                        y: -o.MAP_HEIGHT / 2,
                        width: o.MAP_WIDTH,
                        height: o.MAP_HEIGHT
                    }, 3, 6), null != l.serverConfigs && (null != l.serverConfigs.food_max && (this._foodMax = l.serverConfigs.food_max), 
                    null != l.serverConfigs.avatar_max && (this._avatarMax = l.serverConfigs.avatar_max)), this.init_foods(this._foodMax), 
                    this.init_avatars(this._avatarMax), this.loadEnteredAvatars(), this.scheduleOnce(function() {
                        this.init_self(), this.showBottomNotice("游戏开始", 3);
                    }.bind(this), .5), this.tickCount = 0, this._gameStartTime = g.getNow(), this._killTimes = 0, 
                    this._score = 0, this._lastRefreshExp = 0, this.levelLabel.string = 1, this.killLabel.string = this._killTimes, 
                    this.refresh_friend_ui(), this.init_skills(), this.ySpeed1 = 0, this.xSpeed1 = 0, this.ySpeed2 = 0, 
                    this.xSpeed2 = 0;
                },
                set_input_control: function() {
                    var a = this;
                    cc.eventManager.addListener({
                        event: cc.EventListener.KEYBOARD,
                        onKeyPressed: function(d) {
                            d === cc.macro.KEY.w ? (a.ySpeed1 = 1, a.update_input_speed()) : d === cc.macro.KEY.s ? (a.ySpeed2 = -1, 
                            a.update_input_speed()) : d === cc.macro.KEY.a ? (a.xSpeed1 = -1, a.update_input_speed()) : d === cc.macro.KEY.d ? (a.xSpeed2 = 1, 
                            a.update_input_speed()) : void 0;
                        },
                        onKeyReleased: function(d) {
                            d === cc.macro.KEY.w ? (a.ySpeed1 = 0, a.update_input_speed()) : d === cc.macro.KEY.s ? (a.ySpeed2 = 0, 
                            a.update_input_speed()) : d === cc.macro.KEY.a ? (a.xSpeed1 = 0, a.update_input_speed()) : d === cc.macro.KEY.d ? (a.xSpeed2 = 0, 
                            a.update_input_speed()) : void 0;
                        }
                    }, a.node);
                },
                update_input_speed: function() {
                    var a = this.xSpeed1 + this.xSpeed2, e = this.ySpeed1 + this.ySpeed2;
                    if (null != l.avatar) {
                        var d = this.entities[l.avatar.id];
                        if (null != d) if (0 == a && 0 == e) d.getComponent("Player").setSpeed(0); else {
                            var i = 360 - (Math.atan2(e, a) * (180 / Math.PI) + 360) % 360;
                            d.getComponent("Player").setSpeed(5), d.getComponent("Player").setAngle(i);
                        }
                    }
                },
                refresh_ui: function() {
                    try {
                        2.16 < l.system_info.windowWidth / l.system_info.windowHeight && (this.node.getChildByName("ui").getComponent(cc.Widget).left = 65, 
                        this._ranksNode.getComponent(cc.Widget).left = 65, this.speedyBtn.node.getComponent(cc.Widget).right = 70, 
                        this.attackBtn.getComponent(cc.Widget).right = 130);
                    } catch (a) {}
                },
                refresh_newbie: function() {
                    try {
                        this.newbieNode.active = !0;
                        var a = this.newbieNode.getChildByName("newbie_attack");
                        a.x = this.attackBtn.x - 196, a.y = this.attackBtn.y + 74;
                        var e = this.newbieNode.getChildByName("newbie_speedy");
                        e.x = this.speedyBtn.node.x - 118, e.y = this.speedyBtn.node.y + 140;
                        var d = this.newbieNode.getChildByName("newbie_move"), i = this.node.getChildByName("ui").getChildByName("joy_stick");
                        d.x = i.x + 80, d.y = i.y + 80, this.newbieNode.getChildByName("btn_iknow").runAction(cc.repeatForever(cc.sequence(cc.scaleTo(.3, 1.06), cc.scaleTo(.3, 1)))), 
                        this.scheduleOnce(function() {
                            this.on_click_know();
                        }.bind(this), 5);
                    } catch (a) {}
                },
                on_click_know: function() {
                    this.newbieNode.active = !1;
                },
                init_skills: function() {
                    if (1 == l.skills[0] && 1 == l.skills[1] && 1 == l.skills[2]) this.bottomNode.active = !1; else {
                        this.bottomNode.active = !0;
                        var a = "+" + (.4 * (l.skills[0] - 1)).toFixed(1) + "%";
                        0 < l.skills_halo[0] && (a += ",+" + o.SKILL_HALO_PERCENT + "%");
                        var e = "+" + (.4 * (l.skills[1] - 1)).toFixed(1) + "%";
                        0 < l.skills_halo[1] && (e += ",+" + o.SKILL_HALO_PERCENT + "%");
                        var d = "+" + (.4 * (l.skills[2] - 1)).toFixed(1) + "%";
                        0 < l.skills_halo[2] && (d += ",+" + o.SKILL_HALO_PERCENT + "%"), this.skill1Lbl.string = a, 
                        this.skill2Lbl.string = e, this.skill3Lbl.string = d;
                    }
                },
                init_foods: function(a, e) {
                    for (var d in this._foodtree.clear(), this.food_infos) this.food_infos.hasOwnProperty(d) && this._foodtree.insert(this.food_infos[d]);
                    for (var _, i = 0; i < a; i++) _ = this.new_food(), e && 25 >= g.getRandomInt(1, 100) && (_.x /= 2, 
                    _.y /= 2), l.enteredFoods.push(_);
                    this.loadEnteredFoods();
                },
                check_foods: function() {
                    if (null != this.foods) {
                        var a = 0;
                        for (var e in this.foods) this.foods.hasOwnProperty(e) && (a += 1);
                        var d = this._foodMax - a;
                        0 < d && this.init_foods(d, !0);
                    }
                },
                new_food: function() {
                    var a = g.getRandomPosition(), e = {
                        x: a[0],
                        y: a[1],
                        width: 10,
                        height: 10,
                        id: this._foodIdx,
                        fid: g.getRandomInt(1, 7)
                    };
                    return this._foodIdx += 1, e;
                },
                init_self: function() {
                    var a = this.new_avatar();
                    if (a.is_player = !0, (a.level = 1) < l.today_buff) {
                        var e = s.get_int("today_buff_until");
                        if (g.getNow() < e) {
                            a.level = l.today_buff, a.expLevel = l.today_buff, a.modelID = l.today_buff, a.weaponID = l.today_buff;
                            for (var d = 1; d < l.today_buff; d++) a.exp += r.data[d].exp;
                        }
                    }
                    if (0 < l.wheel_buff) {
                        for (a.level = 10, a.expLevel = 10, a.modelID = 10, a.weaponID = 10, d = 1; 10 > d; d++) a.exp += r.data[d].exp;
                        l.on_update_wheel_buff(l.wheel_buff - 1);
                    }
                    a.name = null != l.userInfo && null != l.userInfo.nickName ? g.subString(l.userInfo.nickName, 10, !0) : "荣耀勇士";
                    var i = this._get_random_pos_with_less_enemy();
                    a.x = i[0], a.y = i[1], l.enteredAvatars.push(a);
                },
                init_avatars: function(d) {
                    for (var e, i = [ 1, 1, 2, 2, 3, 4, 5, 6, 7, 7, 8, 8, 9, 9, 10 ], _ = a.data.length, n = 0; n < d; n++) {
                        e = this.new_avatar(), e.level = i[n], e.expLevel = i[n], e.modelID = i[n], e.weaponID = i[n];
                        for (var t = 1; t < i[n]; t++) e.exp += r.data[t].exp;
                        e.name = g.subString(a.data[g.getRandomInt(0, _ - 1)], 10, !0), e.expOfLevel = g.getRandomInt(1, r.data[i[n]].exp - 10), 
                        l.enteredAvatars.push(e);
                    }
                },
                new_avatar: function() {
                    var a = g.getRandomPosition(), e = {
                        is_player: !1,
                        is_alive: !0,
                        in_protect: !1,
                        name: this._avatarIdx,
                        x: a[0],
                        y: a[1],
                        width: 30,
                        height: 30,
                        id: this._avatarIdx,
                        modelID: 1,
                        weaponID: 1,
                        level: 1,
                        expLevel: 1,
                        exp: 0,
                        expOfLevel: 0,
                        addSpeed: 0,
                        revive_times: 0
                    };
                    return this._avatarIdx += 1, e;
                },
                loadEnteredAvatars: function() {
                    this._startLoadAvatar = !0;
                },
                onRealAvatarEnterWorld: function(a) {
                    var e = null;
                    if ((e = 0 < this.avatarsPool.size() ? this.avatarsPool.get() : cc.instantiate(this.playerPrefab)).x = a.x, 
                    e.y = a.y, e.getComponent("Player").setAvatar(a), this.entities[a.id] = e, this.avatars[a.id] = a, 
                    e.getComponent("Player").initModel(), e.getComponent("Player").onIsDead(a, !1), e.getComponent("Player").setPos(a.x, a.y), 
                    a.is_player) {
                        l.avatar = a, this.set_protect(l.avatar), this._player = e;
                        var d = this.node.getChildByName("ui").getChildByName("joy_stick");
                        d.getComponent("Joystick").sprite = e, d.getChildByName("joy_bg").getComponent("JoystickBg")._playerNode = e, this.cameraNode.getComponent("Camera").targetNode = e, 
                        this.onRefreshLvInfo(a), this.onIsDead(a);
                    }
                    this.node.getChildByName("avatars").addChild(e, a.modelID), this.onUpdateRank(), this._avatartree.insert(a);
                },
                onAvatarLeaveWorld: function(a) {
                    null != this.entities && null != this.entities[a.id] && (this.avatarsPool.put(this.entities[a.id]), 
                    delete this.entities[a.id], delete this.avatars[a.id], this.onUpdateRank());
                },
                loadEnteredFoods: function() {
                    for (var a = 0, e = l.enteredFoods.length; a < e; a++) this.onFoodEnterWorld(l.enteredFoods[a]);
                    l.enteredFoods = [];
                },
                onFoodEnterWorld: function(a) {
                    this._foodsToLoad.push(a);
                },
                onFoodRealEnterWorld: function(a) {
                    var e = null;
                    (e = 0 < this.foodsPool.size() ? this.foodsPool.get() : cc.instantiate(this.foodPrefab)).x = a.x, 
                    e.y = a.y, this.foods[a.id] = e, this.food_infos[a.id] = a, e.fid = a.fid;
                    var d = l.weaponAtlas.getSpriteFrame("food_" + a.fid);
                    if (null != d) {
                        var _ = n.data[a.fid], t = 100;
                        null != _ && null != _.radius_per && (t = _.radius_per), e.active = !0, e.scale = o.INIT_FOOD_SCALE * t / 100, 
                        e.getComponent(cc.Sprite).spriteFrame = d, this.node.getChildByName("foods").addChild(e), this._foodtree.insert(a);
                    }
                },
                onFoodLeaveWorld: function(a) {
                    this.onFoodRealLeaveWorld(a);
                },
                onFoodRealLeaveWorld: function(a) {
                    null != this.foods && null != this.foods[a.id] && (this.foodsPool.put(this.foods[a.id]), 
                    delete this.foods[a.id], delete this.food_infos[a.id]);
                },
                reduce_exp: function(a, e) {
                    if (null != l.gameCtrl) {
                        var d = l.gameCtrl.avatars[a];
                        if (null != d) {
                            if (d.exp = Math.max(d.exp - e, 0), d.expOfLevel >= e) d.expOfLevel -= e; else {
                                var _ = e - d.expOfLevel;
                                if (1 >= d.level) d.expOfLevel = 0, d.expLevel = 1; else {
                                    d.level -= 1, d.expLevel = d.level;
                                    var t = r.data[d.expLevel].exp || 0;
                                    d.expOfLevel = Math.max(t - _, 0), this.on_level_downgrade(d, d.level);
                                }
                            }
                            this.onRefreshExp(d);
                        }
                    }
                },
                add_exp_delay: function(a, e, d) {
                    this.scheduleOnce(function() {
                        this.add_exp(a, e);
                    }.bind(this), d);
                },
                add_exp: function(a, e) {
                    if (null != l.gameCtrl) {
                        var d = l.gameCtrl.avatars[a];
                        if (null != d) {
                            if (!d.is_player) {
                                var _ = {
                                    1: 80,
                                    2: 60,
                                    3: 40,
                                    4: 30,
                                    5: 20
                                }[this._myRank];
                                null == _ && (_ = 0), e = Math.floor(e * (1 + _ / 100));
                            }
                            d.exp += e, d.expOfLevel += e;
                            var t = r.data[d.expLevel].exp || 0;
                            0 < t && d.expOfLevel > t && (d.expOfLevel -= t, d.expLevel < d.level ? d.expLevel += 1 : d.level < o.MAX_LV ? (d.level = Math.min(d.level + 1, o.MAX_LV), 
                            d.expLevel = Math.min(d.expLevel + 1, o.MAX_LV), this.on_level_upgrade(d, d.level)) : d.expOfLevel = t), 
                            this.onRefreshExp(d), d.is_player && (this._score = d.exp, this.onRefreshLvInfo(d), (30 < this._score - this._lastRefreshExp || 10 >= this._score) && this.refresh_friend_ui());
                        }
                    }
                },
                refresh_friend_ui: function() {
                    this.friendUINode.getComponent("FriendUI").init(this._score), this._lastRefreshExp = this._score, this.scheduleOnce(function() {
                        this.friendUINode.getChildByName("friend").active = !0;
                    }.bind(this), .2);
                },
                refresh_avatartree: function() {
                    if (null != this._avatartree) for (var a in this._avatartree.clear(), this.avatars) this.avatars.hasOwnProperty(a) && (this.avatars[a].x = this.entities[a].x, 
                    this.avatars[a].y = this.entities[a].y, this._avatartree.insert(this.avatars[a]));
                },
                set_protect: function(a) {
                    null != a && null != l.avatar && a.id == l.avatar.id && (a.in_protect = !0, 
                    this.scheduleOnce(function() {
                        a.in_protect = !1;
                    }.bind(this), 3));
                },
                set_position: function() {},
                set_direction: function() {},
                updatePosition: function(a) {
                    null != this.entities[a.id] && (a.needsSyncToServer() || this.entities[a.id].getComponent("Player").move_to(a.position.x * o.METER_TO_PIXEL, a.position.z * o.METER_TO_PIXEL));
                },
                onClickRank: function() {
                    var a = this._ranksNode.getChildByName("body");
                    this._rankOpen ? (this._rankOpen = !1, a.active = !1) : (this._rankOpen = !0, a.active = !0);
                },
                onUpdateRank: function() {
                    if (null != l.avatar) {
                        var d = Object.keys(this.avatars).map(function(a) {
                            return [ a, this.avatars[a] ];
                        }.bind(this));
                        d.sort(function(a, e) {
                            return e[1].exp == a[1].exp ? e[1].id - a[1].id : e[1].exp - a[1].exp;
                        });
                        for (var e = 16, _ = 0; _ < d.length; _++) d[_][1].id == l.avatar.id && (e = _ + 1, 
                        this._myRank = e);
                        var r = this._ranksNode.getChildByName("body"), t = d.slice(0, this._rankNum);
                        for (_ = 0; _ < t.length; _++) {
                            var i = r.getChildByName("rank" + (_ + 1)), s = r.getChildByName("exp" + (_ + 1)), n = r.getChildByName("nickname" + (_ + 1));
                            i.active = !0, s.active = !0, n.active = !0, 3 > _ && (r.getChildByName("ico_" + (_ + 1)).active = !0);
                            var a = t[_][1];
                            n.getComponent(cc.Label).string = a.name, s.getComponent(cc.Label).string = a.exp;
                            var u = cc.Color.WHITE;
                            0 == _ ? u.fromHEX("#FF5236") : 1 == _ ? u.fromHEX("#55FF75") : 2 == _ && u.fromHEX("#3695FF"), 
                            a.id == l.avatar.id && u.fromHEX("#B47EFF"), 3 <= _ && (i.color = u), 
                            n.color = u, s.color = u;
                        }
                        for (_ = t.length; _ < this._rankNum; _++) i = r.getChildByName("rank" + (_ + 1)), s = r.getChildByName("exp" + (_ + 1)), 
                        n = r.getChildByName("nickname" + (_ + 1)), 3 > _ && (r.getChildByName("ico_" + (_ + 1)).active = !1), 
                        i.active = !1, s.active = !1, n.active = !1;
                        r.getChildByName("rank").getComponent(cc.Label).string = e, r.getChildByName("nickname").getComponent(cc.Label).string = l.avatar.name, 
                        r.getChildByName("exp").getComponent(cc.Label).string = l.avatar.exp;
                    } else this.scheduleOnce(function() {
                        this.onUpdateRank();
                    }.bind(this), 1);
                },
                onClickAttack: function() {
                    null != this._player && !l.gameOver && l.avatar.is_alive && (this._player.getComponent("Player")._inAttackAction || (this._player.getComponent("Player").playAttackAction(), 
                    this._player.getComponent("Player").onClickAttack()));
                },
                on_attack: function(a, d, i) {
                    if (null != this.entities[a.id] && (null != l.avatar && l.avatar.id == a.id || this.entities[a.id].getComponent("Player").playAttackAction(), 
                    i)) {
                        var t = a.is_alive && this.check_rayhit(a, d) && d.is_alive && !d.in_protect;
                        this.scheduleOnce(function() {
                            if (t && a.is_alive) {
                                var i = d.is_alive;
                                if (d.is_alive = !1, this.onIsDead(d), null != l.avatar && a.id == l.avatar.id) {
                                    this._killTimes += 1, this.killLabel.string = this._killTimes, null != d.name && this.showTopNotice("你击败了" + g.subString(d.name, 8, !0), 1.5);
                                    var e = g.getNow();
                                    5 > e - this._lastKillTime ? (this._seqKillTimes += 1, 2 <= this._seqKillTimes && this.showNewCombo("/" + this._seqKillTimes, 1.7)) : this._seqKillTimes = 1, 
                                    this._lastKillTime = e, this.audioNode.getComponent("AudioCtrl").play_audio(1);
                                    try {
                                        wx.vibrateShort();
                                    } catch (a) {}
                                }
                                var _ = r.data[d.expLevel].exp_bonus || 0;
                                this.add_exp_delay(a.id, _, .2), a.is_player || d.is_player || this.reduce_exp(d.id, _), 
                                null != l.avatar && d.id == l.avatar.id && i && this.on_gameover(a.name);
                            }
                        }.bind(this), .12);
                    }
                },
                check_rayhit: function(p, e) {
                    if (null != p && null != e) {
                        var _ = this.entities[p.id], k = this.entities[e.id];
                        if (null == _ || null == k) return !1;
                        var t = [ _.x, _.y ], i = [ k.x, k.y ], s = _.getComponent("Player").attackRadius, n = _.getComponent("Player").radius, a = k.getComponent("Player").radius, c = _.getComponent("Player")._playerNode.rotation;
                        c = (c + 30) % 360;
                        var o = g.getDist2({
                            x: t[0],
                            y: t[1]
                        }, {
                            x: i[0],
                            y: i[1]
                        });
                        if (o < 2 * (n * n)) return !0;
                        var h = s + a;
                        if (h * h < o) return !1;
                        var r = i[0] - t[0], l = i[1] - t[1], u = Math.cos(c / 180 * Math.PI), d = -Math.sin(c / 180 * Math.PI);
                        return 0 <= g.point_interact_ray(u, d, r, l);
                    }
                },
                onIsDead: function(a) {
                    null != this.entities[a.id] && (a.is_player && this.cancel_speedy(), this.entities[a.id].getComponent("Player").onIsDead(a, !0), 
                    this.scheduleOnce(function() {
                        a.id != l.avatar.id && this.onRevive(a);
                    }.bind(this), 1));
                },
                onRevive: function(d) {
                    var e = g.getRandomPosition();
                    if (null == d.revive_times ? d.revive_times = 1 : d.revive_times += 1, d.is_player) e = this._get_random_pos_with_less_enemy(); else if (e = this._get_random_pos_beyond_player(), 
                    2 <= d.revive_times) {
                        var _ = a.data.length;
                        if (d.name = g.subString(a.data[g.getRandomInt(0, _ - 1)], 10, !0), d.revive_times = 0, 
                        null != l.avatar && d.exp > l.avatar.exp) {
                            var n = Object.keys(this.avatars).map(function(a) {
                                return [ a, this.avatars[a] ];
                            }.bind(this));
                            n.sort(function(a, e) {
                                return e[1].exp == a[1].exp ? e[1].id - a[1].id : e[1].exp - a[1].exp;
                            });
                            var t = Math.floor(.8 * l.avatar.exp);
                            5 < n.length && (t = n[4].exp);
                            var i = d.exp - t;
                            0 < i && this.reduce_exp(d.id, i);
                        }
                    }
                    d.x = e[0], d.y = e[1], this.entities[d.id].x = e[0], this.entities[d.id].y = e[1], 
                    d.is_alive = !0, this.entities[d.id].getComponent("Player").onIsDead(d, !0), d.is_player && this.set_protect(d);
                },
                get_revive_node: function() {
                    return null == this._reviveNode && (this._reviveNode = cc.instantiate(this.revivePrefab), this.node.addChild(this._reviveNode)), 
                    this._reviveNode;
                },
                on_gameover: function(a) {
                    this.friendUINode.active = !1, this.audioNode.getComponent("AudioCtrl").play_audio(1), this.scheduleOnce(function() {
                        var d = this.get_revive_node();
                        1 > this._reviveTimes ? g.log_event("end_game_1", {
                            level: "" + l.avatar.expLevel
                        }) : g.log_event("end_game_2", {
                            level: "" + l.avatar.expLevel
                        }), d.active = !0, d.getComponent("GameOver").on_refresh(this._reviveTimes, a, this._score, this._killTimes);
                    }.bind(this), 2), l.last_play = [ this._killTimes, this._score, g.getNow() - this._gameStartTime, l.avatar.expLevel ];
                },
                on_real_revive: function() {
                    return l.set_coin_revive(l.coin_revive + 1), this.onClickRevive(), g.log_event("share_2", {
                        stype: "3"
                    }), "复活成功";
                },
                check_share_valid: function(a) {
                    var e = "share_groups:" + g.getToday() + ":" + a, d = s.get_int(e);
                    if (0 >= d) return s.set_data(e, 1), !0;
                    try {
                        return !(l.share_diff && l.avatar.expLevel >= l.share_diff_lv || d >= l.share_revive_max || (s.set_data(e, d + 1), 
                        0));
                    } catch (a) {
                        return s.set_data(e, 1), !0;
                    }
                },
                onClickShare: function() {
                    i.shareToWx(2);
                },
                _add_share_times: function() {
                    var a = "share_times:" + g.getToday(), e = s.get_int(a);
                    s.set_data(a, e + 1);
                },
                onClickRevive: function() {
                    l.avatar.is_alive || (this.get_revive_node().getComponent("GameOver").destroy_ads(), 0 == this._reviveTimes && 0 < l.coin_revive && (l.gameOver = !1, 
                    l.set_coin_revive(l.coin_revive - 1), this.get_revive_node().active = !1, this.onRevive(l.avatar), this._reviveTimes = 1), 
                    this.friendUINode.getComponent("FriendUI").init(this._score), this._lastRefreshExp = this._score, this.friendUINode.active = !0, 
                    this.friendUINode.getChildByName("bg").active = !1, this.friendUINode.getChildByName("friend").active = !1, 
                    this.scheduleOnce(function() {
                        this.friendUINode.getChildByName("bg").active = !0, this.friendUINode.getChildByName("friend").active = !0;
                    }.bind(this), .2));
                },
                onClickReturn: function() {
                    this.onReturnDestroy(), cc.director.loadScene("home", function() {}.bind(this));
                },
                onReturnDestroy: function() {
                    this.get_revive_node().active = !1, this.avatarsPool.clear(), this.foodsPool.clear(), this.footprintsPool.clear(), 
                    l.reset();
                },
                onRefreshBtns: function(a) {
                    this.node.getChildByName("ui").active = a;
                },
                on_level_upgrade: function(a, e) {
                    if (null != this.entities[a.id]) {
                        if (a.is_player) {
                            this.onRefreshLvInfo(a), this.showTopNotice("恭喜你进化到" + a.level + "级", 1.5);
                            var d = r.data[a.level].name;
                            null != d && this.showNewLevel(d, 1.7), this.show_tips(a), this.play_audio(3);
                        }
                        this.entities[a.id].getComponent("Player").on_level_upgrade(a, e);
                    }
                },
                show_tips: function() {
                    this.node.getChildByName("ui").getChildByName("top_bg").getChildByName("tips").active = !1;
                },
                on_level_downgrade: function(a, e) {
                    null != this.entities[a.id] && this.entities[a.id].getComponent("Player").on_level_downgrade(a, e);
                },
                onRefreshLvInfo: function(a) {
                    this.onRefreshExp(a);
                    var e = r.data[a.level];
                    if (null != e) {
                        var d = e.camera_ratio;
                        null == d && (d = 100), this.cameraNode.getComponent(cc.Camera).zoomRatio = o.INIT_CAMERA_ZOOM_RATIO * d / 100;
                    }
                },
                onRefreshExp: function(a) {
                    var e = this.node.getChildByName("levels").getChildByName("levels_" + a.id);
                    if (null != e) {
                        e.getChildByName("lv").getComponent(cc.Label).string = a.level, a.is_player && (this.levelLabel.string = a.level);
                        var d = r.data[a.expLevel];
                        if (null != d) {
                            var i = d.exp;
                            a.is_player && (e.getChildByName("exp").getComponent(cc.ProgressBar).progress = a.expOfLevel / i);
                        }
                        this.onUpdateRank();
                    }
                },
                createFootprint: function(a) {
                    if (null != this.footprintsPool) {
                        var e = this.node.getChildByName("player_parts").getChildByName("footprints"), d = e.convertToNodeSpaceAR(a), i = null;
                        (i = 0 < this.footprintsPool.size() ? this.footprintsPool.get() : cc.instantiate(this.footprintPrefab)).opacity = 255, 
                        i.x = d.x, i.y = d.y, e.addChild(i), i.runAction(cc.sequence(cc.fadeOut(.4), cc.removeSelf(), cc.callFunc(function() {
                            this.footprintsPool.put(i);
                        }.bind(this))));
                    }
                },
                onClickSpeedyStart: function() {
                    if (!l.gameOver && l.avatar.is_alive) {
                        var a = r.data[l.avatar.expLevel];
                        if (null != a) {
                            var e = a.exp * o.ADD_SPEED_NEED_EXP_PER_SEC / 100;
                            if (!(1 >= l.avatar.expLevel && l.avatar.expOfLevel < e)) {
                                var d = r.data[l.avatar.level];
                                if (null != d) {
                                    var i = d.add_speed_per || 0;
                                    this._player.getComponent("Player").setSpeedBoostScale(1 + i / 100), l.avatar.addSpeed = i;
                                }
                            }
                        }
                    }
                },
                onClickSpeedyEnd: function() {
                    !l.gameOver && l.avatar.is_alive && this.cancel_speedy();
                },
                cancel_speedy: function() {
                    this._player.getComponent("Player").setSpeedBoostScale(1), l.avatar.addSpeed = 0;
                },
                onSpeedy: function(a) {
                    null != this.entities[a.id] && this.entities[a.id].getComponent("Player").setSpeedBoostScale(1 + a.addSpeed / 100);
                },
                showNewCombo: function(a, e) {
                    this._comboQueue.push([ a, e ]);
                },
                realShowNewCombo: function(a) {
                    var e = a[0];
                    a[1], this.comboNode.active ? (this.comboNode.getChildByName("combo").getComponent(cc.Label).string = e, 
                    this.comboNode.getComponent(cc.Animation).play("new_combo_seq"), this.comboNode.opacity = 255) : (this.comboNode.active = !0, 
                    this.comboNode.stopAllActions(), this.comboNode.opacity = 255, this.comboNode.x = 760, this.comboNode.getChildByName("combo").getComponent(cc.Label).string = e, 
                    this.comboNode.getComponent(cc.Animation).play("new_combo")), this.comboNode.stopAllActions(), this.comboNode.runAction(cc.sequence(cc.delayTime(3), cc.fadeOut(2), cc.callFunc(function() {
                        this.comboNode.active = !1;
                    }.bind(this))));
                },
                showNewLevel: function(a, e) {
                    var d = this.node.getChildByName("ui").getChildByName("top_notice");
                    d.active ? this.scheduleOnce(function() {
                        this.showNewLevel(a, e);
                    }.bind(this), .6) : (d.active = !0, d.opacity = 255, d.getChildByName("notice").getComponent(cc.Label).string = a, 
                    d.getComponent(cc.Animation).play("new_level"), null != e && d.runAction(cc.sequence(cc.delayTime(e), cc.fadeOut(.3), cc.callFunc(function() {
                        d.active = !1;
                    }))));
                },
                showTopNotice: function(a, e) {
                    var d = this.node.getChildByName("ui").getChildByName("notice");
                    d.active ? this.scheduleOnce(function() {
                        this.showTopNotice(a, e);
                    }.bind(this), .6) : (d.active = !0, d.opacity = 255, d.getChildByName("notice").getComponent(cc.Label).string = a, 
                    d.getComponent(cc.Animation).play("top_notice"), null != e && d.runAction(cc.sequence(cc.delayTime(e), cc.fadeOut(.3), cc.callFunc(function() {
                        d.active = !1;
                    }))));
                },
                showBottomNotice: function(a, e) {
                    var d = this.node.getChildByName("ui").getChildByName("bottom_notice");
                    d.active = !0, d.opacity = 255;
                    var i = d.getChildByName("text");
                    i.getComponent(cc.Label).string = a, d.width = Math.max(i.width + 200, 320), d.getComponent(cc.Animation).play("notice"), 
                    null != e && d.runAction(cc.sequence(cc.delayTime(e), cc.fadeOut(.3), cc.callFunc(function() {
                        d.getChildByName("ico_battle").active = !1, d.active = !1;
                    })));
                },
                hideBottomNotice: function(a) {
                    var e = this.node.getChildByName("ui").getChildByName("bottom_notice");
                    e.runAction(cc.sequence(cc.fadeOut(a), cc.callFunc(function() {
                        e.active = !1;
                    })));
                },
                _get_random_pos_with_less_enemy: function() {
                    if (null == this._avatartree) return g.getRandomPosition();
                    for (var a = 20, e = null, d = 0; 5 >= d; d++) {
                        var _ = g.getRandomPosition(), t = this._avatartree.retrieve({
                            x: _[0],
                            y: _[1],
                            width: 100,
                            height: 100
                        });
                        t.length < a && (a = t.length, e = _);
                    }
                    return e;
                },
                _get_random_pos_beyond_player: function() {
                    var a = g.getRandomPosition();
                    if (null == l.avatar) return a;
                    try {
                        for (var e = this.entities[l.avatar.id].x, d = this.entities[l.avatar.id].y, _ = cc.winSize, t = 0; 10 > t && (a = g.getRandomPosition(), 
                        !(Math.abs(a[0] - e) > _.width / 2 && Math.abs(a[1], d) > _.height / 2)); t++) ;
                    } catch (a) {}
                    return a;
                },
                start: function() {},
                fixedUpdate: function() {
                    if (null != l.avatar && 0 < l.avatar.addSpeed) {
                        var a = r.data[l.avatar.expLevel].exp * o.ADD_SPEED_NEED_EXP_PER_SEC / 100 * this._fixedTimeStep;
                        1 >= l.avatar.expLevel && l.avatar.expOfLevel < a ? this.cancel_speedy() : (l.avatar.expOfLevel >= a ? l.avatar.expOfLevel -= a : (l.avatar.expLevel -= 1, 
                        l.avatar.expOfLevel = r.data[l.avatar.expLevel].exp), this.onRefreshLvInfo(l.avatar));
                    }
                },
                update: function(a) {
                    if (this._accumulatedTime += a, this._accumulatedTime > this._fixedTimeStep && (this.fixedUpdate(), this._accumulatedTime -= this._fixedTimeStep), 
                    this.tickCount += 1, 0 == this.tickCount % 4) {
                        if (null != this._comboQueue && 0 < this._comboQueue.length && null != this.comboNode) {
                            var e = this.comboNode.getComponent(cc.Animation);
                            if (!e.getAnimationState("new_combo").isPlaying && !e.getAnimationState("new_combo_seq").isPlaying) {
                                var d = this._comboQueue.shift();
                                this.realShowNewCombo(d);
                            }
                        }
                        if (this._startLoadAvatar && 0 < l.enteredAvatars.length && (d = l.enteredAvatars.shift(), this.onRealAvatarEnterWorld(d)), 
                        0 < this._foodsToLoad.length) {
                            var i = this._foodsToLoad.shift();
                            this.onFoodRealEnterWorld(i);
                        }
                    }
                    0 < this._foodsToLeave.length && (i = this._foodsToLeave.shift(), this.onFoodRealLeaveWorld(i)), 0 == this.tickCount % 60 && 0 == this._foodsToLoad.length && this.check_foods();
                }
            })