
            var l = require("Utils"), r = require("GlobalData"), a = require("Storage"), o = (require("Const"), require("VideoMgr")), i = (require("wxSdk"), 
            require("ShareUtils"));
            cc.Class({
                extends: cc.Component,
                properties: {
                    pointer: cc.Node,
                    light1Node: cc.Node,
                    light2Node: cc.Node,
                    btnFree: cc.Node,
                    btnShare: cc.Node,
                    btnAd: cc.Node,
                    btnAdLabel: cc.Label,
                    timeLabel: cc.Label,
                    bonusNode: cc.Node,
                    _inAction: !1
                },
                start: function() {},
                on_open: function() {
                    l.log_event("click_wheel", {}), this.pointer.rotation = 0, this.node.runAction(cc.repeatForever(cc.sequence(cc.callFunc(function() {
                        this.light1Node.active = !0, this.light2Node.active = !1;
                    }.bind(this)), cc.delayTime(.3), cc.callFunc(function() {
                        this.light1Node.active = !1, this.light2Node.active = !0;
                    }.bind(this)), cc.delayTime(.3), cc.callFunc(function() {
                        this.light1Node.active = !0, this.light2Node.active = !1;
                    }.bind(this))))), this.on_refresh();
                },
                adOnClose: function(a) {
                    o.videoAd.bind_this.after_ad(a);
                },
                after_ad: function(a) {
                    var e = r.system_info.SDKVersion || "";
                    !l.sdkVersionGreater(e, "2.1.0") || null != a && a.isEnded ? (l.log_event("video_ad_9_5", {}), o.releaseVideo(), 
                    this.after_ad_finish(), l.log_event("click_wheel_ad_finish", {})) : this.scheduleOnce(function() {
                        try {
                            wx.showToast({
                                title: "看完广告抽奖"
                            });
                        } catch (a) {}
                    }.bind(this), .2);
                },
                on_ad_loaded: function() {
                    l.log_event("video_ad_9_2", {}), this.btnAd.getComponent(cc.Button).interactable = !0, this.on_refresh_ad_btn();
                },
                _set_remain_time: function(a) {
                    this.timeLabel.string = l.formatSeconds2(a), 0 >= a && (this.btnAd.getComponent(cc.Button).interactable = !0);
                },
                on_refresh_ad_btn: function() {
                    var d = a.get_int("wheel_next_ad"), e = l.getNow();
                    if (d <= e) this.timeLabel.node.active = !1; else {
                        this.btnAd.getComponent(cc.Button).interactable = !1, this.unscheduleAllCallbacks(), this.timeLabel.node.active = !0;
                        var _ = Math.ceil(d - e), t = _;
                        this.schedule(function() {
                            0 <= (t -= 1) && this._set_remain_time(t);
                        }, 1, _, 0);
                    }
                },
                on_refresh: function() {
                    this._inAction = !1;
                    var d = l.getToday(), e = "wheel_free:" + d, _ = a.get_int(e);
                    if (0 < r.wheel_free && _ < r.wheel_free) this.btnFree.active = !0, this.btnShare.active = !1; else {
                        this.btnFree.active = !1, this.btnShare.active = !0;
                        var u = "wheel_share:" + d, t = a.get_int(u);
                        this.btnShare.getComponent(cc.Button).interactable = 1 > t, r.io && !r.s_c || (this.btnShare.active = !1, 
                        this.btnAd.x = 0);
                    }
                    var i = "wheel_ad:" + d, s = a.get_int(i);
                    this.btnAd.getComponent(cc.Button).interactable = !1, s >= r.wheel_ad_times ? (this.btnAdLabel.string = "今日次数已用尽", 
                    this.timeLabel.node.active = !1) : (this.btnAdLabel.string = "今日还剩" + (r.wheel_ad_times - s) + "次", 
                    o.createVideo(this, 9), this.on_refresh_ad_btn());
                },
                on_click_share: function() {
                    this._inAction || i.shareToWx(4);
                },
                on_click_ad: function() {
                    this._inAction || (l.log_event("video_ad_9_3", {}), l.log_event("click_wheel_ad", {}), o.showVideo());
                },
                after_share: function() {
                    this.scheduleOnce(function() {
                        var d = "wheel_share:" + l.getToday();
                        a.set_data(d, 1), this.on_click_start();
                    }.bind(this), .8);
                },
                after_ad_finish: function() {
                    this.scheduleOnce(function() {
                        var d = "wheel_ad:" + l.getToday(), e = a.get_int(d);
                        if (a.set_data(d, e + 1), 0 < r.wheel_ad_cd) {
                            var i = l.getNow() + r.wheel_ad_cd;
                            a.set_data("wheel_next_ad", i);
                        }
                        this.on_click_start();
                    }.bind(this), .8);
                },
                on_click_free: function() {
                    this._inAction || (this.on_click_start(), this.scheduleOnce(function() {
                        var d = "wheel_free:" + l.getToday(), e = a.get_int(d);
                        a.set_data(d, e + 1);
                    }.bind(this), .8));
                },
                on_click_start: function() {
                    this._inAction = !0, this.pointer.rotation = 0;
                    var a = l.weightingChoice(r.wheel_weight) - 1, e = cc.rotateBy(6, [ 5, 47, 92, 137, 182, 227, 272, 317 ][a] + 4320 + l.getRandomInt(1, 35));
                    this.pointer.runAction(e).easing(cc.easeCubicActionOut(6)), this.scheduleOnce(function() {
                        this.show_bonus(a);
                    }.bind(this), 6.5);
                },
                show_bonus: function(d) {
                    this._inAction = !1, this.bonusNode.active = !0;
                    var e = this.bonusNode.getChildByName("sun");
                    e.stopAllActions(), e.runAction(cc.repeatForever(cc.rotateBy(1, 40)));
                    var _ = this.bonusNode.getChildByName("bg");
                    _.on(cc.Node.EventType.TOUCH_START, function() {}, this), _.on(cc.Node.EventType.TOUCH_MOVE, function() {}, this), 
                    _.on(cc.Node.EventType.TOUCH_END, this.onBonusTouchMaskEnd, this), _.on(cc.Node.EventType.TOUCH_CANCEL, this.onBonusTouchMaskEnd, this);
                    var o = this.bonusNode.getChildByName("content"), t = 0, i = "";
                    if (0 == d) {
                        t = 5, i = "单局10级开始";
                        var s = r.wheel_buff || 0;
                        r.on_update_wheel_buff(s + 1);
                    } else if (2 == d || 4 == d || 6 == d) t = 0, i = "获得金币", 2 == d ? (i += "x40", 
                    r.on_update_gold(r.gold + 40)) : 4 != d && 6 != d || (i += "x80", r.on_update_gold(r.gold + 80)); else if (1 == d) {
                        t = 1, i = "单局武器长度+3%";
                        var n = r.skills_halo[0] || 0;
                        r.on_update_skill_halo(0, n + 1);
                    } else 3 == d ? (t = 2, i = "单局移动速度+3%", n = r.skills_halo[1] || 0, r.on_update_skill_halo(1, n + 1)) : 5 == d ? (t = 3, 
                    i = "单局挥刀速度+3%", n = r.skills_halo[2] || 0, r.on_update_skill_halo(2, n + 1)) : 7 == d && (t = 4, i = "复活币x1", 
                    r.set_coin_revive(r.coin_revive + 1));
                    for (var a = 0; 5 >= a; a++) o.getChildByName("ico_" + a).active = a == t;
                    o.getChildByName("label").getComponent(cc.Label).string = i;
                },
                onBonusTouchMaskEnd: function() {
                    this.bonusNode.active = !1, this.on_refresh();
                },
                on_click_close: function() {
                    var a = this.node.parent.getComponent("GameOver");
                    null != a && a.on_update_gold();
                    var e = this.node.parent.getComponent("HomeUI");
                    null != e && e.on_update_coin_revive(), this.node.active = !1;
                }
            })