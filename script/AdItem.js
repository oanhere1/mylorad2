var d = require("GlobalData");
cc.Class({
    extends: cc.Component,
    properties: {
        icon: cc.Sprite,
        title: cc.Label,
        _out_id: 2
    },
    init: function (a) {
        var i = this;
        this._out_id = a;
        var e = d.cdnUrl + "daojian/ad_item_" + this._out_id + ".png";
        cc.loader.load({
            url: e,
            type: "png"
        }, function (a, e) {
            null != i.icon && (i.icon.spriteFrame = new cc.SpriteFrame(e));
        });
        var n = d.ad_list_items[this._out_id];
        n && n[1] && (this.title.string = n[1]);
    },
    on_click: function () {
        var a = d.ad_list_items[this._out_id];
        if (a && !(3 > a.length)) {
            var e = a[0], t = a[2];
            wx.navigateToMiniProgram({
                appId: e,
                path: t,
                extraData: "",
                success: function () {
                    console.log("AdItem.on_click.success: " + this._out_id);
                }.bind(this),
                fail: function () {
                    console.log("AdItem.on_click.fail: " + this._out_id);
                }.bind(this)
            });
        }
    },
    start: function () { }
})