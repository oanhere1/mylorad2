var d = require("Storage"),
    i = require("wxSdk"),
    o = require("Const");
module.exports = {
    baseUrl: "https://xyx.45app.com/",
    cdnUrl: "https://xyx.45app.com/",
    gameId:1,
    version: "v1.1.5",
    io: !0,
    s_c: !1,
    coin_revive: 1,
    share_diff: !1,
    share_diff_lv: 15,
    revive_mode: 1,
    bannerShowDeception: 0,
    bannerShowTwice: 0,
    bannerDelay: .3,
    bannerBtnPos: [-220, -140],
    bannerShowDeception_2: 0,
    bannerShowTwice_2: 0,
    bannerDelay_2: .3,
    bannerBtnPos_2: [-220, -140],
    share_time_delta: 3,
    share_succ_prob: 50,
    share_revive_req: 0,
    share_revive_max: 5,
    share_revive_coin: 0,
    gold_mode: {
        1: 1,
        2: 1
    },
    out_share: !1,
    out_share_conf: {
        1: 0,
        2: 0
    },
    out_share_appids: {
        1: "wx5fff8998292bb3ea",
        2: "wxf4ac0077b6ffb1e7",
        3: "wxfcb3bf2298d6de64",
        4: "wx0e6edc9869e48ead",
        5: "wx3df1cf2a43a6b16d"
    },
    out_share_paths: {
        1: "",
        2: "?channel_id=10001",
        3: "",
        4: "?ald_media_id=2672&ald_link_key=7f5637a378dfd8c7&ald_position_id=0",
        5: ""
    },
    out_share_weight: {},
    out_share_events: {
        1: 1,
        2: 1,
        3: 1,
        4: 1,
        5: 1
    },
    out_share_types: {},
    ad_list: [2, 3, 6],
    ad_list_items: {
        2: ["wxf4ac0077b6ffb1", "测试游戏", "?channel_id=10001"],
        3: ["wxa2fd73a6fc7150", "测试游戏", "?ald_media_id=3725&ald_link_key=9605380242b4ff73&ald_position_id=0"],
        6: ["wx323844398d34ba", "测试游戏", ""]
    },
    ad_list_speed: 1,
    wheel_on: !0,
    wheel_free: 1,
    wheel_auto: 3,
    wheel_ad_times: 3,
    wheel_ad_cd: 0,
    wheel_weight: {
        1: 23,
        2: 12.5,
        3: 12.5,
        4: 12.5,
        5: 8.5,
        6: 12.5,
        7: 8.5,
        8: 10
    },
    today_buff: 5,
    today_buff_req: 3,
    today_buff_time: 3600,
    gold_delay: 1.5,
    today_invite: !1,
    ad_banner_ids: {
        1: "adunit-b129c08eb9e2b2ea",
        2: "adunit-b129c08eb9e2b2ea",
        4: "adunit-b129c08eb9e2b2ea"
    },
    ad_video_ids: {
        3: "adunit-cd4eb1f15621b21f",
        5: "adunit-cd4eb1f15621b21f",
        6: "adunit-cd4eb1f15621b21f",
        7: "adunit-cd4eb1f15621b21f",
        8: "adunit-cd4eb1f15621b21f",
        9: "adunit-cd4eb1f15621b21f",
        10: "adunit-cd4eb1f15621b21f"
    },
    gameOver: !1,
    avatar: null,
    gameCtrl: null,
    auto_new_game: !1,
    homeUICtrl: null,
    last_play: [],
    newuser: 1,
    cardid: 0,
    inviterid: "",
    gameSceneLoaded: !1,
    enteredAvatars: [],
    enteredFoods: [],
    msgQueueWorld: [],
    friendsQueue: [],
    friendsMsgQueue: [],
    friendsReqQueue: [],
    avatarAtlas_1: null,
    avatarAtlas_2: null,
    avatarAtlas_3: null,
    avatarAtlas_4: null,
    foodAtlas: null,
    weaponAtlas: null,
    userInfo: null,
    serverConfigs: null,
    play_times: 0,
    today_play_times: 0,
    share_weights: null,
    share_titles: null,
    best_info: [0, 0, 0],
    last_info: [0, 0, 0],
    total_info: [0, 0, 0],
    invite: 0,
    invite_num: 0,
    invite_claimed: 0,
    sound_on: 0,
    wheel_buff: 0,
    gold: 0,
    skills: [1, 1, 1],
    skills_halo: [0, 0, 0],
    maxlevel: 1,
    is_connected: function () {
        return null != this.userInfo && null != this.userInfo.openId;
    },
    refresh_user_info: function () {
        if (this.init_level(), this.init_gold(), this.init_wheel_buff(), this.init_skills(), this.init_skills_halo(),
            this.init_sound(), this.last_info = wx.getStorageSync("last_play") || [0, 0, 0], this.best_info = wx.getStorageSync("best_play") || [0, 0, 0],
            this.total_info = wx.getStorageSync("total_play") || [0, 0, 0], this.is_connected()) {
            var a = this.userInfo.u_level || 1;
            a > this.maxlevel && this.on_update_level(a);
            var e = this.userInfo.u_gold || 0;
            e > this.gold && this.on_update_gold(e);
            var d = this.userInfo.u_skill_1 || 1,
                _ = this.userInfo.u_skill_2 || 1,
                t = this.userInfo.u_skill_3 || 1;
            d > this.skills[0] && this.on_update_skill(0, d), _ > this.skills[1] && this.on_update_skill(1, _),
                t > this.skills[2] && this.on_update_skill(2, t), this.push_data_to_server();
        }
    },
    push_data_to_server: function () {
        var extInfo = {
            score: this.best_info[1],
            kill: this.best_info[0],
            level: this.maxlevel,
            gold: this.gold,
            skill_1: this.skills[0],
            skill_2: this.skills[1],
            skill_3: this.skills[2],
            last_score: this.last_play[1] || 0
        }
        i.request({
            url: this.baseUrl + "updateRole?port=9009",
            method: "GET",
            data:{
                roleName:"",
                serverId:1,
                gameId:this.gameId,
                openId:this.userInfo.openId,
                roleLevel:this.maxlevel,
                extInfo:extInfo
            },
            success: function () {},
            fail: function () {}
        });
    },
    init_sound: function () {
        this.sound_on = d.get_int("sound_on");
    },
    init_level: function () {
        var a = d.get_int("max_unlock");
        0 < a && (this.maxlevel = a);
    },
    on_update_level: function (a) {
        var e = Math.floor(a);
        e > this.maxlevel && (this.maxlevel = e, d.set_data("max_unlock", this.maxlevel));
    },
    init_gold: function () {
        this.gold = d.get_int("gold");
    },
    on_update_gold: function (a) {
        this.gold = Math.floor(a), d.set_data("gold", this.gold);
    },
    init_skills: function () {
        var a = d.get_data("skills");
        if (null == a) this.skills = [1, 1, 1];
        else {
            var e = a.split(",");
            if (3 <= e.length) {
                for (var _, i = [], n = 0; 3 > n; n++) _ = Math.min(Math.floor(e[n]), o.MAX_SKILL_LV),
                    i.push(_);
                this.skills = i;
            } else this.skills = [1, 1, 1];
        }
    },
    on_update_skill: function (a, e) {
        this.skills[a] = Math.min(e, o.MAX_SKILL_LV);
        for (var i = "", _ = 0; _ < this.skills.length; _++) i += this.skills[_] + ",";
        d.set_data("skills", i);
    },
    init_skills_halo: function () {
        var a = d.get_data("skills_halo");
        if (null == a) this.skills_halo = [0, 0, 0];
        else {
            var e = a.split(",");
            if (3 <= e.length) {
                for (var _, i = [], n = 0; 3 > n; n++) _ = Math.min(Math.floor(e[n]), o.MAX_SKILL_LV),
                    i.push(_);
                this.skills_halo = i;
            } else this.skills_halo = [0, 0, 0];
        }
    },
    on_update_skill_halo: function (a, e) {
        this.skills_halo[a] = Math.min(e, o.MAX_SKILL_LV);
        for (var i = "", _ = 0; _ < this.skills_halo.length; _++) i += this.skills_halo[_] + ",";
        d.set_data("skills_halo", i);
    },
    init_wheel_buff: function () {
        this.wheel_buff = d.get_int("wheel_buff");
    },
    on_update_wheel_buff: function (a) {
        this.wheel_buff = Math.floor(a), d.set_data("wheel_buff", this.wheel_buff);
    },
    set_coin_revive: function (a) {
        this.coin_revive = a, wx.setStorageSync("coin_revive", [this.coin_revive]);
    },
    refresh_configs: function () {
        if (null != this.serverConfigs) {
            var a = this.serverConfigs.cards_weight;
            null != a && (this.share_weights = a);
            var e = this.serverConfigs.cards_title;
            null != e && (this.share_titles = e), this.io = 1 == this.serverConfigs[this.version],
                this.share_diff = 1 == this.serverConfigs.share_diff, null != this.serverConfigs.share_diff_lv && (this.share_diff_lv = Math.floor(this.serverConfigs.share_diff_lv)),
                this.revive_mode = this.serverConfigs.revive_mode || 1, this.share_revive_req = this.serverConfigs.sr_req || 0,
                this.share_revive_max = this.serverConfigs.sr_max || 3, this.share_revive_coin = this.serverConfigs.sr_coin || 0,
                this.gold_mode = this.serverConfigs.gold_mode || {
                    1: 1,
                    2: 1
                }, null != this.serverConfigs.out_share_appids && (this.out_share_appids = this.serverConfigs.out_share_appids), null != this.serverConfigs.out_share_paths && (this.out_share_paths = this.serverConfigs.out_share_paths),
                null != this.serverConfigs.out_share_weight && (this.out_share_weight = this.serverConfigs.out_share_weight), null != this.serverConfigs.out_share_events && (this.out_share_events = this.serverConfigs.out_share_events),
                null != this.serverConfigs.out_share_types && (this.out_share_types = this.serverConfigs.out_share_types), this.out_share = 1 == this.serverConfigs.out_share,
                null != this.serverConfigs.out_share_conf && (this.out_share_conf = this.serverConfigs.out_share_conf), this.serverConfigs.ad_list_2 && (this.ad_list = this.serverConfigs.ad_list_2),
                this.serverConfigs.ad_list_items && (this.ad_list_items = this.serverConfigs.ad_list_items), this.ad_list_speed = this.serverConfigs.ad_list_speed || 1,
                this.s_c = 1 == this.serverConfigs.s_c, this.wheel_on = 1 == this.serverConfigs.wheel_on,
                this.wheel_free = this.serverConfigs.wheel_free || 1, this.wheel_auto = this.serverConfigs.wheel_auto || 3,
                this.wheel_ad_times = this.serverConfigs.wheel_ad_times || 3, this.wheel_ad_cd = this.serverConfigs.wheel_ad_cd || 0,
                null != this.serverConfigs.wheel_weight && (this.wheel_weight = this.serverConfigs.wheel_weight), this.today_buff = this.serverConfigs.today_buff || 1,
                this.today_buff_req = this.serverConfigs.today_buff_req || 3, this.today_buff_time = this.serverConfigs.today_buff_time || 0,
                this.gold_delay = this.serverConfigs.gold_delay || 0, this.today_invite = this.serverConfigs.today_invite || !1;
            var d = this.serverConfigs.ad_banner_ids;
            if (null != d)
                for (var _ in d) d.hasOwnProperty(_) && (this.ad_banner_ids[_] = d[_]);
            var t = this.serverConfigs.ad_banner_ids_2;
            if (null != t)
                for (var _ in t) t.hasOwnProperty(_) && (this.ad_banner_ids[_] = t[_]);
            var i = this.serverConfigs.ad_video_ids;
            if (null != i)
                for (var _ in i) i.hasOwnProperty(_) && (this.ad_video_ids[_] = i[_]);
            this.bannerShowDeception = this.serverConfigs.banner_try || 0, this.bannerShowTwice = this.serverConfigs.banner_twice || 0,
                this.bannerDelay = this.serverConfigs.banner_delay || .3, this.bannerShowDeception_2 = this.serverConfigs.banner_try_2 || 0,
                this.bannerShowTwice_2 = this.serverConfigs.banner_twice_2 || 0, this.bannerDelay_2 = this.serverConfigs.banner_delay_2 || .3,
                this.io && !this.s_c || (this.bannerShowDeception = 0, this.bannerShowTwice = 0, this.bannerShowDeception_2 = 0,
                    this.bannerShowTwice_2 = 0), this.bannerBtnPos = this.serverConfigs.banner_btn_pos || [-220, -140], this.bannerBtnPos_2 = this.serverConfigs.banner_btn_pos_2 || [-220, -140],
                this.share_time_delta = this.serverConfigs.share_time_delta || 3, this.share_succ_prob = this.serverConfigs.share_succ_prob || 50;
        }
    },
    reset: function () {
        this.gameCtrl = null, this.gameSceneLoaded = !1, this.enteredAvatars = [], this.enteredFoods = [];
    }
}