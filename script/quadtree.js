
                function o(a, e, d, i) {
                    this.max_objects = e || 10, this.max_levels = d || 4, this.level = i || 0, this.bounds = a, 
                    this.objects = [], this.nodes = [];
                }
                o.prototype.split = function() {
                    var a = this.level + 1, e = Math.round(this.bounds.width / 2), n = Math.round(this.bounds.height / 2), s = Math.round(this.bounds.x), l = Math.round(this.bounds.y);
                    this.nodes[0] = new o({
                        x: s + e,
                        y: l,
                        width: e,
                        height: n
                    }, this.max_objects, this.max_levels, a), this.nodes[1] = new o({
                        x: s,
                        y: l,
                        width: e,
                        height: n
                    }, this.max_objects, this.max_levels, a), this.nodes[2] = new o({
                        x: s,
                        y: l + n,
                        width: e,
                        height: n
                    }, this.max_objects, this.max_levels, a), this.nodes[3] = new o({
                        x: s + e,
                        y: l + n,
                        width: e,
                        height: n
                    }, this.max_objects, this.max_levels, a);
                }, o.prototype.getIndex = function(a) {
                    var e = -1, d = this.bounds.x + this.bounds.width / 2, _ = this.bounds.y + this.bounds.height / 2, t = a.y < _ && a.y + a.height < _, i = a.y > _;
                    return a.x < d && a.x + a.width < d ? t ? e = 1 : i && (e = 2) : a.x > d && (t ? e = 0 : i && (e = 3)), 
                    e;
                }, o.prototype.insert = function(a) {
                    var e, d = 0;
                    if (!(void 0 === this.nodes[0] || -1 === (e = this.getIndex(a)))) this.nodes[e].insert(a); else if (this.objects.push(a), 
                    this.objects.length > this.max_objects && this.level < this.max_levels) for (void 0 === this.nodes[0] && this.split(); d < this.objects.length; ) -1 === (e = this.getIndex(this.objects[d])) ? d += 1 : this.nodes[e].insert(this.objects.splice(d, 1)[0]);
                }, o.prototype.retrieve = function(a) {
                    var e = this.getIndex(a), d = this.objects;
                    if (void 0 !== this.nodes[0]) if (-1 !== e) d = d.concat(this.nodes[e].retrieve(a)); else for (var i = 0; i < this.nodes.length; i += 1) d = d.concat(this.nodes[i].retrieve(a));
                    return d;
                }, o.prototype.clear = function() {
                    this.objects = [];
                    for (var a = 0; a < this.nodes.length; a += 1) void 0 !== this.nodes[a] && this.nodes[a].clear();
                    this.nodes = [];
                }, window.Quadtree = o;