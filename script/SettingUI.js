
            var d= require("GlobalData"), _ = (require("Utils"), require("Storage"));
            cc.Class({
                extends: cc.Component,
                properties: {
                    toggleOpen: cc.Node,
                    toggleClose: cc.Node
                },
                on_open: function() {
                    this.toggleOpen.getComponent(cc.Toggle).isChecked = 0 == d.sound_on, this.toggleClose.getComponent(cc.Toggle).isChecked = 0 != d.sound_on;
                },
                on_click_close: function() {
                    this.node.active = !1;
                },
                on_click_open_sound: function() {
                    this._on_click(!0);
                },
                on_click_close_sound: function() {
                    this._on_click(!1);
                },
                _on_click: function(a) {
                    this.toggleOpen.getComponent(cc.Toggle).isChecked = a, this.toggleClose.getComponent(cc.Toggle).isChecked = !a, 
                    d.sound_on = a ? 0 : 1, _.set_data("sound_on", d.sound_on);
                },
                start: function() {}
            })