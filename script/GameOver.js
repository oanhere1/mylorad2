
            var o = require("GlobalData"), n = require("Utils"), a = require("Const"), l = require("VideoMgr"), t = require("Storage");
            cc.Class({
                extends: cc.Component,
                properties: {
                    bg1: cc.Node,
                    bg2: cc.Node,
                    bgGold: cc.Node,
                    contentLbl: cc.Label,
                    countdownLabel: cc.Label,
                    btnRevive: cc.Node,
                    btnWatch: cc.Node,
                    btnLeave: cc.Node,
                    btnWatchLabel: cc.Node,
                    btnAd: cc.Node,
                    btnAgain: cc.Node,
                    scoreNode: cc.Node,
                    killNode: cc.Node,
                    rankNodePrefab: cc.Prefab,
                    wheelNodePrefab: cc.Prefab,
                    subRankNode: cc.Node,
                    globalRankNode: cc.Node,
                    todayRankNode: cc.Node,
                    skillNode1: cc.Node,
                    skillNode2: cc.Node,
                    skillNode3: cc.Node,
                    todayBuffNode: cc.Node,
                    _gameoverAd: null,
                    _gameoverAd2: null,
                    _adResized: !1,
                    _score: 0,
                    _kill: 0,
                    _sysInfo: null,
                    _tabSelected: 1
                },
                onLoad: function() {
                    this._sysInfo = o.system_info, this.reviveAdLoaded = !1, this.goldAdLoaded = !1, this.reviveFlag = !1, 
                    this.goldFlag = !1;
                },
                on_refresh: function(a, e, d, _) {
                    var l = this;
                    if (this._score = d, this._kill = _, 1 > a) {
                        if (!(this.bg1.active = !0, this.bgGold.active = !1, this.bg2.active = !1, 
                        this.contentLbl.string = "您被玩家 " + e + " 击败！", this.refresh_countdown(), 1 > a)) this._only_show_skip(); else if (this.reviveFlag = n.getRandomInt(1, 100) <= o.bannerShowDeception, 
                        this.reviveFlag ? this.btnLeave.setPosition(0, o.bannerBtnPos[0]) : this.btnLeave.setPosition(0, o.bannerBtnPos[1]), 
                        0 < o.coin_revive) this.btnRevive.active = !0, this.btnRevive.getChildByName("coin_revive").getChildByName("value").getComponent(cc.Label).string = o.coin_revive, 
                        this.btnWatch.active = !1, this.btnWatchLabel.active = !1, this.btnAd.active = !1; else if (this.btnRevive.active = !1, 
                        o.s_c) n.is_ad_enabled() ? this._only_show_ad() : this._only_show_skip(); else if (!o.io) this._only_show_skip(); else if (1 == o.revive_mode) this._only_show_share(); else if (2 == o.revive_mode) n.is_ad_enabled() ? this._only_show_ad() : this._only_show_share(); else if (n.is_ad_enabled()) {
                            var i = "share_times:" + n.getToday();
                            t.get_int(i) < o.share_revive_req ? this._only_show_share() : this._show_both_share_ad();
                        } else this._only_show_share();
                        this.scheduleOnce(function() {
                            l.create_ad_ongameover(1);
                        }, o.bannerDelay);
                    } else 0 < this._kill ? this._show_gold() : this._show_end();
                },
                _only_show_skip: function() {
                    this.btnRevive.active = !1, this.btnWatch.active = !1, this.btnWatchLabel.active = !1, this.btnAd.active = !1, 
                    this.btnLeave.x = 0;
                },
                _only_show_share: function() {
                    this.btnWatch.active = !0, this.btnWatchLabel.active = !0, this.btnAd.active = !1, this.btnWatchLabel.getComponent(cc.Label).string = "求助群友帮忙复活一次", 
                    this.btnWatch.stopAllActions(), this.btnWatch.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(.3, 1.06), cc.scaleTo(.3, 1))));
                },
                _only_show_ad: function() {
                    this.btnWatch.active = !1, this.btnWatchLabel.active = !0, this.btnAd.active = !0, this.btnWatchLabel.getComponent(cc.Label).string = "看广告即可免费复活一次", 
                    this.bg1.getComponent("ReviveAd").init(this);
                },
                _show_both_share_ad: function() {
                    this.btnWatch.active = !0, this.btnWatchLabel.active = !0, this.btnWatch.x = -140, 
                    this.btnWatchLabel.getComponent(cc.Label).string = "求助群友或看广告免费复活一次", this.btnAd.active = !0, this.bg1.getComponent("ReviveAd").init(this), 
                    this.btnAd.x = 140, this.btnLeave.x = 0;
                },
                enable_ad_revive: function(a) {
                    (this.btnAd.getComponent(cc.Button).interactable = a) && (this.btnAd.stopAllActions(), this.btnAd.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(.3, 1.06), cc.scaleTo(.3, 1)))));
                },
                refresh_countdown: function() {
                    this.unscheduleAllCallbacks();
                    var a = 9;
                    this.schedule(function() {
                        0 <= (a -= 1) ? this.countdownLabel.string = a : this.on_click_skip();
                    }.bind(this), 1, 9, 0);
                },
                refresh_result_rank: function() {
                    var a = this.bg2.getChildByName("bg_rank");
                    a.active = !1, this.scheduleOnce(function() {
                        a.active = !0, a.getChildByName("rank").active = !1, a.getComponent("ResultRankUI").init();
                    }.bind(this), .3);
                },
                cancel_share: function() {
                    cc.log("cancel_share"), this.refresh_countdown();
                },
                create_ad_ongameover: function(a) {
                    var d = this, t = o.system_info.SDKVersion || "";
                    if (n.sdkVersionGreater(t, "2.0.4")) {
                        var e = 2 < this._sysInfo.pixelRatio ? 290 : 270, s = this._sysInfo.screenWidth / 2 - (2 < this._sysInfo.pixelRatio ? 290 : 270) / 2;
                        try {
                            if (null != this._gameoverAd) try {
                                this._gameoverAd.destroy();
                            } catch (e) {}
                            this._adResized = !1, this._gameoverAd = wx.createBannerAd({
                                adUnitId: o.ad_banner_ids[a],
                                style: {
                                    left: s,
                                    top: 76,
                                    width: e
                                }
                            }), this._gameoverAd.onResize(function() {
                                null != this._gameoverAd && (this._gameoverAd.style.top = this._sysInfo.screenHeight - this._gameoverAd.style.realHeight + 2);
                            }.bind(this)), this._gameoverAd.show().then(function() {
                                if (1 === a) {
                                    if (!d.reviveFlag) return;
                                    if (d.reviveAdLoaded) {
                                        if (d._gameoverAd2) try {
                                            d._gameoverAd2.destroy();
                                        } catch (a) {}
                                    } else if (n.getRandomInt(1, 100) <= o.bannerShowTwice) d._gameoverAd2 = d._gameoverAd, d._gameoverAd = null, 
                                    d.reviveAdLoaded = !0, d.btnLeave.setPosition(0, o.bannerBtnPos[1]), d.create_ad_ongameover(1); else {
                                        if (d._gameoverAd2) try {
                                            d._gameoverAd2.destroy();
                                        } catch (a) {}
                                        d.btnLeave.setPosition(0, o.bannerBtnPos[1]);
                                    }
                                } else if (4 === a) {
                                    if (!d.goldFlag) return;
                                    if (d.goldAdLoaded) {
                                        if (d._gameoverAd2) try {
                                            d._gameoverAd2.destroy();
                                        } catch (a) {}
                                    } else if (n.getRandomInt(1, 100) <= o.bannerShowTwice_2) {
                                        d._gameoverAd2 = d._gameoverAd, d._gameoverAd = null, d.goldAdLoaded = !0;
                                        var i = d.bgGold.getComponent("GoldBonusUI");
                                        i && i.btnGoldClaim && i.btnGoldClaim.setPosition(0, o.bannerBtnPos_2[1]), d.create_ad_ongameover(4);
                                    } else {
                                        if (d._gameoverAd2) try {
                                            d._gameoverAd2.destroy();
                                        } catch (a) {}
                                        var e = d.bgGold.getComponent("GoldBonusUI");
                                        e && e.btnGoldClaim && e.btnGoldClaim.setPosition(0, o.bannerBtnPos_2[1]);
                                    }
                                }
                            });
                        } catch (e) {}
                    }
                },
                destroy_ads: function() {
                    if (null != this._gameoverAd) {
                        try {
                            this._gameoverAd.hide(), this._gameoverAd.destroy();
                        } catch (a) {}
                        this._gameoverAd = null;
                    }
                    if (null != this._gameoverAd2) {
                        try {
                            this._gameoverAd2.hide(), this._gameoverAd2.destroy();
                        } catch (a) {}
                        this._gameoverAd2 = null;
                    }
                },
                onClickShare: function() {
                    null != o.gameCtrl && o.gameCtrl.play_audio(2), this.unscheduleAllCallbacks(), o.gameCtrl.onClickShare();
                },
                on_ad_revive: function(a) {
                    0 == a ? this.refresh_countdown() : (o.set_coin_revive(o.coin_revive + 1), o.gameCtrl.onClickRevive());
                },
                onClickAdRevive: function() {
                    null != o.gameCtrl && o.gameCtrl.play_audio(2), this.unscheduleAllCallbacks(), n.log_event("video_ad_7_3", {}), 
                    n.log_event("click_revive_ad", {}), this.destroy_ads(), l.showVideo();
                },
                onClickRevive: function() {
                    null != o.gameCtrl && o.gameCtrl.play_audio(2), this.unscheduleAllCallbacks(), o.avatar.is_alive || (this.destroy_ads(), 
                    o.gameCtrl.onClickRevive());
                },
                _show_gold: function() {
                    var a = this;
                    this.destroy_ads(), l.releaseVideo(), this.unscheduleAllCallbacks(), this.bg1.active = !1, this.bgGold.active = !0, 
                    this.bg2.active = !1, this.goldFlag = n.getRandomInt(1, 100) <= o.bannerShowDeception_2;
                    try {
                        this._gameoverAd && (this._gameoverAd.destroy(), this._gameoverAd = null), this._gameoverAd2 && this._gameoverAd2.destroy();
                    } catch (a) {}
                    this.scheduleOnce(function() {
                        a.create_ad_ongameover(4);
                    }, o.bannerDelay_2), this.bgGold.getComponent("GoldBonusUI").init(this, this._kill);
                    var e = this.bgGold.getComponent("GoldBonusUI");
                    e && e.btnGoldClaim && (this.goldFlag ? e.btnGoldClaim.setPosition(0, o.bannerBtnPos_2[0]) : e.btnGoldClaim.setPosition(0, o.bannerBtnPos_2[1]));
                },
                on_update_gold: function() {
                    this.bg2.getChildByName("coin_gold").getChildByName("value").getComponent(cc.Label).string = o.gold;
                },
                _show_ad_list: function(a) {
                    var e = cc.find("Canvas/revive/revive_bg_2/ad_list"), d = cc.find("Canvas/revive/revive_bg_2/bg_rank");
                    if (a) e.getComponent("AdListUI").init(), n.isFullScreen() && (d.getComponent(cc.Widget).left = 80, 
                    e.getComponent(cc.Widget).right = 80); else {
                        e.active = !1;
                        var i = d.getComponent(cc.Widget);
                        i.isAlignLeft = !1, i.isAlignRight = !0, i.right = 14;
                    }
                },
                _refresh_ad_list: function() {
                    var a = o.system_info.SDKVersion || "";
                    n.sdkVersionGreater(a, "2.2.0") ? this._show_ad_list(!0) : this._show_ad_list(!1);
                },
                _show_end: function() {
                    if (this.destroy_ads(), l.releaseVideo(), this.bg1.active = !1, this.bgGold.active = !1, 
                    this.bg2.active = !0, this.todayBuffNode.active = !1, this.unscheduleAllCallbacks(), this._tabSelected = 1, 
                    this._on_click_tab(), this.skillNode1.getComponent("SkillItem").init(1), this.skillNode2.getComponent("SkillItem").init(2), 
                    this.skillNode3.getComponent("SkillItem").init(3), this._refresh_ad_list(), this.on_update_gold(), 
                    this.bg2.getChildByName("btn_wheel").active = o.wheel_on, o.wheel_on) {
                        var a = this.bg2.getChildByName("btn_wheel");
                        a.getChildByName("pointer").runAction(cc.repeatForever(cc.rotateBy(.1, 50))), a.getChildByName("sun").runAction(cc.repeatForever(cc.rotateBy(.4, 50)));
                    }
                    this.scoreNode.getComponent(cc.Label).string = this._score, this.killNode.getComponent(cc.Label).string = this._kill;
                    var e = wx.getStorageSync("best_play") || [ 0, 0, 0 ], d = this.killNode.getChildByName("ico_new");
                    d.stopAllActions();
                    var _ = this.scoreNode.getChildByName("ico_new");
                    if (_.stopAllActions(), this._kill > e[0] ? (d.active = !0, d.runAction(cc.repeatForever(cc.sequence(cc.moveBy(.3, cc.v2(0, 5)), cc.moveBy(.3, cc.v2(0, -5)))))) : d.active = !1, 
                    this._score > e[1] ? (_.active = !0, _.runAction(cc.repeatForever(cc.sequence(cc.moveBy(.3, cc.v2(0, 5)), cc.moveBy(.3, cc.v2(0, -5)))))) : _.active = !1, 
                    n.push_record(), this.btnAgain.stopAllActions(), this.btnAgain.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(.3, 1.06), cc.scaleTo(.3, 1)))), 
                    0 < o.wheel_auto && 0 == o.today_play_times % o.wheel_auto && 0 < o.today_play_times) {
                        var s = "wheel_ad:" + n.getToday();
                        t.get_int(s) < o.wheel_ad_times && this.on_click_wheel();
                    }
                },
                on_click_skip: function() {
                    null != o.gameCtrl && o.gameCtrl.play_audio(2), this.destroy_ads(), this.scheduleOnce(function() {
                        0 < this._kill ? this._show_gold() : this._show_end();
                    }.bind(this), .1);
                },
                on_click_share: function() {
                    null != o.gameCtrl && o.gameCtrl.play_audio(2);
                    var d = n.getRandomInt(1, a.SHARE_NUMS);
                    null != o.share_weights && (d = Math.floor(n.weightingChoice(o.share_weights))), wx.shareAppMessage({
                        title: "我得了" + this._score + "分，你敢来挑战吗？",
                        imageUrl: o.cdnUrl + "share_cards/campaign_" + d + ".png",
                        success: function() {
                            this.scheduleOnce(function() {
                                wx.showToast({
                                    title: "分享成功"
                                });
                            }.bind(this), .5), n.log_event("share_1", {
                                stype: "4"
                            });
                        }.bind(this),
                        fail: function() {
                            n.log_event("share_0", {
                                stype: "4"
                            });
                        }.bind(this)
                    });
                },
                on_click_again: function() {
                    null != o.gameCtrl && o.gameCtrl.play_audio(2), o.auto_new_game = !0, n.log_event("try_again", {});
                    var a = "today_buff:" + n.getToday(), e = t.get_int(a);
                    o.today_play_times >= o.today_buff_req && 1 < o.today_buff && 0 == e ? (this.todayBuffNode.active = !0, this.todayBuffNode.y = 2e3, 
                    this.todayBuffNode.getComponent("TodayBuffUI").init(this), this.scheduleOnce(function() {
                        10 == l.cur_adid && l.videoAdLoaded ? this.todayBuffNode.y = 0 : (this.todayBuffNode.active = !1, 
                        this.on_click_return());
                    }.bind(this), 1)) : this.on_click_return();
                },
                on_today_buff_back: function() {
                    this.todayBuffNode.active = !1, this.on_click_return();
                },
                on_click_return: function() {
                    null != o.gameCtrl && o.gameCtrl.play_audio(2), this.destroy_ads(), o.gameCtrl.onClickReturn();
                },
                on_click_all_rank: function() {
                    null != o.gameCtrl && o.gameCtrl.play_audio(2), this.destroy_ads(), o.homeUICtrl._friendRankNeedsUpdate = !0;
                    var a = this.node.getChildByName("rank_bg");
                    null == a && (a = cc.instantiate(this.rankNodePrefab), this.node.addChild(a)), a.active = !0, 
                    a.getComponent("RankUI").on_open();
                },
                _on_click_tab: function() {
                    1 == this._tabSelected ? (this.globalRankNode.active = !1, this.todayRankNode.active = !1, this.subRankNode.active = !0, 
                    this.refresh_result_rank()) : 2 == this._tabSelected ? (n.log_event("click_global_rank", {}), this.bg2.getChildByName("bg_rank").active = !0, 
                    this.globalRankNode.active = !0, this.todayRankNode.active = !1, this.subRankNode.active = !1, this.globalRankNode.getChildByName("rank").getComponent("GlobalRankUI").on_refresh(!0)) : 3 == this._tabSelected && (n.log_event("click_today_rank", {}), 
                    this.bg2.getChildByName("bg_rank").active = !0, this.globalRankNode.active = !1, this.todayRankNode.active = !0, 
                    this.subRankNode.active = !1, this.todayRankNode.getChildByName("rank").getComponent("TodayRankUI").on_refresh(!0));
                },
                on_click_tab: function(a, e) {
                    null != o.gameCtrl && o.gameCtrl.play_audio(2), this._tabSelected = e, this._on_click_tab();
                },
                on_click_wheel: function() {
                    var a = this.node.getChildByName("wheel_bg");
                    null == a && (a = cc.instantiate(this.wheelNodePrefab), this.node.addChild(a)), a.active = !0, 
                    n.log_event("click_wheel_2", {}), a.getComponent("WheelUI").on_open();
                },
                start: function() {}
            })