
            var d= require("JoystickCommon");
            cc.Class({
                extends: cc.Component,
                properties: {
                    dot: {
                        default: null,
                        type: cc.Node,
                        displayName: "摇杆节点"
                    },
                    _joyCom: {
                        default: null,
                        displayName: "joy Node"
                    },
                    _playerNode: {
                        default: null,
                        displayName: "被操作的目标Node"
                    },
                    _angle: {
                        default: null,
                        displayName: "当前触摸的角度"
                    },
                    _radian: {
                        default: null,
                        displayName: "弧度"
                    },
                    _speed: 0,
                    speed1: 5,
                    speed2: 5,
                    _opacity: 0
                },
                onLoad: function() {
                    this._joyCom = this.node.parent.getComponent("Joystick"), this._playerNode = this._joyCom.sprite, 
                    this._joyCom.touchType == d.TouchType.DEFAULT && this._initTouchEvent();
                },
                _initTouchEvent: function() {
                    this.node.on(cc.Node.EventType.TOUCH_START, this._touchStartEvent, this), 
                    this.node.on(cc.Node.EventType.TOUCH_MOVE, this._touchMoveEvent, this), this.node.on(cc.Node.EventType.TOUCH_END, this._touchEndEvent, this), 
                    this.node.on(cc.Node.EventType.TOUCH_CANCEL, this._touchEndEvent, this);
                },
                _getDistance: function(a, e) {
                    return Math.sqrt(Math.pow(a.x - e.x, 2) + Math.pow(a.y - e.y, 2));
                },
                _getRadian: function(a) {
                    return this._radian = Math.PI / 180 * this._getAngle(a), this._radian;
                },
                _getAngle: function(a) {
                    var e = this.node.getPosition();
                    this._angle = Math.atan2(a.y - e.y, a.x - e.x) * (180 / Math.PI);
                    var d = 360 - (this._angle + 360) % 360;
                    return null != this._playerNode && this._playerNode.getComponent("Player").setAngle(d), this._angle;
                },
                _setSpeed: function(a) {
                    this._getDistance(a, this.node.getPosition()) < this._radius ? this._realSetSpeed(this.speed1) : this._realSetSpeed(this.speed2);
                },
                _realSetSpeed: function(a) {
                    this._speed = a, null != this._playerNode && this._playerNode.getComponent("Player").setSpeed(this._speed);
                },
                _touchStartEvent: function(a) {
                    var e = this.node.convertToNodeSpaceAR(a.getLocation()), d = this._getDistance(e, cc.v2(0, 0)), _ = this.node.width / 2;
                    this._stickPos = e;
                    var t = this.node.getPosition().x + e.x, i = this.node.getPosition().y + e.y;
                    return d < _ && (this.dot.setPosition(cc.v2(t, i)), !0);
                },
                _touchMoveEvent: function(d) {
                    var e = this.node.convertToNodeSpaceAR(d.getLocation()), _ = this._getDistance(e, cc.v2(0, 0)), o = this.node.width / 2, t = this.node.getPosition().x + e.x, i = this.node.getPosition().y + e.y;
                    if (_ < o) this.dot.setPosition(cc.v2(t, i)); else {
                        var s = this.node.getPosition().x + Math.cos(this._getRadian(cc.v2(t, i))) * o, n = this.node.getPosition().y + Math.sin(this._getRadian(cc.v2(t, i))) * o;
                        this.dot.setPosition(cc.v2(s, n));
                    }
                    this._getAngle(cc.v2(t, i)), this._setSpeed(cc.v2(t, i));
                },
                _touchEndEvent: function() {
                    this.dot.setPosition(this.node.getPosition()), this._realSetSpeed(0);
                }
            })