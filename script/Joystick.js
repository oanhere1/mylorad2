
            var d= require("JoystickCommon"), _= require("JoystickBg");
            cc.Class({
                extends: cc.Component,
                properties: {
                    dot: {
                        default: null,
                        type: cc.Node,
                        displayName: "摇杆节点"
                    },
                    ring: {
                        default: null,
                        type: _,
                        displayName: "摇杆背景节点"
                    },
                    stickX: {
                        default: 0,
                        displayName: "摇杆X位置"
                    },
                    stickY: {
                        default: 0,
                        displayName: "摇杆Y位置"
                    },
                    touchType: {
                        default: d.TouchType.DEFAULT,
                        type: d.TouchType,
                        displayName: "触摸类型"
                    },
                    directionType: {
                        default: d.DirectionType.ALL,
                        type: d.DirectionType,
                        displayName: "方向类型"
                    },
                    sprite: {
                        default: null,
                        type: cc.Node,
                        displayName: "操控的目标"
                    },
                    _stickPos: {
                        default: null,
                        type: cc.Node,
                        displayName: "摇杆当前位置"
                    },
                    _touchLocation: {
                        default: null,
                        type: cc.Node,
                        displayName: "摇杆当前位置"
                    }
                },
                onLoad: function() {
                    this._createStickSprite(), this.touchType == d.TouchType.FOLLOW && this._initTouchEvent();
                },
                _createStickSprite: function() {
                    this.ring.node.setPosition(this.stickX, this.stickY), this.dot.setPosition(this.stickX, this.stickY);
                },
                _initTouchEvent: function() {
                    this.node.on(cc.Node.EventType.TOUCH_START, this._touchStartEvent, this), 
                    this.node.on(cc.Node.EventType.TOUCH_MOVE, this._touchMoveEvent, this), this.node.on(cc.Node.EventType.TOUCH_END, this._touchEndEvent, this), 
                    this.node.on(cc.Node.EventType.TOUCH_CANCEL, this._touchEndEvent, this);
                },
                _touchStartEvent: function(a) {
                    var e = a.getLocation(), d = this.node.convertToNodeSpaceAR(e);
                    return !(400 < cc.v2(-200, -200).sub(d).mag()) && void (this._touchLocation = e, 
                    this.ring.node.setPosition(d), this.dot.setPosition(d), this._stickPos = d);
                },
                _touchMoveEvent: function(d) {
                    if (null == this._touchLocation) return !1;
                    var e = d.getLocation();
                    if (this._touchLocation.x == e.x && this._touchLocation.y == e.y) return !1;
                    var _ = this.ring.node.convertToNodeSpaceAR(e), o = this.ring._getDistance(_, cc.v2(0, 0)), t = this.ring.node.width / 2, i = this._stickPos.x + _.x, s = this._stickPos.y + _.y;
                    if (o < t) this.dot.setPosition(cc.v2(i, s)); else {
                        var n = this._stickPos.x + Math.cos(this.ring._getRadian(cc.v2(i, s))) * t, a = this._stickPos.y + Math.sin(this.ring._getRadian(cc.v2(i, s))) * t;
                        this.dot.setPosition(cc.v2(n, a));
                    }
                    this.ring._getAngle(cc.v2(i, s)), this.ring._setSpeed(cc.v2(i, s));
                },
                _touchEndEvent: function() {
                    this.dot.setPosition(this.ring.node.getPosition()), this.ring._realSetSpeed(0), 
                    this.touchType == d.TouchType.FOLLOW && (this.ring.node.setPosition(this.stickX, this.stickY), 
                    this.dot.setPosition(this.stickX, this.stickY)), this._touchLocation = null;
                },
                reset: function() {
                    this._touchEndEvent();
                }
            })