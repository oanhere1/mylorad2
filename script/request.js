
            var k = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(a) {
                return typeof a;
            } : function(a) {
                return a && "function" == typeof Symbol && a.constructor === Symbol && a !== Symbol.prototype ? "symbol" : typeof a;
            }, s= require("constants"), l= require("wxutils"), d= require("session"), i= require("login"), u = function() {}, n = function() {
                function a(a, d) {
                    Error.call(this, d), this.type = a, this.message = d;
                }
                return a.prototype = new Error(), a.prototype.constructor = a;
            }();
            module.exports = {
                RequestError: n,
                request: function(p) {
                    function f() {
                        i.loginWithCode({
                            success: g,
                            fail: a
                        });
                    }
                    function g() {
                        var i, e, _ = {}, o = d.get();
                        o && (i = o.skey, e = {}, i && (e[s.WX_HEADER_SKEY] = i), _ = e), wx.request(l.extend({}, p, {
                            header: l.extend({}, b, _),
                            success: function(i) {
                                var e, _, o = i.data;
                                return o && -1 === o.code || 401 === i.statusCode ? (d.clear(), S ? (_ = "登录态已过期", 
                                e = new n(o.error, _), void a(e)) : (S = !0, void f())) : void y.apply(null, arguments);
                            },
                            fail: a,
                            complete: u
                        }));
                    }
                    if ("object" !== (void 0 === p ? "undefined" : k(p))) {
                        var h = "请求传参应为 object 类型，但实际传了 " + (void 0 === p ? "undefined" : k(p)) + " 类型";
                        throw new n(s.ERR_INVALID_PARAMS, h);
                    }
                    var e = p.login, _ = p.success || u, m = p.fail || u, t = p.complete || u, b = p.header || {}, y = function() {
                        _.apply(null, arguments), t.apply(null, arguments);
                    }, a = function(a) {
                        m.call(null, a), t.call(null, a);
                    }, S = !1;
                    e ? f() : g();
                }
            }