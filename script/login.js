function o(a, e, d) {
    return e in a ? Object.defineProperty(a, e, {
    value: d,
    enumerable: !0,
    configurable: !0,
    writable: !0
    }) : a[e] = d, a;
    }
            var s = require("constants"), n = require("session"), a = function() {}, l = {
                method: "GET",
                success: a,
                fail: a,
                loginUrl: null
            };
            module.exports = {
                login: function(a) {
                    if (!(a = Object.assign({}, l, a)).loginUrl) return a.fail(new Error("登录错误：缺少登录地址，请通过 setLoginUrl() 方法设置登录地址"));
                    var d;
                    d = function(d, e) {
                        var i;
                        if (d) return a.fail(d);
                        var _ = (o(i = {}, s.WX_HEADER_CODE, e.code), o(i, s.WX_HEADER_ENCRYPTED_DATA, e.encryptedData), 
                        o(i, s.WX_HEADER_IV, e.iv), i);
                        console.log("e.code "+e.code);
                        wx.request({
                            url: a.loginUrl,
                            data:{
                                code:e.code,
                                encryptedData:e.encryptedData
                            },
                            success: function(d) {
                                var e = d.data;
                                if (!e || 0 !== e.code || !e.data || !e.data.skey) return a.fail(new Error("响应错误，" + JSON.stringify(e)));
                                var i = e.data;
                                return i && i.userinfo ? void (n.set(i), a.success(i.userinfo, i.configs)) : a.fail(new Error("登录失败(" + e.error + ")：" + e.message));
                            },
                            fail: function(d) {
                                console.error("登录失败，可能是网络错误或者服务器发生异常"), a.fail(d);
                            }
                        });
                    }, wx.login({
                        success: function(a) {
                            wx.getUserInfo({
                                success: function(i) {
                                    d(null, {
                                        code: a.code,
                                        encryptedData: i.encryptedData,
                                        iv: i.iv,
                                        userInfo: i.userInfo
                                    });
                                },
                                fail: function() {
                                    d(new Error("获取微信用户信息失败，请检查网络状态"), null);
                                }
                            });
                        },
                        fail: function() {
                            d(new Error("微信登录失败，请检查网络状态"), null);
                        }
                    });
                },
                login_2: function(a) {
                    var d;
                    if (!(a = Object.assign({}, l, a)).loginUrl) return a.fail(new Error("登录错误：缺少登录地址，请通过 setLoginUrl() 方法设置登录地址"));
                    if (!a.res) return a.fail(new Error("请先wx.login"));
                    var e = a.res, i = (o(d = {}, s.WX_HEADER_CODE, a.code), o(d, s.WX_HEADER_ENCRYPTED_DATA, e.encryptedData), 
                    o(d, s.WX_HEADER_IV, e.iv), d);
                    wx.request({
                        url: a.loginUrl,
                        header: i,
                        method: a.method,
                        success: function(d) {
                            var e = d.data;
                            if (!e || 0 !== e.code || !e.data || !e.data.skey) return a.fail(new Error("响应错误，" + JSON.stringify(e)));
                            var i = e.data;
                            return (cc.log(i), !i || !i.userinfo) ? a.fail(new Error("登录失败(" + e.error + ")：" + e.message)) : void (n.set(i), 
                            a.success(i.userinfo, i.configs));
                        },
                        fail: function(d) {
                            console.error("登录失败，可能是网络错误或者服务器发生异常"), a.fail(d);
                        }
                    });
                },
                setLoginUrl: function(a) {
                    l.loginUrl = a;
                },
                loginWithCode: function(a) {
                    return (a = Object.assign({}, l, a)).loginUrl ? void wx.login({
                        success: function(d) {
                            var e = o({}, s.WX_HEADER_CODE, d.code);
                            wx.request({
                                url: a.loginUrl,
                                header: e,
                                method: a.method,
                                success: function(d) {
                                    var e = d.data;
                                    if (cc.log(e), !e || 0 !== e.code || !e.data || !e.data.skey) return a.fail(new Error("用户未登录过，请先使用 login() 登录"));
                                    var i = e.data;
                                    return i && i.userinfo ? void (n.set(i), a.success(i.userinfo, i.configs)) : a.fail(new Error("登录失败(" + e.error + ")：" + e.message));
                                },
                                fail: function(d) {
                                    console.error("登录失败，可能是网络错误或者服务器发生异常"), a.fail(d);
                                }
                            });
                        }
                    }) : a.fail(new Error("登录错误：缺少登录地址，请通过 setLoginUrl() 方法设置登录地址"));
                },
                loginWithCode_2: function(a) {
                    if (!(a = Object.assign({}, l, a)).loginUrl) return a.fail(new Error("登录错误：缺少登录地址，请通过 setLoginUrl() 方法设置登录地址"));
                    if (!a.code) return a.fail(new Error("缺少code"));
                    var d = o({}, s.WX_HEADER_CODE, a.code);
                    wx.request({
                        url: a.loginUrl,
                        header: d,
                        method: "GET",
                        data:{
                            code:a.code
                        },
                        success: function(d) {
                            var e = d.data;
                            if (cc.log(e), !e || 0 !== e.code || !e.data || !e.data.skey) return a.fail(new Error("用户未登录过，请先使用 login() 登录"));
                            var i = e.data;
                            return i && i.userinfo ? void (n.set(i), a.success(i.userinfo, i.configs)) : a.fail(new Error("登录失败(" + e.error + ")：" + e.message));
                        },
                        fail: function(d) {
                            console.error("登录失败，可能是网络错误或者服务器发生异常"), a.fail(d);
                        }
                    });
                }
            }