 cc.Class({
                extends: cc.Component,
                properties: {
                    pressedScale: .9,
                    transDuration: .1,
                    clickCd: 0,
                    _lastClickTime: 0
                },
                onLoad: function() {
                    function a() {
                        0 != e.button.interactable && (this.stopAllActions(), this.runAction(e.scaleUpAction));
                    }
                    var e = this;
                    e.initScale = this.node.scale, e.button = e.getComponent(cc.Button), e.scaleDownAction = cc.scaleTo(e.transDuration, e.pressedScale), 
                    e.scaleUpAction = cc.scaleTo(e.transDuration, e.initScale), this.node.on("touchstart", function() {
                        if (0 != e.button.interactable) {
                            var a = new Date() / 1e3;
                            a - e._lastClickTime < e.clickCd || (e._lastClickTime = a, this.stopAllActions(), this.runAction(e.scaleDownAction));
                        }
                    }, this.node), this.node.on("touchend", a, this.node), this.node.on("touchcancel", a, this.node);
                }
            })