
            var d= require("GlobalData"), _= require("Utils");
            cc.Class({
                extends: cc.Component,
                properties: {
                    subRankNode: cc.Node,
                    globalRankNode: cc.Node,
                    todayRankNode: cc.Node,
                    tabNode: cc.Node,
                    titleLabel: cc.Label,
                    _tabSelected: 1
                },
                onLoad: function() {},
                start: function() {},
                on_open: function() {
                    this._tabSelected = 1, this._on_click_tab();
                },
                _on_click_tab: function() {
                    1 == this._tabSelected ? (this.globalRankNode.active = !1, this.todayRankNode.active = !1, this.subRankNode.active = !0, 
                    this.subRankNode.getComponent("SubRankUI").init(), this.titleLabel.string = "好友排行") : 2 == this._tabSelected ? (_.log_event("click_global_rank", {}), 
                    this.globalRankNode.active = !0, this.todayRankNode.active = !1, this.subRankNode.active = !1, this.globalRankNode.getComponent("GlobalRankUI").on_refresh(), 
                    this.titleLabel.string = "全国排行") : 3 == this._tabSelected && (_.log_event("click_today_rank", {}), this.globalRankNode.active = !1, 
                    this.todayRankNode.active = !0, this.subRankNode.active = !1, this.todayRankNode.getComponent("TodayRankUI").on_refresh(), 
                    this.titleLabel.string = "今日最强");
                },
                on_click_tab: function(a, e) {
                    var i = this.node.parent.getComponent("GameOver");
                    null != i && null != i ? null != d.gameCtrl && d.gameCtrl.play_audio(2) : null != d.homeUICtrl && d.homeUICtrl.play_audio(0), 
                    this._tabSelected = e, this._on_click_tab();
                },
                onClickClose: function() {
                    var a = this.node.parent.getComponent("GameOver");
                    null != a && null != a ? (a.refresh_result_rank(), null != d.gameCtrl && d.gameCtrl.play_audio(2)) : null != d.homeUICtrl && d.homeUICtrl.play_audio(0), 
                    this.node.active = !1;
                },
                update: function() {}
            })