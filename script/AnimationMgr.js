 module.exports = {
                inited: !1,
                allLoaded: !1,
                unitAtlas: [],
                resLoadedStatus: [],
                curArmyId: 1,
                check_loaded: function() {
                    for (var a = 1; 8 >= a; a++) if (1 != this.resLoadedStatus[a]) return !1;
                    return this.allLoaded = !0, cc.log(this.unitAtlas), !0;
                },
                init: function() {
                    this.inited || this.load_one();
                },
                load_one: function() {
                    this.resLoadedStatus[this.curArmyId] = !1, cc.loader.loadRes("textures/army/" + this.curArmyId, cc.SpriteAtlas, function(a, e) {
                        this.unitAtlas[this.curArmyId] = e, this.resLoadedStatus[this.curArmyId] = !0, this.check_loaded(), this.curArmyId += 1, 
                        8 >= this.curArmyId && this.load_one();
                    }.bind(this));
                }
            }