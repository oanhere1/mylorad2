
            var d= require("Utils"), t= require("GlobalData"), i= require("Storage"), o = (require("Const"), require("VideoMgr"));
            require("wxSdk"), cc.Class({
                extends: cc.Component,
                properties: {
                    btnClaim: cc.Node,
                    noteLabel: cc.Label,
                    noteLabel2: cc.Label,
                    _gameoverCtrl: null
                },
                init: function(a) {
                    this._gameoverCtrl = a, this.btnClaim.getComponent(cc.Button).interactable = !1, o.createVideo(this, 10), 
                    this.noteLabel.string = "看完视频以后每局出生即" + t.today_buff + "级";
                    var e = Math.floor(t.today_buff_time / 60);
                    this.noteLabel2.string = "(持续" + e + "分钟, 每日仅此1次)";
                },
                adOnClose: function(a) {
                    o.videoAd.bind_this.after_ad(a);
                },
                after_ad: function(a) {
                    var e = t.system_info.SDKVersion || "";
                    !d.sdkVersionGreater(e, "2.1.0") || null != a && a.isEnded ? (d.log_event("video_ad_10_5", {}), o.releaseVideo(), 
                    this.after_ad_finish()) : this.scheduleOnce(function() {
                        try {
                            wx.showToast({
                                title: "看完获得光环"
                            });
                        } catch (a) {}
                    }.bind(this), .2);
                },
                on_ad_loaded: function() {
                    d.log_event("video_ad_10_2", {}), this.btnClaim.getComponent(cc.Button).interactable = !0, this.btnClaim.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(.3, 1.06), cc.scaleTo(.3, 1))));
                },
                after_ad_finish: function() {
                    cc.log("after ad finish"), this._set_shown();
                    var a = d.getNow();
                    i.set_data("today_buff_until", a + t.today_buff_time), this.scheduleOnce(function() {
                        try {
                            wx.showToast({
                                title: "恭喜获得光环"
                            });
                        } catch (a) {}
                        this.on_click_close();
                    }.bind(this), .2);
                },
                _set_shown: function() {
                    var a = "today_buff:" + d.getToday();
                    i.set_data(a, 1);
                },
                start: function() {},
                on_click_ad: function() {
                    this.btnClaim.getComponent(cc.Button).interactable = !1, d.log_event("video_ad_10_3", {}), o.showVideo();
                },
                on_click_close: function() {
                    this._set_shown(), null == this._gameoverCtrl ? this.node.active = !1 : this._gameoverCtrl.on_today_buff_back();
                }
            })