
            var d= require("Utils");
            cc.Class({
                extends: cc.Component,
                properties: {
                    avatarSprite: cc.Sprite,
                    rankLabel: cc.Label,
                    nicknameLabel: cc.Label,
                    scoreLabel: cc.Label,
                    killLabel: cc.Label,
                    cityLabel: cc.Label,
                    maleNode: cc.Node,
                    femaleNode: cc.Node,
                    _rank: 0,
                    _info: null
                },
                init: function(o, e) {
                    var _ = this;
                    if (this._rank = o + 1, this._info = e, "" != e[2]) try {
                        var l = wx.createImage();
                        l.onload = function() {
                            try {
                                var a = new cc.Texture2D();
                                a.initWithElement(l), a.handleLoadedTexture(), _.avatarSprite.spriteFrame = new cc.SpriteFrame(a);
                            } catch (a) {
                                cc.log(a), _.avatarSprite.node.active = !1;
                            }
                        }, l.src = e[2];
                    } catch (a) {
                        cc.log(a), this.avatarSprite.node.active = !1;
                    }
                    if (3 < this._rank) this.rankLabel.node.active = !0, this.rankLabel.string = this._rank; else {
                        this.rankLabel.node.active = !1;
                        for (var t = 1; 3 >= t; t++) this.node.getChildByName("ico_rank_" + t).active = t == this._rank;
                    }
                    if (this.nicknameLabel.string = "" != e[1] && null != e[1] ? d.subString(e[1], 10, !0) : "荣耀勇士", 
                    this.scoreLabel.string = e[3], this.killLabel.string = e[4], 10 <= e.length) {
                        this.maleNode.active = 1 == e[6], this.femaleNode.active = 2 == e[6];
                        var i = e[7], s = e[8], n = e[9], a = "";
                        d.isHanzi(n) && ("中国" == n ? (d.isHanzi(s) && (a = s), d.isHanzi(i) && (a += i)) : a = n), 
                        "" != a && (this.cityLabel.string = a);
                    }
                },
                start: function() {}
            })