 cc.Class({
                extends: cc.Component,
                properties: {
                    targetNode: cc.Node,
                    _lastPosX: 0,
                    _lastPosY: 0
                },
                start: function() {},
                update: function() {
                    var e = this.targetNode.convertToWorldSpaceAR(cc.Vec2.ZERO);
                    (.5 < Math.abs(e.x - this._lastPosX) || .5 < Math.abs(e.y - this._lastPosY)) && (this.node.position = this.node.parent.convertToNodeSpaceAR(e)), 
                    this._lastPosX = e.x, this._lastPosY = e.y;
                }
            })