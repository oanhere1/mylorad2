 module.exports = {
     get_data: function (a) {
        var e = ""
        if (typeof (wx) !== "undefined") {
            e = wx.getStorageSync(a);
        }else{
            e = cc.sys.localStorage.getItem(a);;
        }
         return "" == e && (e = void 0), e;
     },
     get_int: function (a) {
        var e = ""
        if (typeof (wx) !== "undefined") {
            e = wx.getStorageSync(a);
        }else{
            e = cc.sys.localStorage.getItem(a);;
        }
         try {
             var d = parseInt(e);
             return isNaN(d) ? 0 : d;
         } catch (a) {
             return 0;
         }
     },
     set_data: function (a, e) {
        if (typeof (wx) !== "undefined") {
            wx.setStorageSync(a, "" + e);
        }else{
            cc.sys.localStorage.getItem(a,"" + e);
        }
     }
 }