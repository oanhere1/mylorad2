
            var d= require("avatar_data"), _= require("GlobalData"), l= require("Utils");
            cc.Class({
                extends: cc.Component,
                properties: {
                    nameLabel: cc.Label,
                    lvLabel: cc.Label,
                    avatarNode: cc.Node,
                    weaponSprite: cc.Sprite,
                    onbgNode: cc.Node,
                    offbgNode: cc.Node,
                    lockNode: cc.Node,
                    _aid: 0,
                    _maxUnlockedLv: 0
                },
                init: function(a, e) {
                    this._aid = a, this._maxUnlockedLv = e, this.onRefresh();
                },
                onRefresh: function() {
                    var a = d.data[this._aid];
                    if (null != a) {
                        this.lvLabel.string = "等级" + this._aid;
                        var e = this._aid <= this._maxUnlockedLv;
                        if (this.onbgNode.active = e, this.offbgNode.active = !e, this.avatarNode.active = e, 
                        this.weaponSprite.node.active = e, this.lockNode.active = !e, e) {
                            this.nameLabel.string = a.name;
                            var i = _.weaponAtlas.getSpriteFrame("weapon_" + this._aid);
                            null == i && (i = _.weaponAtlas.getSpriteFrame("weapon_1")), this.weaponSprite.spriteFrame = i, this.createClips(this.avatarNode, [ "Walk" ]), 
                            this.play("Walk");
                        } else this.nameLabel.string = "尚未解锁";
                    }
                },
                play: function(a) {
                    this.avatarNode.getComponent(cc.Animation).play(a);
                },
                get_atlas: function() {
                    return _.avatarAtlas_1;
                },
                createClips: function(d, e) {
                    for (var _ = 0; _ < e.length; _++) {
                        for (var o = e[_], t = [], i = 0; 6 > i; i++) {
                            var s = this._aid + "_" + o + "_" + l.zeroPad(i, 2), n = this.get_atlas().getSpriteFrame(s);
                            null != n && t.push(n);
                        }
                        var a = this.createClip(o, t, !0);
                        d.getComponent(cc.Animation).addClip(a);
                    }
                },
                createClip: function(a, e, d) {
                    var i = cc.AnimationClip.createWithSpriteFrames(e, 60);
                    return i.name = a, i.speed = .12, d && (i.wrapMode = cc.WrapMode.Loop), i;
                },
                start: function() {}
            })