
            var l = require("Const"), a = require("GlobalData"), r = require("Storage"), o = require("Utils");
            module.exports = {
                dailyFirstShare: !1,
                shareType: 0,
                startShareTime: -1,
                lastShareSucc: !1,
                skillId: -1,
                clearShareData: function() {
                    this.dailyFirstShare = !1, this.shareType = 0, this.startShareTime = -1, this.finishShareTime = -1;
                },
                onShareSucc: function(d) {
                    this.lastShareSucc = !0;
                    var e = "分享成功";
                    if (1 == d) {
                        e = "获得复活币";
                        var n = cc.find("Canvas");
                        if (a.io && !a.s_c && 1 == a.share_revive_coin) {
                            var l = "daily_coins:" + o.getToday(), t = r.get_int(l);
                            3 > t ? (i = n.getComponent("HomeUI")) && i.on_real_add_coin && (i.on_real_add_coin(), r.set_data(l, t + 1)) : e = "每天最多获得三枚";
                        } else e = "";
                    } else {
                        var i;
                        2 == d ? (e = "复活成功", n = cc.find("Canvas"), a.io ? (i = n.getComponent("Game")) && i.on_real_revive && i.on_real_revive() : e = "") : 3 == d ? (e = "", 
                        this.skillId && 1 <= this.skillId && 3 >= this.skillId && (n = cc.find("Canvas/revive/revive_bg_2/skill_node/skill_item_" + this.skillId)) && (i = n.getComponent("SkillItem")) && i.after_upgrade && i.after_upgrade()) : 4 == d && (e = "分享成功", 
                        (n = cc.find("Canvas/wheel_bg")) || (n = cc.find("Canvas/revive/wheel_bg")), n && (i = n.getComponent("WheelUI")) && i.after_share && i.after_share());
                    }
                    "" != e && wx.showToast({
                        title: e
                    });
                },
                onShareFail: function() {
                    this.lastShareSucc = !1, wx.showToast({
                        title: "分享失败"
                    });
                },
                shareToWx: function(d, e) {
                    var _ = "daily_shares:" + o.getToday(), u = r.get_int(_);
                    this.dailyFirstShare = 0 == u, r.set_data(_, u + 1), this.shareType = d, this.startShareTime = o.getNowWithMs(), 
                    e && (this.skillId = e);
                    var t = o.getRandomInt(1, l.SHARE_NUMS);
                    null != a.share_weights && (t = Math.floor(o.weightingChoice(a.share_weights)));
                    var i = l.SHARE_TITLES[t - 1];
                    null != a.share_titles && (i = a.share_titles[t - 1]);
                    var c = a.cdnUrl + "share_cards/campaign_" + t + ".png";
                    wx.shareAppMessage({
                        title: i,
                        imageUrl: c,
                        query: "cardid=" + t + "&shareType=" + d
                    });
                },
                checkShareSuccess: function() {
                    if (-1 != this.startShareTime) {
                        if (this.dailyFirstShare) return this.onShareFail(), void this.clearShareData();
                        o.getNowWithMs() - this.startShareTime >= a.share_time_delta && (!this.lastShareSucc || o.getRandomInt(0, 100) <= a.share_succ_prob) ? this.onShareSucc(this.shareType) : this.onShareFail(), 
                        this.clearShareData();
                    } else this.clearShareData();
                }
            }