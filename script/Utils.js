
            var l= require("GlobalData"), d= require("Const"),  u = (require("Storage"), require("wxSdk"));
            module.exports = {
                timeDelta: 0,
                isFullScreen: function() {
                    var a = cc.view.getVisibleSize();
                    return console.log("size: " + a.width + "," + a.height), a.width / a.height > 16 / 9 + .05;
                },
                isUndefined: function(a) {
                    return void 0 === a;
                },
                getRandomInt: function(a, e) {
                    return Math.floor(Math.random() * (e - a + 1)) + a;
                },
                getRandomPosition: function() {
                    return [ (Math.random() - .5) * (d.MAP_WIDTH - 300), (Math.random() - .5) * (d.MAP_HEIGHT - 500) ];
                },
                shuffle: function(a) {
                    for (var e = a.length - 1; 0 < e; e--) {
                        var d = Math.floor(Math.random() * (e + 1)), i = [ a[d], a[e] ];
                        a[e] = i[0], a[d] = i[1];
                    }
                    return a;
                },
                zeroPad: function(a, e) {
                    var d = e - a.toString().length + 1;
                    return Array(+(0 < d && d)).join("0") + a;
                },
                getArmyId: function(a, e) {
                    return Math.floor("10" + a + this.zeroPad(e, 3));
                },
                elementIn: function(a, e) {
                    for (var d in e) if (e[d] == a) return !0;
                    return !1;
                },
                setTimeDelta: function(a) {
                    this.timeDelta = a;
                },
                getNow: function() {
                    return Math.floor(new Date() / 1e3) + this.timeDelta;
                },
                getNowWithMs: function() {
                    return new Date() / 1e3 + this.timeDelta;
                },
                getNowInMs: function() {
                    return Math.floor(new Date());
                },
                getToday: function() {
                    var a = new Date(), e = "" + a.getFullYear() + this.zeroPad(a.getMonth() + 1, 2) + this.zeroPad(a.getDate(), 2);
                    return parseInt(e);
                },
                getDist2: function(a, e) {
                    var d = a.x - e.x, i = a.y - e.y;
                    return d * d + i * i;
                },
                point_interact_ray: function(a, e, d, i) {
                    return a * i - e * d;
                },
                formatSeconds: function(a) {
                    return new Date(1e3 * a).toISOString().substr(11, 8);
                },
                formatSeconds2: function(a) {
                    var d = +a, i = (Math.floor(d / 86400), Math.floor(d % 86400 / 3600), Math.floor(d % 3600 / 60)), _ = Math.floor(d % 3600 % 60);
                    return (10 > i ? "0" + i : i) + ":" + (10 > _ ? "0" + _ : _);
                },
                formatSeconds3: function(a) {
                    var d = +a, _ = Math.floor(d / 86400), n = Math.floor(d % 86400 / 3600), t = Math.floor(d % 3600 / 60), i = Math.floor(d % 3600 % 60);
                    return (0 < _ ? _ + "天 " : "") + (0 < n ? n + "小时" : "") + (0 < t ? t + "分" : "") + (0 < i ? i + "秒" : "");
                },
                formatTimestamp: function(a) {
                    return new Date(1e3 * a).toLocaleTimeString("en-GB");
                },
                formatDate: function(a) {
                    return new Date(1e3 * a).toLocaleDateString("zh-Hans-CN");
                },
                clamp: function(a, e, d) {
                    return e < a ? e : a < d ? d : a;
                },
                strlen: function(a) {
                    for (var e, d = 0, i = 0; i < a.length; i++) e = a.charCodeAt(i), 1 <= e && 126 >= e || 65376 <= e && 65439 >= e ? d++ : d += 2;
                    return d;
                },
                subString: function(d, e, _) {
                    for (var o = 0, t = "", i = /[^\x00-\xff]/g, s = "", n = d.replace(i, "**").length, a = 0; a < n && (null == (s = d.charAt(a).toString()).match(i) ? o++ : o += 2, 
                    !(e < o)); a++) t += s;
                    return _ && e < n && (t += "..."), t;
                },
                compareObjects: function(a, e) {
                    for (var d in a) if (a.hasOwnProperty(d) && a[d] !== e[d]) return !1;
                    for (var d in e) if (e.hasOwnProperty(d) && a[d] !== e[d]) return !1;
                    return !0;
                },
                isHanzi: function(a) {
                    return /.*[\u4e00-\u9fa5]+.*$/.test(a);
                },
                sdkVersionGreater: function(a, e) {
                    var d = a.split("."), i = e.split(".");
                    return 3 == d.length && 3 == i.length && (parseInt(d[0]) > parseInt(i[0]) || d[0] == i[0] && parseInt(d[1]) > parseInt(i[1]) || d[0] == i[0] && d[1] == i[1] && parseInt(d[2]) >= parseInt(i[2]));
                },
                weightingChoice: function(d) {
                    var e = 0, _ = [], o = [];
                    for (var t in d) d.hasOwnProperty(t) && (e += d[t], o.push(t), _.push(d[t]));
                    if (0 == e) return -1;
                    for (var i = 0; i < _.length; i++) _[i] /= e;
                    var s = 0;
                    for (i = 0; i < _.length; i++) s += _[i], _[i] = s;
                    var n = Math.random();
                    for (i = 0; i < _.length; i++) if (n < _[i]) return o[i];
                    return o[Math.floor(Math.random() * o.length)];
                },
                log_event: function(a, e) {
                    // try {
                    //     t.Event.stat(a, e);
                    // } catch (a) {
                    //     cc.log(a);
                    // }
                    // try {
                    //     //SendEvent(a, e);
                    // } catch (a) {
                    //     cc.log("ald exception"), cc.log(a);
                    // }
                },
                is_ad_enabled: function() {
                    if(typeof(wx) != undefined){
                        l.system_info = wx.getSystemInfoSync()
                    }else{
                        l.system_info = {}
                    }
                    var a = l.system_info.SDKVersion || "";
                    return this.sdkVersionGreater(a, "2.0.4");
                },
                print_call_stack: function() {
                    var a = new Error().stack;
                    console.log("PRINTING CALL STACK"), console.log(a);
                },
                push_record: function() {
                    u.setLoginUrl(l.baseUrl + "login");
                    var d = wx.getStorageSync("last_play") || [ 0, 0, 0 ], e = wx.getStorageSync("best_play") || [ 0, 0, 0 ], _ = wx.getStorageSync("total_play") || [ 0, 0, 0 ];
                    if (4 == l.last_play.length) {
                        var r = l.last_play[0], t = l.last_play[1], i = l.last_play[2], s = l.last_play[3];
                        d = [ r, t, i ], wx.setStorageSync("last_play", d);
                        var n = !1;
                        r > e[0] && (e[0] = r, n = !0), t > e[1] && (e[1] = t, n = !0), i > e[2] && (e[2] = i, 
                        n = !0), n && wx.setStorageSync("best_play", e), _ = [ _[0] + r, _[1] + t, _[2] + i ], wx.setStorageSync("total_play", _), 
                        l.on_update_level(s), n && wx.postMessage({
                            messageType: 2,
                            kill: "" + e[0],
                            score: "" + e[1],
                            level: "" + l.maxlevel
                        }), l.best_info = e, l.push_data_to_server(), l.last_play = [], l.last_info = d, l.total_info = _;
                        var a = wx.getStorageSync("today_best") || [ 0, 0, 0 ], c = this.getToday();
                        c == a[0] ? (t > a[1] || r > a[2]) && this.push_today_to_server([ c, t, r ]) : this.push_today_to_server([ c, t, r ]);
                    }
                },
                push_today_to_server: function(a) {
                    u.request({
                        url: l.baseUrl + "updateRole?port=9009&gameId="+l.gameId, 
                        method: "GET",
                        data: {
                            today: a[0],
                            score: a[1],
                            kill: a[2]
                        },
                        success: function() {},
                        fail: function() {}
                    }), wx.setStorageSync("today_best", a);
                }
            }