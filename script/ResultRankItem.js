
            var d= require("Utils");
            cc.Class({
                extends: cc.Component,
                properties: {
                    avatarSprite: cc.Sprite,
                    rankLabel: cc.Label,
                    nicknameLabel: cc.Label,
                    scoreLabel: cc.Label,
                    _rank: 0,
                    _info: null
                },
                init: function(a, e) {
                    var i = this;
                    if (this._rank = a + 1, this._info = e, "" != e[2]) try {
                        var _ = wx.createImage();
                        _.onload = function() {
                            try {
                                var a = new cc.Texture2D();
                                a.initWithElement(_), a.handleLoadedTexture(), i.avatarSprite.spriteFrame = new cc.SpriteFrame(a);
                            } catch (a) {
                                cc.log(a), i.avatarSprite.node.active = !1;
                            }
                        }, _.src = e[2];
                    } catch (a) {
                        cc.log(a), this.avatarSprite.node.active = !1;
                    }
                    this.rankLabel.string = this._rank, this.nicknameLabel.string = "" != e[1] && null != e[1] ? d.subString(e[1], 10, !0) : "荣耀勇士", 
                    this.scoreLabel.string = e[3];
                },
                start: function() {}
            })