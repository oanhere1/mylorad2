
            var d= require("GlobalData"), t= require("Utils"), i = (require("Const"), require("VideoMgr"));
            require("Storage"), cc.Class({
                extends: cc.Component,
                properties: {
                    goldLabel: cc.Label,
                    goldBonus: cc.Label,
                    btnGoldClaim: cc.Node,
                    btnGoldClaimDouble: cc.Node,
                    _goCtrl: null,
                    _kill: 0,
                    _goldClaimed: !1,
                    _adid: 5
                },
                init: function(a, e) {
                    this._goCtrl = a, this._kill = e, this._goldClaimed = !1, this.init_gold_ad(), this.goldLabel.string = d.gold, 
                    this.goldBonus.string = "/" + this._kill, this.btnGoldClaimDouble.getChildByName("coin_gold").getChildByName("value").getComponent(cc.Label).string = "x" + 2 * this._kill, 
                    this.btnGoldClaim.active = !0;
                },
                _play_gold_anim: function() {
                    var a = this.goldBonus.node.parent.getComponent(cc.Animation);
                    a.stop("new_res_fly"), a.play("new_res_fly");
                },
                adOnClose: function(a) {
                    i.videoAd.bind_this.after_ad(a);
                },
                after_ad: function(a) {
                    var e = d.system_info.SDKVersion || "";
                    !t.sdkVersionGreater(e, "2.1.0") || null != a && a.isEnded ? (t.log_event("video_ad_" + this._adid + "_5", {}), 
                    i.releaseVideo(), this._give_gold_bonus(2 * this._kill), t.log_event("click_gold_ad_finish", {})) : this.scheduleOnce(function() {
                        try {
                            wx.showToast({
                                title: "看完获得奖励"
                            });
                        } catch (a) {}
                    }.bind(this), .2);
                },
                _create_video: function() {
                    var a = {
                        1: 5,
                        2: 8
                    }, e = a[t.getRandomInt(1, 2)];
                    null == e && (e = a[1]), this._adid = e, i.createVideo(this, e);
                },
                init_gold_ad: function() {
                    t.is_ad_enabled() && this._create_video(), this.enable_claim_double(!1);
                },
                on_ad_loaded: function() {
                    t.log_event("video_ad_" + i.cur_adid + "_2", {}), this.enable_claim_double(!0);
                },
                enable_claim_double: function(a) {
                    (this.btnGoldClaimDouble.getComponent(cc.Button).interactable = a) && (this.btnGoldClaimDouble.stopAllActions(), this.btnGoldClaimDouble.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(.3, 1.06), cc.scaleTo(.3, 1)))));
                },
                on_click_gold_claim: function() {
                    this._goldClaimed || this._give_gold_bonus(this._kill);
                },
                _give_gold_bonus: function(a) {
                    this._goldClaimed = !0, this._play_gold_anim(), d.on_update_gold(d.gold + a), wx.showToast({
                        title: "领取成功"
                    }), this.scheduleOnce(function() {
                        this._goCtrl._show_end();
                    }.bind(this), 1.5);
                },
                on_click_gold_claim_double: function() {
                    this._goldClaimed || t.is_ad_enabled() && (t.log_event("video_ad_" + i.cur_adid + "_3", {}), t.log_event("click_gold_ad", {}), 
                    this._goCtrl.destroy_ads(), i.showVideo());
                },
                on_click_gold_skip: function() {
                    this._goCtrl._show_end();
                },
                start: function() {}
            })