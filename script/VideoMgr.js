
            var d= require("Utils"), t= require("GlobalData");
            module.exports = {
                videoAd: null,
                videoAdLoaded: !1,
                cur_adid: 0,
                createVideo: function(a, e) {
                    null != this.videoAd && this.releaseVideo();
                    var i = t.ad_video_ids[e];
                    null != i && (this.cur_adid = e, d.log_event("video_ad_" + this.cur_adid + "_1", {}), this.videoAd = wx.createRewardedVideoAd({
                        adUnitId: i
                    }), this.videoAd.bind_this = a,this.videoAd.onError(function(){}), this.videoAd.onClose(this.videoAd.bind_this.adOnClose), 
                    this.videoAdLoaded = !1, this.videoAd.load().then(function() {
                        this.videoAdLoaded = !0, a.on_ad_loaded();
                    }.bind(this)).catch(function() {}));
                },
                releaseVideo: function() {
                    this.videoAd && (this.videoAd.bind_this && this.videoAd.offClose(this.videoAd.bind_this.adOnClose), 
                    this.videoAd.bind_this = null), this.videoAd = null, this.cur_adid = 0, this.videoAdLoaded = !1;
                },
                showVideo: function(a) {
                    null != a && (this.videoAd && this.videoAd.bind_this && a != this.videoAd.bind_this && this.videoAd.offClose(this.videoAd.bind_this.adOnClose), 
                    this.videoAd.bind_this != a && (this.videoAd.bind_this = a, this.videoAd.onClose(this.videoAd.bind_this.adOnClose))), 
                    this.videoAdLoaded ? this.videoAd.show().then(function() {
                        d.log_event("video_ad_" + this.cur_adid + "_4", {});
                    }.bind(this)).catch(function() {}) : this.videoAd.load().then(function() {
                        this.videoAdLoaded = !0, d.log_event("video_ad_" + this.cur_adid + "_2", {}), this.videoAd.show().then(function() {
                            d.log_event("video_ad_" + this.cur_adid + "_4", {});
                        }.bind(this)).catch(function() {});
                    }.bind(this)).catch(function() {});
                }
            }