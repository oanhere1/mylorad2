var l = require("GlobalData"),
    n = require("Utils"),
    a = require("wxSdk"),
    i = require("Const"),
    r = require("Storage");
var u = require("ShareUtils");
cc.Class({
    extends: cc.Component,
    properties: {
        usernameInput: cc.Node,
        btnLogin: cc.Node,
        loadingNode: cc.Node,
        loadingProgress: cc.ProgressBar,
        loadingLabel: cc.Label,
        loginBtn: cc.Node,
        _auth_btn: null,
        _maxPercent: null,
        _totalPercent: null,
        _percentQueue: []
    },
    onLoad: function () {
        this._maxPercent = [], this._totalPercent = [], this.add_load("textures/avatar_1", "avatarAtlas_1"), this.add_load("textures/weapon", "weaponAtlas"),
            cc.loader.onProgress = function () {}.bind(this), cc.director.preloadScene("home", function () {
                cc.log("Home scene preloaded");
            }), this.init_mta();
        if (typeof (wx) !== "undefined") {
            l.system_info = wx.getSystemInfoSync(), wx.onShow(function (a) {
                this.on_show(a);
            }.bind(this));
            var a = wx.getLaunchOptionsSync();
            this.on_show(a), l.share_weights = i.SHARE_WEIGHTS, l.share_titles = i.SHARE_TITLES;
        }
    },
    on_show: function (a) {
        if (cc.log("on_show"), cc.log(a), u.checkShareSuccess(), null != a) {
            var e = a.query.cardid;
            null != e && "" != e && (l.cardid = e, n.log_event("click_card", {
                value: "" + e
            }), n.log_event("click_card_" + e, {}));
            var d = r.get_int("newuser");
            l.newuser = d;
            var _ = a.query.inviterid;
            null != _ && "" != _ && (l.inviterid = _, n.log_event("from_inviter", {}), 0 == d && n.log_event("from_inviter_newuser", {}));
            try {
                var t = a.referrerInfo;
                if (null != t) {
                    var i = t.appId;
                    null != i && (n.log_event("from_appid", {
                        appid: "" + i
                    }), 0 == d && n.log_event("from_appid_newuser", {
                        appid: "" + i
                    }));
                }
            } catch (a) {
                cc.log(a);
            }
            r.set_data("newuser", 1);
        }
    },
    init_mta: function () {
        // _.App.init({
        //     appID: "500629102",
        //     eventID: "500629103",
        //     statShareApp: !0
        // }), n.log_event("enter_game", {});
    },
    update_percent: function (d, e, _) {
        if (null != this.usernameInput && !this.usernameInput.active) {
            // console.log("aaaaa");
            this._totalPercent[d] = e / _;
            var o = 0,
                t = 0;
            for (var i in this._maxPercent) this._maxPercent.hasOwnProperty(i) && (o += this._maxPercent[i], t += this._totalPercent[i] * this._maxPercent[i]);
            var s = Math.floor(100 * t / o),
                n = this._percentQueue.length;
            (0 == n || s > this._percentQueue[n - 1]) && this._percentQueue.push(s);
        }
    },
    add_load: function (a, d) {
        this._maxPercent[d] = 1, this._totalPercent[d] = 0, cc.loader.loadRes(a, cc.SpriteAtlas, function (a, e) {
            // console.log("add load " + a)
            this.update_percent(d, a, e);
        }.bind(this), function (a, e) {
            l[d] = e;
        }.bind(this));
    },
    on_loaded: function () {
        var a = this;
        if (typeof (wx) !== "undefined") {
            wx.showShareMenu({
                withShareTicket: !0
            }), wx.onShareAppMessage(function () {
                var a = n.getRandomInt(1, i.SHARE_NUMS);
                null != l.share_weights && (a = Math.floor(n.weightingChoice(l.share_weights)));
                var e = i.SHARE_TITLES[a - 1];
                return null != l.share_titles && (e = l.share_titles[a - 1]), {
                    title: e,
                    imageUrl: l.cdnUrl + "share_cards/campaign_" + a + ".png",
                    success: function () {
                        n.log_event("share_1", {
                            stype: "1"
                        });
                    },
                    fail: function () {
                        n.log_event("share_0", {
                            stype: "1"
                        });
                    }
                };
            }.bind(this)), wx.getSetting({
                success: function (d) {
                    if (d.authSetting.scope == undefined) {
                        d.authSetting.scope = {}
                    }
                    cc.log(d.authSetting), null == d.authSetting.scope.userInfo ? 5 > r.get_int("play_times") ? a.login_without_auth() : a.login_on_auth() : a.onClickLogin();
                }
            });
        }else{
            this.after_login()
        }
    },
    after_login: function () {
        null != this._auth_btn && (this._auth_btn.hide(), this.loginBtn.active = !1), l.refresh_user_info(),
            cc.director.loadScene("home");
    },
    login_on_auth: function () {
        var a = this;
        try {
            var d = wx.getSystemInfoSync(),
                e = d.screenHeight;
            this._auth_btn = wx.createUserInfoButton({
                    type: "text",
                    text: "",
                    style: {
                        left: 0,
                        top: 0,
                        width: d.screenWidth,
                        height: e,
                        lineHeight: e,
                        backgroundColor: "#000000",
                        color: "#ffffff",
                        backgroundColorOpacity: .5,
                        opacity: .5,
                        textAlign: "center",
                        fontSize: 16,
                        borderRadius: 0
                    }
                }), this.loginBtn.active = !0, this.loadingLabel.node.active = !1, cc.log(this._auth_btn.style),
                this._auth_btn.onTap(function (d) {
                    console.log(d), a.onClickLogin();
                });
        } catch (e) {
            this.onClickLogin();
        }
    },
    login_without_auth: function () {
        wx.login({
            success: function (d) {
                var i = this;
                d.code && (cc.log("on_success: " + d.code), n.log_event("start_game_no_auth", {
                    cardid: "" + l.cardid
                }), a.loginWithCode_2({
                    loginUrl: l.baseUrl + "login?port=9009&gameId="+l.gameId,
                    code: d.code,
                    success: function (a, e) {
                        cc.log("success: "), cc.log(a), cc.log(e), l.userInfo = a, l.serverConfigs = e || {},
                            l.refresh_configs(), i.after_login();
                    },
                    fail: function (a) {
                        cc.log("fail: " + a), n.log_event("connect_server_0", {
                            ctype: "2"
                        }), i.after_login();
                    }
                }));
            }.bind(this),
            fail: function (a) {
                cc.log("on_fail: " + a.code + "," + a.errMsg);
            }
        });
    },
    onClickLogin: function () {
        wx.login({
            success: function (d) {
                d.code ? (cc.log("on_success: " + d.code), wx.getUserInfo({
                    lang: "zh_CN",
                    success: function (i) {
                        var t = this;
                        cc.log("getUserInfo success"), cc.log(i), l.userInfo = i.userInfo, n.log_event("start_game", {
                            cardid: "" + l.cardid
                        }), a.login_2({
                            loginUrl: l.baseUrl + "login?port=9009&gameId="+l.gameId,
                            res: i,
                            code: d.code,
                            success: function (a, e) {
                                cc.log("success: "), cc.log(a), cc.log(e), l.userInfo = a, l.serverConfigs = e || {},
                                    l.refresh_configs(), n.log_event("connect_server_1", {
                                        ctype: "1"
                                    }), t.after_login();
                            },
                            fail: function (a) {
                                cc.log("fail: " + a), n.log_event("connect_server_0", {
                                    ctype: "1"
                                }), t.after_login();
                            }
                        });
                    }.bind(this),
                    fail: function (i) {
                        var t = this;
                        cc.log("getUserInfo fail"), cc.log(i), n.log_event("start_game_no_auth", {
                            cardid: "" + l.cardid
                        }), a.loginWithCode_2({
                            loginUrl: l.baseUrl + "login?port=9009&gameId="+l.gameId,
                            code: d.code,
                            success: function (a, e) {
                                cc.log("success: "), cc.log(a), cc.log(e), l.userInfo = a, l.serverConfigs = e || {},
                                    l.refresh_configs(), n.log_event("connect_server_1", {
                                        ctype: "2"
                                    }), t.after_login();
                            },
                            fail: function (a) {
                                cc.log("fail: " + a), n.log_event("connect_server_0", {
                                    ctype: "2"
                                }), t.after_login();
                            }
                        });
                    }.bind(this)
                })) : cc.log("登录失败: " + res.errMsg);
            }.bind(this),
            fail: function (a) {
                cc.log("on_fail: " + a.code + "," + a.errMsg);
            }
        });
    },
    start: function () {},
    update: function () {
        if (0 < this._percentQueue.length) {
            // console.log("this._percentQueue.length " + this._percentQueue.length)
            var e = this._percentQueue.shift();
            this.loadingProgress.progress = e / 100, this.loadingLabel.string = e + "%", 100 <= e && (this._percentQueue = [],
                cc.loader.onProgress = null, this.on_loaded());
        }
    }
})