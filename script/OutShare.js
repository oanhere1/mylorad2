
            var d= require("GlobalData"), i = (require("Const"), require("Utils"));
            require("Storage"), cc.Class({
                extends: cc.Component,
                properties: {
                    imgSprite: cc.Sprite,
                    share_pos: 1,
                    _out_id: 2
                },
                onLoad: function() {
                    var a = this;
                    if (!d.out_share) this.hide(); else if (1 == d.out_share_conf[this.share_pos]) {
                        var n = d.system_info.SDKVersion || "";
                        if (!i.sdkVersionGreater(n, "2.2.0")) this.hide(); else if (!(null != d.out_share_appids && null != d.out_share_weight)) this.hide(); else if (this._out_id = Math.floor(i.weightingChoice(d.out_share_weight)), 
                        0 > this._out_id || null == d.out_share_appids[this._out_id]) this.hide(); else {
                            this.node.opacity = 0;
                            var e = d.cdnUrl + "out_share/out_share_" + this._out_id + ".png";
                            cc.loader.load({
                                url: e,
                                type: "png"
                            }, function(d, e) {
                                null != a.imgSprite && (a.imgSprite.spriteFrame = new cc.SpriteFrame(e)), a.node.opacity = 255;
                            });
                            var o = d.out_share_appids[this._out_id];
                            1 == d.out_share_events[this._out_id] && i.log_event("out_share_show_" + this.share_pos, {
                                appid: o
                            });
                        }
                    } else this.hide();
                },
                hide: function() {
                    this.node.active = !1;
                },
                on_click: function() {
                    var a = d.out_share_appids[this._out_id], n = d.out_share_paths[this._out_id] || "", o = 1 == d.out_share_events[this._out_id];
                    if (1 != d.out_share_types[this._out_id]) wx.navigateToMiniProgram({
                        appId: a,
                        path: n,
                        extraData: "",
                        success: function() {
                            o && (i.log_event("out_share_click_total_" + this._out_id, {}), i.log_event("out_share_click_" + this.share_pos + "_1", {
                                appid: a
                            }));
                        }.bind(this),
                        fail: function() {
                            o && i.log_event("out_share_click_" + this.share_pos + "_0", {
                                appid: a
                            });
                        }.bind(this)
                    }); else {
                        var s = d.cdnUrl + "out_share/out_share_" + this._out_id + "_qr.png";
                        wx.previewImage({
                            urls: [ s ],
                            success: function() {
                                o && (i.log_event("out_share_qr_total_" + this._out_id, {}), i.log_event("out_share_qr_" + this.share_pos + "_1", {
                                    appid: a
                                }));
                            }.bind(this),
                            fail: function() {
                                o && i.log_event("out_share_qr_" + this.share_pos + "_0", {
                                    appid: a
                                });
                            }.bind(this)
                        });
                    }
                },
                start: function() {}
            })