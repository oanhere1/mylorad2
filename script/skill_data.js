 module.exports = {
                data: {
                    1: {
                        skill_1: 1,
                        skill_3: 40,
                        skill_2: 40,
                        id: 1
                    },
                    2: {
                        skill_1: 40,
                        skill_3: 40,
                        skill_2: 1,
                        id: 2
                    },
                    3: {
                        skill_1: 40,
                        skill_3: 1,
                        skill_2: 40,
                        id: 3
                    },
                    4: {
                        skill_1: 40,
                        skill_3: 40,
                        skill_2: 40,
                        id: 4
                    },
                    5: {
                        skill_1: 40,
                        skill_3: 40,
                        skill_2: 40,
                        id: 5
                    },
                    6: {
                        skill_1: 1,
                        skill_3: 40,
                        skill_2: 40,
                        id: 6
                    },
                    7: {
                        skill_1: 50,
                        skill_3: 40,
                        skill_2: 1,
                        id: 7
                    },
                    8: {
                        skill_1: 50,
                        skill_3: 1,
                        skill_2: 50,
                        id: 8
                    },
                    9: {
                        skill_1: 50,
                        skill_3: 50,
                        skill_2: 50,
                        id: 9
                    },
                    10: {
                        skill_1: 50,
                        skill_3: 50,
                        skill_2: 50,
                        id: 10
                    },
                    11: {
                        skill_1: 1,
                        skill_3: 50,
                        skill_2: 50,
                        id: 11
                    },
                    12: {
                        skill_1: 60,
                        skill_3: 50,
                        skill_2: 1,
                        id: 12
                    },
                    13: {
                        skill_1: 60,
                        skill_3: 1,
                        skill_2: 60,
                        id: 13
                    },
                    14: {
                        skill_1: 60,
                        skill_3: 60,
                        skill_2: 60,
                        id: 14
                    },
                    15: {
                        skill_1: 60,
                        skill_3: 60,
                        skill_2: 60,
                        id: 15
                    },
                    16: {
                        skill_1: 1,
                        skill_3: 60,
                        skill_2: 60,
                        id: 16
                    },
                    17: {
                        skill_1: 70,
                        skill_3: 60,
                        skill_2: 1,
                        id: 17
                    },
                    18: {
                        skill_1: 70,
                        skill_3: 1,
                        skill_2: 70,
                        id: 18
                    },
                    19: {
                        skill_1: 70,
                        skill_3: 70,
                        skill_2: 70,
                        id: 19
                    },
                    20: {
                        skill_1: 70,
                        skill_3: 70,
                        skill_2: 70,
                        id: 20
                    },
                    21: {
                        skill_1: 1,
                        skill_3: 70,
                        skill_2: 70,
                        id: 21
                    },
                    22: {
                        skill_1: 80,
                        skill_3: 70,
                        skill_2: 1,
                        id: 22
                    },
                    23: {
                        skill_1: 80,
                        skill_3: 1,
                        skill_2: 80,
                        id: 23
                    },
                    24: {
                        skill_1: 80,
                        skill_3: 80,
                        skill_2: 80,
                        id: 24
                    },
                    25: {
                        skill_1: 80,
                        skill_3: 80,
                        skill_2: 80,
                        id: 25
                    },
                    26: {
                        skill_1: 1,
                        skill_3: 80,
                        skill_2: 80,
                        id: 26
                    },
                    27: {
                        skill_1: 90,
                        skill_3: 80,
                        skill_2: 1,
                        id: 27
                    },
                    28: {
                        skill_1: 90,
                        skill_3: 1,
                        skill_2: 90,
                        id: 28
                    },
                    29: {
                        skill_1: 90,
                        skill_3: 90,
                        skill_2: 90,
                        id: 29
                    },
                    30: {
                        skill_1: 90,
                        skill_3: 90,
                        skill_2: 90,
                        id: 30
                    },
                    31: {
                        skill_1: 1,
                        skill_3: 90,
                        skill_2: 90,
                        id: 31
                    },
                    32: {
                        skill_1: 100,
                        skill_3: 90,
                        skill_2: 1,
                        id: 32
                    },
                    33: {
                        skill_1: 100,
                        skill_3: 1,
                        skill_2: 100,
                        id: 33
                    },
                    34: {
                        skill_1: 100,
                        skill_3: 100,
                        skill_2: 100,
                        id: 34
                    },
                    35: {
                        skill_1: 100,
                        skill_3: 100,
                        skill_2: 100,
                        id: 35
                    },
                    36: {
                        skill_1: 1,
                        skill_3: 100,
                        skill_2: 100,
                        id: 36
                    },
                    37: {
                        skill_1: 110,
                        skill_3: 100,
                        skill_2: 1,
                        id: 37
                    },
                    38: {
                        skill_1: 110,
                        skill_3: 1,
                        skill_2: 110,
                        id: 38
                    },
                    39: {
                        skill_1: 110,
                        skill_3: 110,
                        skill_2: 110,
                        id: 39
                    },
                    40: {
                        skill_1: 110,
                        skill_3: 110,
                        skill_2: 110,
                        id: 40
                    },
                    41: {
                        skill_1: 1,
                        skill_3: 110,
                        skill_2: 110,
                        id: 41
                    },
                    42: {
                        skill_1: 120,
                        skill_3: 110,
                        skill_2: 1,
                        id: 42
                    },
                    43: {
                        skill_1: 120,
                        skill_3: 1,
                        skill_2: 120,
                        id: 43
                    },
                    44: {
                        skill_1: 1,
                        skill_3: 120,
                        skill_2: 120,
                        id: 44
                    },
                    45: {
                        skill_1: 120,
                        skill_3: 120,
                        skill_2: 1,
                        id: 45
                    },
                    46: {
                        skill_1: 120,
                        skill_3: 1,
                        skill_2: 120,
                        id: 46
                    },
                    47: {
                        skill_1: 1,
                        skill_3: 120,
                        skill_2: 120,
                        id: 47
                    },
                    48: {
                        skill_1: 140,
                        skill_3: 120,
                        skill_2: 1,
                        id: 48
                    },
                    49: {
                        skill_1: 140,
                        skill_3: 1,
                        skill_2: 140,
                        id: 49
                    },
                    50: {
                        skill_1: 1,
                        skill_3: 140,
                        skill_2: 140,
                        id: 50
                    },
                    51: {
                        skill_1: 160,
                        skill_3: 140,
                        skill_2: 1,
                        id: 51
                    },
                    52: {
                        skill_1: 160,
                        skill_3: 1,
                        skill_2: 160,
                        id: 52
                    },
                    53: {
                        skill_1: 1,
                        skill_3: 160,
                        skill_2: 160,
                        id: 53
                    },
                    54: {
                        skill_1: 180,
                        skill_3: 160,
                        skill_2: 1,
                        id: 54
                    },
                    55: {
                        skill_1: 180,
                        skill_3: 1,
                        skill_2: 180,
                        id: 55
                    },
                    56: {
                        skill_1: 1,
                        skill_3: 180,
                        skill_2: 180,
                        id: 56
                    },
                    57: {
                        skill_1: 200,
                        skill_3: 180,
                        skill_2: 1,
                        id: 57
                    },
                    58: {
                        skill_1: 200,
                        skill_3: 1,
                        skill_2: 200,
                        id: 58
                    },
                    59: {
                        skill_1: 1,
                        skill_3: 200,
                        skill_2: 200,
                        id: 59
                    },
                    60: {
                        skill_1: 220,
                        skill_3: 200,
                        skill_2: 1,
                        id: 60
                    }
                }
            }