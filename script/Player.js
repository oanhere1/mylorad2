
            var d= require("Const"), u= require("avatar_data"), r= require("GlobalData"), l= require("Utils"), i= require("food_data");
            cc.Class({
                extends: cc.Component,
                properties: {
                    angle: 0,
                    tgtAngle: 0,
                    speed: 0,
                    angular: 180,
                    initRadius: 50,
                    radius: 50,
                    mapWidth: 3840,
                    mapHeight: 2980,
                    mapBorder: 33,
                    attackRadius: 35,
                    weaponPrefab: cc.Prefab,
                    deadEffectPrefab: cc.Prefab,
                    nicknamePrefab: cc.Prefab,
                    levelPrefab: cc.Prefab,
                    sexPrefab: cc.Prefab,
                    _aid: 0,
                    _speedLvScale: 1,
                    _speedBoostScale: 1,
                    _isMoving: !1,
                    _tickCount: 0,
                    _lastClockWise: 0,
                    _avatar: null,
                    _lastSyncX: 0,
                    _lastSyncY: 0,
                    _lastSyncYaw: 0,
                    _tgtPosX: null,
                    _tgtPosY: null,
                    _playerNode: null,
                    _inAttackAction: !1,
                    _lastAttackTime: 0,
                    _modelId: 1,
                    _weaponId: 1,
                    _footprintsNum: 0,
                    _fixedTimeStep: .07,
                    _accumulatedTime: 0,
                    _bodyScale: 1,
                    _deadEffectNode: null,
                    _weaponNode: null,
                    _nicknameNode: null,
                    _levelNode: null,
                    _sexNode: null,
                    _avatarInfo: null,
                    _guardRadius: 0,
                    _lastGuardTime: 0,
                    _lastChaseTime: 0,
                    _aiState: 0,
                    _lastRandomWalkTime: 0,
                    _chaseTgt: null,
                    _thinkEatTimes: 0,
                    _tgtFood: null,
                    _chasingPlayer: !1,
                    _playerAttackRadiusFix: 1.05,
                    _levelDelta: .002
                },
                onLoad: function() {
                    if (this.onRefreshByLv(this._avatar), null == this._deadEffectNode && null != r.gameCtrl) {
                        var a = r.gameCtrl.node.getChildByName("player_parts").getChildByName("deadeffects"), e = a.getChildByName("dead_" + this._avatar.id);
                        null == e && ((e = cc.instantiate(this.deadEffectPrefab)).setName("dead_" + this._avatar.id), 
                        a.addChild(e)), this._deadEffectNode = e, this._deadEffectNode.active = !1;
                    }
                },
                start: function() {},
                init: function() {
                    this._aiState = 0, this._lastRandomWalkTime = 0, this._chaseTgt = null, this._thinkEatTimes = 0, this._tgtFood = null, 
                    this._chasingPlayer = !1;
                },
                initModel: function() {
                    if (null != r.gameCtrl) {
                        this.init(), this._aid = this._avatar.id;
                        var d = this._avatar.level;
                        this._avatarInfo = u.data[this._avatar.level], this._modelId = d, this._weaponId = d, 
                        this.removeClips(this.node.getChildByName("playerNode").getChildByName("sprite"), [ "Walk" ]), this.createClips(this.node.getChildByName("playerNode").getChildByName("sprite"), [ "Walk" ]), 
                        this.play("Walk");
                        var e = r.weaponAtlas.getSpriteFrame("weapon_" + this._weaponId);
                        null == e && (e = r.weaponAtlas.getSpriteFrame("weapon_1"));
                        var _ = r.gameCtrl.node.getChildByName("weapons"), o = _.getChildByName("weapon_" + this._aid);
                        null == o && ((o = cc.instantiate(this.weaponPrefab)).setName("weapon_" + this._aid), _.addChild(o)), 
                        this._weaponNode = o, this._weaponNode.scale = 1;
                        var t = this._weaponNode.getChildByName("sprite");
                        t.getComponent(cc.Sprite).spriteFrame = e;
                        var i = this._modelId + "_Walk_00", s = this.get_atlas().getSpriteFrame(i).getOriginalSize().height, n = l.clamp(s / 148, 1.2, 1);
                        this._weaponNode.getChildByName("sprite").y = -60 * n, this.attackRadius = t.width, this._guardRadius = this.attackRadius * (this._avatarInfo.guard_radius || 1), 
                        this.onWeaponUpdatePos(), this.reset_weapon(), this._accumulatedTime = 0;
                    }
                },
                play: function(a) {
                    this.node.getChildByName("playerNode").getChildByName("sprite").getComponent(cc.Animation).play(a);
                },
                get_atlas: function() {
                    return r.avatarAtlas_1;
                },
                removeClips: function(a, e) {
                    for (var d = 0; d < e.length; d++) a.getComponent(cc.Animation).removeClip(e[d], !0);
                },
                createClips: function(d, e) {
                    for (var _ = 0; _ < e.length; _++) {
                        for (var o = e[_], t = [], i = 0; 6 > i; i++) {
                            var s = this._modelId + "_" + o + "_" + l.zeroPad(i, 2), n = this.get_atlas().getSpriteFrame(s);
                            null != n && t.push(n);
                        }
                        var a = this.createClip(o, t, !0);
                        d.getComponent(cc.Animation).addClip(a);
                    }
                },
                createClip: function(a, e, d) {
                    var i = cc.AnimationClip.createWithSpriteFrames(e, 60);
                    return i.name = a, i.speed = .2, d && (i.wrapMode = cc.WrapMode.Loop), i;
                },
                setAvatar: function(d) {
                    this._avatar = d;
                    var e = r.gameCtrl.node.getChildByName("nicknames"), _ = e.getChildByName("nickname_" + d.id);
                    null == _ && ((_ = cc.instantiate(this.nicknamePrefab)).setName("nickname_" + d.id), e.addChild(_)), 
                    this._nicknameNode = _, this._nicknameNode.getComponent(cc.Label).string = d.name, this.onNicknameUpdatePos();
                    var o = r.gameCtrl.node.getChildByName("levels"), t = o.getChildByName("levels_" + d.id);
                    null == t && ((t = cc.instantiate(this.levelPrefab)).setName("levels_" + d.id), o.addChild(t)), 
                    this._levelNode = t, this._levelNode.getChildByName("exp").active = this._avatar.is_player, 
                    this._levelNode.getChildByName("exp_bg").active = !this._avatar.is_player;
                    var i = o.getChildByName("sex_" + d.id);
                    null == i && ((i = cc.instantiate(this.sexPrefab)).setName("sex_" + d.id), o.addChild(i)), 
                    this._sexNode = i;
                    var s = !1, n = !1;
                    this._avatar.is_player || (n = !(s = 45 >= l.getRandomInt(1, 100))), this._sexNode.getChildByName("ico_male").active = s, 
                    this._sexNode.getChildByName("ico_female").active = n, this.onLevelUpdatePos(), this.onRefreshByLv(d, d.level);
                },
                getSpeedScale: function() {
                    return this._speedLvScale * this._speedBoostScale;
                },
                setSpeedBoostScale: function(a) {
                    1 < (this._speedBoostScale = a) || (this._footprintsNum = 0);
                },
                setSpeed: function(a) {
                    this.speed = 0 < a ? d.INIT_SPEED : a;
                },
                setAngle: function(a) {
                    r.gameOver || this._inAttackAction || this.setAngleDirect(a);
                },
                setAngleDirect: function(a) {
                    this.tgtAngle = a, this.angle = a, this.setRotation(this.angle);
                },
                setRotation: function(a) {
                    this._playerNode.rotation = a, this.reset_weapon();
                },
                setPos: function(a, e) {
                    this.node.x = a, this.node.y = e, this.onWeaponUpdatePos(), this.onNicknameUpdatePos(), 
                    this.onLevelUpdatePos();
                },
                updatePerTwoTick: function() {
                    if (null == this._playerNode && (this._playerNode = this.node.getChildByName("playerNode")), null != this._avatar && this._avatar.is_alive && !this._inAttackAction) if (0 < this.speed) {
                        if (null != this._tgtPosX && null != this._tgtPosY && this.check_close(this._tgtPosX, this._tgtPosY)) return this.setPos(this._tgtPosX, this._tgtPosY), 
                        void this.stop_move();
                        var a = this.getSpeedScale(), e = this.node.x + Math.cos(this.angle * (Math.PI / 180)) * this.speed * a, d = this.node.y - Math.sin(this.angle * (Math.PI / 180)) * this.speed * a, i = this.can_move(e, d);
                        i[0] && this.setPos(i[1], i[2]), this._isMoving || (this._isMoving = !0, this.play("Walk"));
                    } else this._isMoving && (this._isMoving = !1, this.play("Walk"));
                },
                _randomWalk: function() {
                    var a = l.getNowWithMs();
                    if (!(2 > a - this._lastRandomWalkTime)) {
                        this._lastRandomWalkTime = a;
                        var e = l.getRandomPosition();
                        this.move_to(e[0], e[1]);
                    }
                },
                onClickAttack: function() {
                    if (null != r.gameCtrl) {
                        var a = r.gameCtrl.avatars;
                        for (var e in a) if (a.hasOwnProperty(e)) {
                            var d = a[e];
                            if (d.id == this._avatar.id || !d.is_alive) continue;
                            var i = r.gameCtrl.entities[d.id];
                            l.getDist2(i, {
                                x: this.node.x,
                                y: this.node.y
                            }), this.attackRadius, this.attackRadius, this._avatar.is_alive && (this.stop_move(), r.gameCtrl.on_attack(this._avatar, d, !0));
                        }
                    }
                },
                _checkTgt: function() {
                    if (l.getRandomInt(1, 100) > 5 * this._avatar.level) return !1;
                    var a = Object.keys(r.gameCtrl.avatars).map(function(a) {
                        return r.gameCtrl.avatars[a];
                    });
                    if (0 < a.length) {
                        this._aiState = d.ROBOT_AI_STATE_FIGHT;
                        for (var e = 99999999, _ = 0; _ < a.length; _++) {
                            var n = a[_], t = l.getDist2({
                                x: this.node.x,
                                y: this.node.y
                            }, n);
                            t < e && 0 < t && !n.in_protect && (!n.is_player || this._canAttackPlayer(n)) && (e = t, 
                            this._chaseTgt = n);
                        }
                        return !0;
                    }
                    return !1;
                },
                _checkFood: function() {
                    for (var a, d = r.gameCtrl._foodtree.retrieve({
                        x: this.node.x,
                        y: this.node.y,
                        width: 50,
                        height: 50
                    }), e = 0; e < d.length; e++) if (a = d[e], 25e4 >= l.getDist2(a, {
                        x: this.node.x,
                        y: this.node.y
                    })) return !0;
                    return !1;
                },
                _canAttackPlayer: function(a) {
                    if (null == a || null == a) return !1;
                    if (2 >= a.level) return null != r.gameCtrl && 40 < l.getNow() - r.gameCtrl._gameStartTime ? 700 >= l.getRandomInt(1, 1e3) : 20 < r.play_times && 2 == a.level && 700 >= l.getRandomInt(1, 1e3);
                    var e = {
                        3: 400,
                        4: 500,
                        5: 600,
                        6: 700,
                        7: 800
                    };
                    null != r.serverConfigs && null != r.serverConfigs.attack_prob && (e = r.serverConfigs.attack_prob);
                    var d = e[a.level];
                    return null == d && (d = 1e3), l.getRandomInt(1, 1e3) <= d;
                },
                think: function() {
                    if (null != r.gameCtrl) {
                        if (null != r.avatar && r.avatar.is_alive && !r.avatar.in_protect && this._canAttackPlayer(r.avatar)) {
                            var a = l.getNowWithMs(), e = r.gameCtrl.entities[r.avatar.id], _ = l.getDist2(e, this.node), n = this.attackRadius * this.attackRadius;
                            if (_ < this._guardRadius * this._guardRadius && a - this._lastGuardTime >= this._avatarInfo.guard_cd) {
                                if (this._chasingPlayer) {
                                    if (_ < 1.21 * n) {
                                        var t = this._avatarInfo.guard_stop || .5;
                                        a - this._lastChaseTime > .2 + t * l.getRandomInt(1, 100) / 100 && (this.stop_move(), r.gameCtrl.on_attack(this._avatar, this._chaseTgt, !0));
                                    } else 2 > a - this._lastChaseTime ? this.move_to_around(e.x, e.y) : (this._chasingPlayer = !1, 
                                    this.stop_move(), this.gotoFree(), this._lastGuardTime = a);
                                    return;
                                }
                                if (l.getRandomInt(1, 1e4) <= 1e4 * this._avatarInfo.guard_prob) return void (_ < 1.21 * n ? (t = this._avatarInfo.guard_stop || .5, 
                                a - this._lastChaseTime > .2 + t * l.getRandomInt(1, 100) / 100 && (this.stop_move(), r.gameCtrl.on_attack(this._avatar, this._chaseTgt, !0))) : (this._chaseTgt = r.avatar, 
                                this._chasingPlayer = !0, this._lastChaseTime = a, this.move_to_around(e.x, e.y)));
                                this._lastGuardTime = a, this._chasingPlayer = !1;
                            } else this._chasingPlayer && null != this._chaseTgt && (this._chasingPlayer = !1, this.stop_move(), 
                            this.gotoFree());
                        }
                        this._aiState == d.ROBOT_AI_STATE_FREE ? this.onThinkFree() : this._aiState == d.ROBOT_AI_STATE_EAT ? this.onThinkEat() : this._aiState == d.ROBOT_AI_STATE_FIGHT ? this.onThinkFight() : this.onThinkOther();
                    }
                },
                onThinkFree: function() {
                    this._randomWalk(), this._checkTgt() || 95 >= l.getRandomInt(1, 100) && this._checkFood() && (this._aiState = d.ROBOT_AI_STATE_EAT);
                },
                onThinkEat: function() {
                    if (!this._checkTgt()) {
                        var a = !1;
                        if (null != this._tgtFood && null != r.gameCtrl.food_infos[this._tgtFood.id] && (a = !0), 
                        a && 0 >= l.getRandomInt(1, 100)) this.gotoFree(); else {
                            if (0 == this._thinkEatTimes && !a) {
                                for (var n, i = r.gameCtrl._foodtree.retrieve({
                                    x: this.node.x,
                                    y: this.node.y,
                                    width: 300,
                                    height: 300
                                }), _ = [], o = 0; o < i.length && (n = i[o], !(25e4 >= l.getDist2(n, {
                                    x: this.node.x,
                                    y: this.node.y
                                }) && (_.push(n), 10 < _.length))); o++) ;
                                var t = null;
                                0 < _.length && (t = _[l.getRandomInt(0, _.length)]), null == t ? (this._aiState = d.ROBOT_AI_STATE_FREE, 
                                this._randomWalk()) : (this._tgtFood = t, this.move_to(t.x, t.y), this._thinkEatTimes += 1);
                            }
                            1 <= this._thinkEatTimes && (this._thinkEatTimes = 0);
                        }
                    }
                },
                onThinkFight: function() {
                    if (null != this._chaseTgt && this._chaseTgt.is_alive && null != r.gameCtrl) {
                        var a = r.gameCtrl.entities[this._chaseTgt.id], e = l.getDist2(a, {
                            x: this.node.x,
                            y: this.node.y
                        }), d = this.attackRadius * this.attackRadius, _ = l.getNowWithMs();
                        if (this._avatar.is_alive && !this._inAttackAction && .6 < _ - this._lastAttackTime) {
                            var t = l.getRandomInt(1, 100);
                            this._lastAttackTime = _, 20 >= t && e < 8 * d ? (this.stop_move(), r.gameCtrl.on_attack(this._avatar, this._chaseTgt, !0)) : 20 < t && 50 >= t && e < 4 * d ? (this.stop_move(), 
                            r.gameCtrl.on_attack(this._avatar, this._chaseTgt, !0)) : e < d && (this.stop_move(), 
                            r.gameCtrl.on_attack(this._avatar, this._chaseTgt, !0));
                        }
                    }
                    null != this._chaseTgt && this._chaseTgt.is_alive && 90 >= l.getRandomInt(1, 100) ? (a = r.gameCtrl.entities[this._chaseTgt.id], 
                    (e = l.getDist2(a, {
                        x: this.node.x,
                        y: this.node.y
                    })) < 1.21 * (d = this.attackRadius * this.attackRadius) && !this._inAttackAction ? .6 < _ - this._lastAttackTime ? (this._lastAttackTime = _, 
                    this.stop_move(), r.gameCtrl.on_attack(this._avatar, this._chaseTgt, !0)) : this.stop_move() : this.move_to_around(this._chaseTgt.x, this._chaseTgt.y)) : this.gotoFree();
                },
                gotoFree: function() {
                    this._chaseTgt = null, this._aiState = d.ROBOT_AI_STATE_FREE, this._randomWalk();
                },
                onThinkOther: function() {
                    this.gotoFree();
                },
                fixedUpdate: function() {
                    if (null != r.gameCtrl && (1 < Math.abs(this._lastSyncX - this.node.x) || 1 < Math.abs(this._lastSyncY - this.node.y))) {
                        for (var a, d = r.gameCtrl._foodtree.retrieve({
                            x: this.node.x,
                            y: this.node.y,
                            width: 5,
                            height: 5
                        }), e = 0; e < d.length; e++) a = d[e], Math.abs(a.x - this.node.x) <= this.radius && Math.abs(a.y - this.node.y) <= this.radius && this.eat_food(a.id);
                        this._lastSyncX = this.node.x, this._lastSyncY = this.node.y;
                    }
                    null != r.gameCtrl && r.gameCtrl.refresh_avatartree(), !this._avatar.is_player && this._avatar.is_alive && this.think();
                },
                eat_food: function(a) {
                    var d = r.gameCtrl.foods[a];
                    if (null != d) {
                        r.gameCtrl.onFoodLeaveWorld({
                            id: a
                        });
                        var _ = i.data[d.fid].exp || 0;
                        r.gameCtrl.add_exp(this._aid, _);
                    }
                },
                update: function(a) {
                    if (!r.gameOver && null != this.node && (this._avatar.is_player, this._accumulatedTime += a, 
                    this._accumulatedTime > this._fixedTimeStep && (this.fixedUpdate(), this._accumulatedTime -= this._fixedTimeStep), this._tickCount += 1, 
                    0 == this._tickCount % 1 && this.updatePerTwoTick(), 0 == this._tickCount % 4 && 999 > this._footprintsNum && 1 < this._speedBoostScale && (this._footprintsNum += 1, 
                    null != r.gameCtrl))) {
                        null == this._playerNode && (this._playerNode = this.node.getChildByName("playerNode"));
                        var e = this._playerNode.convertToWorldSpaceAR(cc.v2(-100, 0));
                        r.gameCtrl.createFootprint(e);
                    }
                },
                stop_move: function() {
                    this.setSpeed(0), this._tgtPosX = null, this._tgtPosY = null, this._isMoving = !1, 
                    null == this._playerNode && (this._playerNode = this.node.getChildByName("playerNode")), this.play("Walk");
                },
                check_close: function(a, e) {
                    var d = e - this.node.y, i = a - this.node.x;
                    return Math.sqrt(d * d + i * i) < this.speed * this.getSpeedScale();
                },
                move_to_around: function(a, e) {
                    if (this.check_close(a, e)) return this.setPos(a, e), void this.stop_move();
                    var d = this.node.x - a, _ = this.node.y - e, t = cc.v2(d, _).normalize(), i = cc.v2(a, e);
                    i = d * d + _ * _ <= this.attackRadius * this.attackRadius ? cc.v2(a, e) : cc.v2(a, e).add(t.mul(.5 * this.attackRadius)), 
                    this.move_to(i.x, i.y);
                },
                rotate_to: function(a, e) {
                    var d = e - this.node.y, _ = a - this.node.x, t = 180 * Math.atan2(d, _) / Math.PI;
                    t = 0 >= t ? -t : 360 - t, this.tgtAngle = t, this.angle = t, null == this._playerNode && (this._playerNode = this.node.getChildByName("playerNode")), 
                    this.setRotation(this.angle);
                },
                move_to: function(a, e) {
                    return this.check_close(a, e) ? (this.setPos(a, e), void this.stop_move()) : void (this.setSpeed(5), 
                    this._tgtPosX = a, this._tgtPosY = e, this.rotate_to(a, e));
                },
                can_move: function(a, e) {
                    var d = this.mapWidth / 2 - this.radius, _ = this.mapHeight / 2 - this.radius - this.mapBorder, o = !0, i = a, s = e;
                    return Math.abs(a) > d && Math.abs(e) > _ ? o = !1 : Math.abs(a) <= d && Math.abs(e) > _ ? (i = a, 
                    _ <= e ? s = _ : e <= -_ && (s = -_)) : Math.abs(a) > d && Math.abs(e) <= _ && (s = e, 
                    d <= a ? i = d : a <= -d && (i = -d)), [ o, i, s ];
                },
                reset_weapon: function() {
                    null != this._weaponNode && null != this._playerNode && (this._weaponNode.rotation = this._playerNode.rotation + 185);
                },
                playAttackAction: function() {
                    var a = .2, e = .1;
                    if (null != this._avatar) {
                        var _ = u.data[this._avatar.level];
                        if (null != _ && (a = _.attack_time_1, e = _.attack_time_2), this._avatar.is_player) {
                            var n = 0;
                            0 < r.skills_halo[2] && (n = d.SKILL_HALO_PERCENT / 100, r.on_update_skill_halo(2, r.skills_halo[2] - 1)), a *= 1 - this._levelDelta / 2 * (r.skills[2] - 1) - n, 
                            e *= 1 - this._levelDelta / 2 * (r.skills[2] - 1) - n;
                        }
                    }
                    if (null != this._weaponNode) {
                        var t = this._weaponNode;
                        t.stopAllActions(), this.reset_weapon(), this._inAttackAction = !0, t.runAction(cc.sequence(cc.rotateBy(a, 145), cc.rotateBy(e, -145), cc.callFunc(function() {
                            this._inAttackAction = !1, this.reset_weapon();
                        }.bind(this))));
                    }
                },
                on_level_upgrade: function(a, e) {
                    if (a.is_player) {
                        var d = this.node.getChildByName("levelup");
                        d.active = !0, d.y = 90, d.opacity = 255, d.stopAllActions(), d.runAction(cc.sequence(cc.moveBy(.4, cc.v2(0, 50)), cc.spawn(cc.moveBy(.4, cc.v2(0, 50)), cc.fadeOut(.4)), cc.callFunc(function() {}.bind(this))));
                    }
                    a.id == this._avatar.id && (this._avatarInfo = u.data[e]), this.onRefreshByLv(a, e);
                },
                on_level_downgrade: function(a, e) {
                    a.id == this._avatar.id && (this._avatarInfo = u.data[e]), this.onRefreshByLv(a, e);
                },
                onRefreshByLv: function(l, e) {
                    var _ = l.level;
                    null != e && (_ = e);
                    var p = u.data[_];
                    if (null != p) {
                        this._modelId = _, this._weaponId = _, this.initModel();
                        var t = p.speed_per;
                        null == t && (t = 100), this._speedLvScale = t / 100;
                        var i = p.body_radius_per;
                        null == i && (i = 100);
                        var s = this.node.getChildByName("playerNode");
                        s.scale = i / 100, this._bodyScale = s.scale, this.radius = this.initRadius * this._bodyScale;
                        var n = p.weapon_radius_per;
                        if (null == n && (n = 100), null != this._weaponNode && (this._weaponNode.scale = n / 100, 
                        this.onWeaponUpdatePos()), this.attackRadius = this._weaponNode.width * this._weaponNode.scale, 
                        this._avatar.is_player) {
                            this.attackRadius = this.attackRadius * this._playerAttackRadiusFix;
                            var a = 0;
                            0 < r.skills_halo[0] && (a = d.SKILL_HALO_PERCENT / 100, r.on_update_skill_halo(0, r.skills_halo[0] - 1)), this.attackRadius = this.attackRadius * (1 + this._levelDelta * (r.skills[0] - 1) + a);
                            var c = 0;
                            0 < r.skills_halo[1] && (c = d.SKILL_HALO_PERCENT / 100, r.on_update_skill_halo(1, r.skills_halo[1] - 1)), this._speedLvScale = this._speedLvScale * (1 + this._levelDelta * (r.skills[1] - 1) + c);
                        }
                        this._guardRadius = this.attackRadius * (this._avatarInfo.guard_radius || 1);
                    }
                },
                onIsDead: function(a, e) {
                    var d = !a.is_alive;
                    if (!(null != this._nicknameNode && (this._nicknameNode.active = a.is_alive), null == this._playerNode && (this._playerNode = this.node.getChildByName("playerNode")), 
                    this._playerNode.active = a.is_alive, null != this._levelNode && (this._levelNode.active = a.is_alive), 
                    null != this._weaponNode && (this._weaponNode.active = a.is_alive), null != this._sexNode && (this._sexNode.active = a.is_alive), 
                    d)) this.setPos(a.x, a.y), this.onRefreshByLv(this._avatar, this._avatar.level), 
                    this._nicknameNode && (this._nicknameNode.getComponent(cc.Label).string = this._avatar.name); else if (e) {
                        this._deadEffectNode.active = !0, this._deadEffectNode.x = this.node.x, this._deadEffectNode.y = this.node.y;
                        var i = this._deadEffectNode.getComponent(cc.Animation);
                        i.on("finished", this.on_hit_finished, this), i.play("avatar_destroy");
                    }
                },
                on_hit_finished: function() {
                    this._deadEffectNode.active = !1;
                },
                onWeaponUpdatePos: function() {
                    null != this._weaponNode && this._weaponNode.active && (this._weaponNode.x = this.node.x, 
                    this._weaponNode.y = this.node.y);
                },
                onNicknameUpdatePos: function() {
                    null != this._nicknameNode && this._nicknameNode.active && (this._nicknameNode.x = this.node.x, 
                    this._nicknameNode.y = this.node.y + 100 * this._bodyScale + 5);
                },
                onLevelUpdatePos: function() {
                    if (null != this._levelNode && this._levelNode.active) {
                        var a = Math.max(this._nicknameNode.width / 2 + 35, 60);
                        this._levelNode.x = this.node.x - a, this._levelNode.y = this.node.y + 100 * this._bodyScale + 5, 
                        null != this._sexNode && this._sexNode.active && (this._sexNode.x = this.node.x - (a - 22), 
                        this._sexNode.y = this.node.y + 100 * this._bodyScale + 5);
                    }
                }
            })