
            var d= require("Utils"), _= require("GlobalData");
            cc.Class({
                extends: cc.Component,
                properties: {
                    loadingNode: cc.Node,
                    _tex: null,
                    _lastReqTime: 0
                },
                init: function() {
                    var a = d.getNow();
                    120 > a - this._lastReqTime && null != this._tex && !_.homeUICtrl._friendRankNeedsUpdate || (this._lastReqTime = a, 
                    _.homeUICtrl._friendRankNeedsUpdate = !1, this.showLoading(), this.scheduleOnce(function() {
                        this.hideLoading();
                    }.bind(this), .2), wx.postMessage({
                        messageType: 1
                    }));
                },
                start: function() {},
                _updateSubDomainCanvas: function() {},
                update: function() {},
                showLoading: function() {
                    this.loadingNode.active = !0, this.loadingNode.stopAllActions(), this.loadingNode.runAction(cc.repeatForever(cc.rotateBy(.2, 40)));
                },
                hideLoading: function() {
                    this.loadingNode.stopAllActions(), this.loadingNode.active = !1;
                }
            })