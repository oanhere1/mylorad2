
            var d= require("GlobalData");
            cc.Class({
                extends: cc.Component,
                properties: {
                    adItemPrefab: cc.Prefab,
                    adListScrollView: cc.ScrollView,
                    speedScale: 1,
                    _downwards: !0,
                    _percent: 100,
                    _scrolling: !1
                },
                onLoad: function() {
                    this.node.on("scroll-began", this.scroll_began, this), this.node.on("scroll-ended", this.scroll_ended, this);
                },
                scroll_began: function() {
                    this._scrolling = !0;
                },
                scroll_ended: function() {
                    this._scrolling = !1;
                    var a = this.adListScrollView.getMaxScrollOffset(), e = this.adListScrollView.getScrollOffset();
                    a && 0 < a.y && (this._percent = 100 - 100 * Math.abs(e.y / a.y));
                },
                init: function() {
                    this._percent = 100, this.adListScrollView.content.removeAllChildren();
                    for (var a, i = d.ad_list, e = 0; e < i.length; e++) a = cc.instantiate(this.adItemPrefab), 
                    a.getComponent("AdItem").init(i[e]), this.adListScrollView.content.addChild(a);
                },
                start: function() {},
                update: function() {
                    if (!this._scrolling) {
                        var e = d.ad_list_speed || 1;
                        e *= this.speedScale, this._downwards ? (this._percent -= e, 0 > this._percent && (this._percent = 0, 
                        this._downwards = !1)) : (this._percent += e, 100 < this._percent && (this._percent = 100, 
                        this._downwards = !0)), this.adListScrollView.scrollToPercentVertical(this._percent / 100);
                    }
                }
            })