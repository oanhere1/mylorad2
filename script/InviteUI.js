
            var o = require("GlobalData"), n = require("Utils"), a = require("Storage"), i = require("Const");
            cc.Class({
                extends: cc.Component,
                properties: {
                    inviteItemPrefab: cc.Prefab,
                    itemsNode: cc.Node,
                    noteLabel: cc.Label,
                    btnShare: cc.Node,
                    btnClaim: cc.Node,
                    bonusNode: cc.Node
                },
                onLoad: function() {},
                on_open: function() {
                    this.itemsNode.removeAllChildren();
                    for (var a, d = 0; 3 > d; d++) a = cc.instantiate(this.inviteItemPrefab), a.setName("item_" + (d + 1)), 
                    this.itemsNode.addChild(a), a.x = 140 * (d - 1);
                    this.on_refresh();
                },
                on_refresh: function() {
                    for (var a = 1; 3 >= a; a++) this.itemsNode.getChildByName("item_" + a).getComponent("InviteItem").init(a, a <= o.invite_num, a <= o.invite_claimed);
                    this.noteLabel.string = "今日已邀请" + o.invite_num + "人", this.bonusNode.active = !1, this.btnShare.getChildByName("red").active = 0 == o.invite, 
                    this.btnShare.stopAllActions(), this.btnShare.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(.3, 1.06), cc.scaleTo(.3, 1)))), 
                    this.btnClaim.getComponent(cc.Button).interactable = o.invite_num > o.invite_claimed && 0 < o.invite_num;
                },
                clear_red: function() {
                    var a = this.node.parent.getComponent("HomeUI");
                    null != a && (a.btnInvite.getChildByName("red").active = !1);
                },
                on_click_close: function() {
                    var a = this.node.parent.getComponent("HomeUI");
                    null != a && a.on_update_coin_revive(), this.node.active = !1;
                },
                on_click_share: function() {
                    var d = n.getRandomInt(1, i.SHARE_NUMS);
                    null != o.share_weights && (d = Math.floor(n.weightingChoice(o.share_weights)));
                    var _ = i.SHARE_TITLES[d - 1];
                    null != o.share_titles && (_ = o.share_titles[d - 1]), o.invite = 1, this.btnShare.getChildByName("red").active = !1;
                    var t = "invite:" + n.getToday();
                    a.set_data(t, 1), this.clear_red(), wx.shareAppMessage({
                        withShareTicket: !0,
                        title: _,
                        query: "cardid=" + d + "&inviterid=" + o.userInfo.openId,
                        imageUrl: o.cdnUrl + "share_cards/campaign_" + d + ".png",
                        success: function() {
                            this.scheduleOnce(function() {
                                wx.showToast({
                                    title: "分享成功"
                                });
                            }.bind(this), .5), n.log_event("share_card_" + d, {}), n.log_event("share_card", {
                                value: "" + d
                            }), n.log_event("share_1", {
                                stype: "7"
                            });
                        }.bind(this),
                        fail: function() {
                            n.log_event("share_0", {
                                stype: "13"
                            });
                        }.bind(this)
                    });
                },
                on_click_claim: function() {
                    if (!(o.invite_claimed >= o.invite_num)) {
                        n.log_event("click_invite_claim", {
                            num: "" + o.invite_num
                        }), this.bonusNode.active = !0;
                        var d = this.bonusNode.getChildByName("sun");
                        d.stopAllActions(), d.runAction(cc.repeatForever(cc.rotateBy(1, 40)));
                        var e = this.bonusNode.getChildByName("bg");
                        e.on(cc.Node.EventType.TOUCH_START, function() {}, this), e.on(cc.Node.EventType.TOUCH_MOVE, function() {}, this), 
                        e.on(cc.Node.EventType.TOUCH_END, this.onBonusTouchMaskEnd, this), e.on(cc.Node.EventType.TOUCH_CANCEL, this.onBonusTouchMaskEnd, this);
                        for (var _ = this.bonusNode.getChildByName("content"), l = "", t = o.invite_claimed + 1; t <= o.invite_num; t++) 1 == t ? (l += "金币x50 ", 
                        o.on_update_gold(o.gold + 50)) : 2 == t ? (l += "复活币x1 ", o.set_coin_revive(o.coin_revive + 1)) : 3 == t && (l += "开局10级", 
                        o.on_update_wheel_buff(o.wheel_buff + 1));
                        o.invite_claimed = Math.min(o.invite_num, 3);
                        var i = "claimed_invite:" + n.getToday();
                        a.set_data(i, o.invite_claimed), _.getChildByName("label").getComponent(cc.Label).string = l, this.clear_red();
                    }
                },
                onBonusTouchMaskEnd: function() {
                    this.bonusNode.active = !1, this.on_refresh();
                },
                start: function() {}
            })