 module.exports = {
                data: {
                    1: {
                        radius_per: 90,
                        id: 1,
                        exp: 3
                    },
                    2: {
                        radius_per: 105,
                        id: 2,
                        exp: 4
                    },
                    3: {
                        radius_per: 120,
                        id: 3,
                        exp: 5
                    },
                    4: {
                        radius_per: 135,
                        id: 4,
                        exp: 6
                    },
                    5: {
                        radius_per: 150,
                        id: 5,
                        exp: 7
                    },
                    6: {
                        radius_per: 165,
                        id: 6,
                        exp: 8
                    },
                    7: {
                        radius_per: 180,
                        id: 7,
                        exp: 9
                    }
                }
            }