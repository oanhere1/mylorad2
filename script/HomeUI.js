var d = require("avatar_data"),
    o = require("GlobalData"),
    u = require("Utils"),
    n = require("wxSdk"),
    r = (require("Const"), require("Storage")),
    i = require("ShareUtils");
cc.Class({
    extends: cc.Component,
    properties: {
        headSprite: cc.Sprite,
        nicknameLabel: cc.Label,
        evoItemsScrollView: cc.ScrollView,
        evoItemPrefab: cc.Prefab,
        historyNode: cc.Node,
        levelLabel: cc.Label,
        coinNode: cc.Node,
        btnShare: cc.Node,
        btnRank: cc.Node,
        btnStart: cc.Node,
        btnWheel: cc.Node,
        btnInvite: cc.Node,
        rankNodePrefab: cc.Prefab,
        wheelNodePrefab: cc.Prefab,
        inviteNodePrefab: cc.Prefab,
        todayBonusPrefab: cc.Prefab,
        btnTodayBonus: cc.Node,
        audioNode: cc.Node,
        settingNode: cc.Node,
        _historyInfo: null,
        _btnGameCenter: null,
        _friendRankNeedsUpdate: !1,
        _globalRankNeedsUpdate: !1,
        _todayRankNeedsUpdate: !1
    },
    onLoad: function () {
        var l = this;
        if (o.homeUICtrl = this, cc.game.setFrameRate(30), cc.director.preloadScene("game", function () {
                cc.log("Game scene preloaded");
            }), null != o.userInfo && null != o.userInfo.avatarUrl) {
            var p = o.userInfo.avatarUrl;
            cc.loader.load({
                url: p,
                type: "png"
            }, function (a, e) {
                null != l.headSprite && (l.headSprite.spriteFrame = new cc.SpriteFrame(e));
            }), this.nicknameLabel.string = o.userInfo.nickName;
        }
        for (var f in cc.log(o.userInfo), this.levelLabel.string = o.maxlevel + "/30", this.onRefreshHistory(this.historyNode.getChildByName("item_1"), o.last_info),
                this.onRefreshHistory(this.historyNode.getChildByName("item_2"), o.best_info), this.onRefreshHistory(this.historyNode.getChildByName("item_3"), o.total_info),
                this.evoItemsScrollView.content.removeAllChildren(), d.data)
            if (d.data.hasOwnProperty(f) && 0 < f) {
                var _ = cc.instantiate(this.evoItemPrefab);
                _.getComponent("EvoItem").init(f, o.maxlevel), this.evoItemsScrollView.content.addChild(_);
            }
        try {
            this._btnGameCenter = wx.createGameClubButton({
                icon: "green",
                style: {
                    left: 20,
                    top: 10,
                    width: 40,
                    height: 40
                }
            });
        } catch (e) {
            cc.log(e);
        }
        var t = wx.getStorageSync("coin_revive") || [1];
        o.coin_revive = t[0], this.btnWheel.active = o.wheel_on, o.wheel_on && (this.btnWheel.getChildByName("pointer").runAction(cc.repeatForever(cc.rotateBy(.1, 50))),
                this.btnWheel.getChildByName("sun").runAction(cc.repeatForever(cc.rotateBy(.4, 50)))), this.btnStart.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(.3, 1.06), cc.scaleTo(.3, 1)))),
            this.coinNode.getChildByName("value").getComponent(cc.Label).string = o.coin_revive, this.btnShare.getChildByName("ico").active = o.io && !o.s_c && 1 == o.share_revive_coin,
            this.audioNode.getComponent("AudioCtrl").init(!1), o.play_times = r.get_int("play_times");
        var i = wx.getStorageSync("today_bonus") || 0,
            s = u.getToday(),
            n = o.system_info.SDKVersion || "";
       //ddsh if (null != o.serverConfigs && 1 == o.serverConfigs.today_ad_bonus && (null == i || s > Math.floor(i)) && u.sdkVersionGreater(n, "2.0.4")) {
            this.btnTodayBonus.active = !0;
            var a = this.node.getChildByName("today_bonus");
            null == a && (a = cc.instantiate(this.todayBonusPrefab), this.node.addChild(a)), a.y = 1e3,
                a.getComponent("TodayBonusUI").init();
       //ddsh  } else this.btnTodayBonus.active = !1;
        this.settingNode.active = !1, this.btnInvite.active = true//o.today_invite && o.is_connected();
         this.check_inviter_by(),
            o.today_invite && o.is_connected() && this.check_inviter(), this.scheduleOnce(function () {
                this.refreshPanel(), o.auto_new_game && (o.auto_new_game = !1, this.onClickStart());
            }.bind(this), .05);
    },
    check_inviter_by: function () {
        if ("" != o.inviterid && o.is_connected()) {
            var e = "invite_by:" + u.getToday() + ":" + o.inviterid;
            0 < r.get_int(e) || n.request({
                url: o.baseUrl + "update_inviter",
                method: "POST",
                data: {
                    iid: o.inviterid
                },
                success: function () {
                    o.inviterid = "", r.set_data(e, 1);
                },
                fail: function () {}
            });
        }
    },
    check_inviter: function () {
        var a = u.getToday(),
            e = "invite:" + a;
        o.invite = r.get_int(e);
        var d = this.btnInvite.getChildByName("red");
        if (0 == o.invite) return d.active = !0, void(d.getChildByName("label").active = !1);
        d.active = !1;
        var i = "claimed_invite:" + a;
        o.invite_claimed = r.get_int(i), 3 <= o.invite_claimed || n.request({
            url: o.baseUrl + "get_inviter",
            method: "GET",
            success: function (a) {
                var e = a.data;
                0 == e.code && null != e.data && (o.invite_num = e.data || 0), 0 < o.invite_num && u.log_event("invite_friend", {
                    num: "" + o.invite_num
                }), o.invite_num > o.invite_claimed && (d.active = !0, d.getChildByName("label").active = !0, d.getChildByName("label").getComponent(cc.Label).string = Math.min(o.invite_num - o.invite_claimed, 3));
            }.bind(this),
            fail: function () {}.bind(this)
        });
    },
    refreshPanel: function () {
        var a = this.node.getChildByName("right");
        884 < a.width && (a.getComponent(cc.Widget).isAlignLeft = !1, a.width = 884, this.evoItemsScrollView.content.getComponent(cc.Layout).updateLayout(),
            this.node.getChildByName("left").getComponent(cc.Widget).left = 40);
        var e = o.system_info.SDKVersion || "";
        u.sdkVersionGreater(e, "2.2.0") ? this._show_ad_list(!0) : this._show_ad_list(!1);
    },
    _show_ad_list: function (a) {
        var e = cc.find("Canvas/right/ad_list");
        e && (e.active = a) && e.getComponent("AdListUI").init();
        var d = cc.find("Canvas/right/evo_bg");
        d && (d.getComponent(cc.Widget).right = a ? 150 : 10, this.evoItemsScrollView.content.getComponent(cc.Layout).updateLayout());
    },
    play_audio: function (a) {
        this.audioNode.getComponent("AudioCtrl").play_audio(a);
    },
    start: function () {},
    onRefreshHistory: function (a, e) {
        a.getChildByName("kill").getComponent(cc.Label).string = e[0], a.getChildByName("score").getComponent(cc.Label).string = e[1],
            a.getChildByName("time").getComponent(cc.Label).string = u.formatSeconds(e[2]);
    },
    add_coin_revive: function (a) {
        o.set_coin_revive(o.coin_revive + a), this.on_update_coin_revive();
    },
    on_update_coin_revive: function () {
        this.coinNode.getChildByName("value").getComponent(cc.Label).string = o.coin_revive;
    },
    on_clicked_btn: function () {
        null != this._btnGameCenter && this._btnGameCenter.hide();
    },
    on_back: function () {
        null != this._btnGameCenter && this._btnGameCenter.show();
    },
    on_click_today_bonus: function () {
        this.on_clicked_btn(), this.play_audio(0), u.log_event("click_today_bonus", {});
        var a = this.node.getChildByName("today_bonus");
        null == a && (a = cc.instantiate(this.todayBonusPrefab), this.node.addChild(a)), a.active = !0,
            a.y = 0, a.getComponent("TodayBonusUI").on_open();
    },
    onClickStart: function () {
        this.on_clicked_btn(), u.log_event("new_game", {
            cardid: "" + o.cardid
        });
        var a = r.get_int("play_times");
        o.play_times = a + 1, r.set_data("play_times", o.play_times);
        var e = "play_times:" + u.getToday(),
            d = r.get_int(e);
        o.today_play_times = d + 1, r.set_data(e, o.today_play_times), this._friendRankNeedsUpdate = !0, this._globalRankNeedsUpdate = !0,
            this._todayRankNeedsUpdate = !0, cc.director.loadScene("game");
    },
    onClickRank: function () {
        this.play_audio(0);
        var a = this.node.getChildByName("rank_bg");
        null == a && (a = cc.instantiate(this.rankNodePrefab), this.node.addChild(a)), a.active = !0,
            a.getComponent("RankUI").on_open();
    },
    on_click_wheel: function () {
        this.on_clicked_btn(), this.play_audio(0);
        var a = this.node.getChildByName("wheel_bg");
        null == a && (a = cc.instantiate(this.wheelNodePrefab), this.node.addChild(a)), a.active = !0,
            u.log_event("click_wheel_1", {}), a.getComponent("WheelUI").on_open();
    },
    on_click_invite: function () {
        this.play_audio(0);
        var a = this.node.getChildByName("invite_bg");
        null == a && (a = cc.instantiate(this.inviteNodePrefab), this.node.addChild(a)), a.active = !0,
            u.log_event("click_invite_1", {}), a.getComponent("InviteUI").on_open();
    },
    on_click_setting: function () {
        this.play_audio(0), this.settingNode.active = !0, this.settingNode.getComponent("SettingUI").on_open();
    },
    on_real_add_coin: function () {
        return this.add_coin_revive(1), "获得复活币";
    },
    check_share_valid: function (a) {
        var e = "home_share_groups:" + u.getToday() + ":" + a;
        return 0 == r.get_int(e) && (r.set_data(e, 1), !0);
    },
    onClickShare: function () {
        this.play_audio(0), i.shareToWx(1);
    },
    on_give_today_bonus: function (a) {
        var e = wx.getStorageSync("today_bonus") || 0,
            d = u.getToday();
        (null == e || d > Math.floor(e)) && (wx.setStorageSync("today_bonus", "" + d), this.scheduleOnce(function () {
            wx.showToast({
                title: "获得" + a + "枚复活币"
            });
        }.bind(this), .5), this.add_coin_revive(a), this.btnTodayBonus.active = !1);
    }
})