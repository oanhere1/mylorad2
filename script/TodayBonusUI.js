
            var d= require("GlobalData"), t= require("Utils"), i= require("VideoMgr");
            cc.Class({
                extends: cc.Component,
                properties: {
                    bgNode: cc.Node,
                    btnClaim: cc.Node,
                    btnClaimDouble: cc.Node,
                    _todayBonusAd: null,
                    _todayBonusAdLoaded: !1
                },
                onLoad: function() {
                    this.bgNode.on(cc.Node.EventType.TOUCH_START, function() {}, this), this.bgNode.on(cc.Node.EventType.TOUCH_MOVE, function() {}, this), 
                    this.bgNode.on(cc.Node.EventType.TOUCH_END, this.onTouchMaskEnd, this), this.bgNode.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchMaskCancel, this);
                },
                onClose: function() {
                    this.node.active = !1;
                },
                onTouchMaskEnd: function() {
                    this.onClose();
                },
                onTouchMaskCancel: function() {
                    this.onClose();
                },
                init: function() {
                    this.enable_claim_double(!1);
                    var a = d.system_info.SDKVersion || "";
                    t.sdkVersionGreater(a, "2.0.4") && i.createVideo(this, 3);
                },
                adOnClose: function(a) {
                    i.videoAd.bind_this.after_ad(a);
                },
                after_ad: function(a) {
                    var e = d.system_info.SDKVersion || "";
                    !t.sdkVersionGreater(e, "2.1.0") || null != a && a.isEnded ? (t.log_event("video_ad_3_5", {}), i.releaseVideo(), 
                    this.give_bonus(1), t.log_event("click_today_ad_finish", {})) : this.scheduleOnce(function() {
                        try {
                            wx.showToast({
                                title: "看完获得奖励"
                            });
                        } catch (a) {}
                    }.bind(this), .2);
                },
                on_ad_loaded: function() {
                    t.log_event("video_ad_3_2", {}), this.enable_claim_double(!0);
                },
                on_open: function() {
                    null == i.videoAd && this.init();
                },
                enable_claim_double: function(a) {
                    (this.btnClaimDouble.active = a) ? (this.btnClaim.x = -120, this.btnClaimDouble.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(.3, 1.06), cc.scaleTo(.3, 1))))) : this.btnClaim.x = 0;
                },
                give_bonus: function(a) {
                    cc.log("give_bonus: " + a), null != d.homeUICtrl && d.homeUICtrl.on_give_today_bonus(a), this.on_close();
                },
                on_click_claim: function() {
                    t.log_event("click_today_free", {}), this.give_bonus(1);
                },
                on_click_claim_double: function() {
                    t.is_ad_enabled() && (t.log_event("click_today_ad", {}), t.log_event("video_ad_3_3", {}), i.showVideo());
                },
                on_close: function() {
                    this.node.active = !1;
                },
                start: function() {}
            })