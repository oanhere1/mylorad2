
            var d= require("GlobalData");
            cc.Class({
                extends: cc.Component,
                properties: {
                    audios: {
                        default: [],
                        type: cc.AudioClip
                    },
                    _bg_audio: null,
                    _current_audio: null
                },
                onLoad: function() {},
                init: function(a) {
                    a && this.play_bg();
                },
                onDestroy: function() {
                    null != this._current_audio && (cc.audioEngine.stop(this._current_audio), this._current_audio = null), 
                    null != this._bg_audio && (cc.audioEngine.stop(this._bg_audio), this._bg_audio = null);
                },
                start: function() {},
                play_bg: function() {
                    0 == d.sound_on && (null != this._bg_audio && (cc.audioEngine.stop(this._bg_audio), this._bg_audio = null), 
                    this._bg_audio = cc.audioEngine.play(this.audios[0], !0, .5));
                },
                play_audio: function(a, e) {
                    0 == d.sound_on && this._play_audio(a, e);
                },
                _play_audio: function(a, e) {
                    null != this._current_audio && (cc.audioEngine.stop(this._current_audio), this._current_audio = null), 
                    null == e && (e = .8), this._current_audio = cc.audioEngine.play(this.audios[a], !1, e);
                }
            })