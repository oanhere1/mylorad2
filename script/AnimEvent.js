
            var d= require("GlobalData");
            cc.Class({
                extends: cc.Component,
                properties: {
                    goldNode: cc.Node
                },
                onEventNewRes: function() {
                    var a = this.goldNode;
                    this.goldNode.getChildByName("value").getComponent(cc.Label).string = d.gold;
                    for (var e, i = 1; 3 >= i; i++) {
                        e = this.node.getChildByName("res_" + i), e.stopAllActions();
                        var _ = a.parent.convertToWorldSpaceAR(a.position);
                        e.runAction(cc.moveTo(.15, this.node.convertToNodeSpaceAR(_)));
                    }
                    this.scheduleOnce(function() {
                        for (var a, d = 1; 3 >= d; d++) a = this.node.getChildByName("res_" + d), a.stopAllActions(), a.active = !1;
                    }.bind(this), .18);
                },
                start: function() {}
            })