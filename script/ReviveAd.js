
            var d= require("GlobalData"), t= require("Utils"), i = (require("Const"), require("VideoMgr"));
            require("Storage"), cc.Class({
                extends: cc.Component,
                properties: {
                    _goCtrl: null
                },
                init: function(a) {
                    this._goCtrl = a, i.createVideo(this, 7), this._goCtrl.enable_ad_revive(!1);
                },
                adOnClose: function(a) {
                    i.videoAd.bind_this.after_ad(a);
                },
                after_ad: function(a) {
                    var e = d.system_info.SDKVersion || "";
                    !t.sdkVersionGreater(e, "2.1.0") || null != a && a.isEnded ? (t.log_event("video_ad_7_5", {}), i.releaseVideo(), 
                    this._goCtrl.on_ad_revive(1), t.log_event("click_revive_ad_finish", {})) : (this._goCtrl.on_ad_revive(0), this.scheduleOnce(function() {
                        try {
                            wx.showToast({
                                title: "看完广告复活"
                            });
                        } catch (a) {}
                    }.bind(this), .2));
                },
                on_ad_loaded: function() {
                    t.log_event("video_ad_" + i.cur_adid + "_2", {}), this._goCtrl.enable_ad_revive(!0);
                },
                start: function() {}
            })