 cc.Class({
                extends: cc.Component,
                properties: {
                    rankSprite: cc.Sprite,
                    _tex: null,
                    _tick_count: 0
                },
                onLoad: function() {},
                init: function() {
                    this._tex = new cc.Texture2D(), sharedCanvas.width = 160, sharedCanvas.height = 450, 
                    this.rankSprite.node.active = !1, wx.postMessage({
                        messageType: 3
                    }), this._tick_count = 0;
                },
                start: function() {},
                _updateSubDomainCanvas: function() {},
                update: function() {
                    this._tick_count += 1, 10 < this._tick_count && (this.rankSprite.node.active || (this.rankSprite.node.active = !0));
                }
            })