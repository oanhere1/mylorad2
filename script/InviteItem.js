 cc.Class({
                extends: cc.Component,
                properties: {
                    onNode: cc.Node,
                    offNode: cc.Node,
                    valueLabel: cc.Label,
                    noteLabel: cc.Label,
                    claimedNode: cc.Node
                },
                init: function(a, e, d) {
                    this.onNode.active = e, this.offNode.active = !e, 1 == a ? this.valueLabel.string = "x50" : 2 == a ? this.valueLabel.string = "x1" : 3 == a && (this.valueLabel.string = "开局10级"), 
                    this.noteLabel.string = "邀请" + a + "人", this.claimedNode.active = d;
                    for (var i = 1; 3 >= i; i++) this.node.getChildByName("ico_" + i).active = a == i;
                },
                start: function() {}
            })